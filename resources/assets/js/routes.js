import VueRouter from 'vue-router'
import helper from './services/helper'

let routes = [
    {
        path: '/',
        component: require('./layouts/front-page'),
        meta: { requiresAuth: true },
        children: [
            {
                path: '/search',
                component: require('./views/front/pages/home.vue'),
                name: 'home'
            },
            {
                path: '/timelines',
                component: require('./views/front/pages/timeline/index'),
                name: 'timelines'
            },
            {
                path: '/likes',
                component: require('./views/front/pages/like/index'),
                name: 'likes'
            },
            {
                path: '/messages',
                component: require('./views/front/pages/message/index'),
                name: 'messages'
            },
            {
                path: '/mypage',
                component: require('./views/front/pages/mypage/index'),
                name: 'mypage'
            }
        ]
    },
    {
        path: '/',
        component: require('./layouts/front-page'),
        meta: { requiresAuth: true },
        children: [
            {
                path: '/settings',
                component: require('./views/front/pages/settings/index'),
                name: 'settings'
            },
            {
                path: '/email-change',
                component: require('./views/front/pages/settings/email_change'),
                name: 'email_change'
            },
            {
                path: '/password-change',
                component: require('./views/front/pages/settings/password_change'),
                name: 'password_change'
            },
            {
                path: '/notification-settings',
                component: require('./views/front/pages/settings/notification_settings'),
                name: 'notification_settings'
            },
            {
                path: '/block-users',
                component: require('./views/front/pages/settings/block_users'),
                name: 'block_users'
            },
            {
                path: '/favorite-users',
                component: require('./views/front/pages/settings/favorite_users'),
                name: 'favorite_users'
            },
            {
                path: '/quit/member',
                component: require('./views/front/pages/settings/quit_member'),
                name: 'quit_member'
            },
            {
                path: '/quit/member/confirm',
                component: require('./views/front/pages/settings/quit_member_confirm'),
                name: 'quit_member_confirm'
            },
            {
                path: '/notifications',
                component: require('./views/front/pages/notifications/index'),
                name: 'notifications'
            },
            {
                path: '/mypage/likes',
                component: require('./views/front/pages/like/sent'),
                name: 'sent_likes'
            },
            {
                path: '/mypage/timeline',
                component: require('./views/front/pages/timeline/manage'),
                name: 'my_timeline'
            },
            {
                path: '/mypage/timeline/:id',
                component: require('./views/front/pages/timeline/view'),
                name: 'my_timeline_view'
            },
            {
                path: '/mypage/history',
                component: require('./views/front/pages/points/history'),
                name: 'points_history'
            },
            {
                path: '/mypage/exchange',
                component: require('./views/front/pages/points/exchange_point'),
                name: 'exchange_point'
            },
            {
                path: '/mypage/finish',
                component: require('./views/front/pages/points/exchange_finish'),
                name: 'exchange_finish'
            },
            {
                path: '/mypage/form-banking',
                component: require('./views/front/pages/mypage/form_banking'),
                name: 'form_banking'
            },
            {
                path: '/mypage/history-detail/:id',
                component: require('./views/front/pages/points/history_detail'),
                name: 'history_detail'
            },
            {
                path: '/mypage/receipt/:id',
                component: require('./views/front/pages/points/receipt'),
                name: 'receipt'
            },
            {
                path: '/mypage/private',
                component: require('./views/front/pages/mypage/set_private'),
                name: 'set_private'
            },
            // {
            //     path: '/mypage/help',
            //     component: require('./views/front/pages/mypage/help'),
            //     name: 'help'
            // },
            // {
            //     path: '/mypage/ask',
            //     component: require('./views/front/pages/mypage/ask'),
            //     name: 'mypage_ask'
            // },
            {
                path: '/mypage/edit',
                component: require('./views/front/pages/mypage/profile'),
                name: 'mypage_profile'
            },
            {
                path: '/mypage/edit/introduction/:type',
                component: require('./views/front/pages/mypage/profile_introduction'),
                name: 'mypage_profile_introduction'
            },
            {
                path: '/mypage/edit/info1/:type',
                component: require('./views/front/pages/mypage/profile_info1'),
                name: 'mypage_profile_info1'
            },
            {
                path: '/mypage/edit/info2/:type',
                component: require('./views/front/pages/mypage/profile_info2'),
                name: 'mypage_profile_info2'
            },
            {
                path: '/mypage/edit/info3/:type',
                component: require('./views/front/pages/mypage/profile_info3'),
                name: 'mypage_profile_info3'
            },
            {
                path: '/notification/:id/detail',
                component: require('./views/front/pages/notifications/detail'),
                name: 'notification_detail'
            },
            {
                path: '/search/filter',
                component: require('./views/front/pages/search/filter'),
                name: 'search_filter'
            },
            {
                path: '/search/filter/:type',
                component: require('./views/front/pages/search/filter-item'),
                name: 'search_filter_item'
            },
            {
                path: '/user/:uid',
                component: require('./views/front/pages/search/detail'),
                name: 'profile_detail'
            },
            {
                path: '/timeline/add',
                component: require('./views/front/pages/timeline/add'),
                name: 'timeline_add'
            },
            {
                path: '/timeline/detail/:id',
                component: require('./views/front/pages/timeline/detail'),
                name: 'timeline_detail'
            },
            {
                path: '/chat/:firebaseKey',
                component: require('./views/front/pages/message/chat'),
                name: 'chat'
            },
            {
                path: '/message/prototype',
                component: require('./views/front/pages/message/prototype'),
                name: 'prototype'
            },
            {
                path: '/message/prototype/create',
                component: require('./views/front/pages/message/prototype_form'),
                name: 'prototype_create'
            },
            {
                path: '/message/prototype/edit/:id',
                component: require('./views/front/pages/message/prototype_form'),
                name: 'prototype_edit'
            },
            {
                path: '/mypage/identity',
                component: require('./views/front/pages/mypage/identity'),
                name: 'identity'
            },
        ]
    },
    {
        path: '/',
        component: require('./layouts/front-page'),
        meta: { requiresMale: true },
        children: [
            {
                path: '/mypage/membership',
                component: require('./views/front/pages/mypage/membership'),
                name: 'membership'
            },
            {
                path: '/user-card',
                // component: require('./views/front/pages/settings/credit_card_backup'),
                component: require('./views/front/pages/settings/credit_card'),
                name: 'credit_card'
            },
            {
                path: '/user-card/complete',
                // component: require('./views/front/pages/settings/credit_card_backup'),
                component: require('./views/front/pages/settings/credit_card_register'),
                name: 'credit_card_register'
            },
            {
                path: '/mypage/purchase',
                component: require('./views/front/pages/points/purchase'),
                name: 'point_purchase'
            },
            {
                path: '/mypage/membership/paid',
                component: require('./views/front/pages/mypage/membership_paid'),
                name: 'membership_paid'
            },
            {
                path: '/mypage/membership/platium',
                component: require('./views/front/pages/mypage/membership_platium'),
                name: 'membership_platium'
            },
            {
                path: '/mypage/membership/vip',
                component: require('./views/front/pages/mypage/membership_vip'),
                name: 'membership_vip'
            },
            {
                path: '/settings/membership',
                component: require('./views/front/pages/settings/membership_settings'),
                name: 'membership_settings'
            },
            {
                path: '/mypage/vip/image',
                component: require('./views/front/pages/mypage/select_vip_image'),
                name: 'select_vip_image'
            },
            {
                path: '/mypage/vip/complete',
                component: require('./views/front/pages/mypage/membership_vip_complete'),
                name: 'vip_complete'
            },
        ]
    },
    // {
    //     path: '/',
    //     component: require('./layouts/front-page'),
    //     meta: { requiresFemale: true },
    //     children: [
    //         {
    //             path: '/mypage/vip',
    //             component: require('./views/front/pages/mypage/set_vip'),
    //             name: 'set_vip'
    //         },
    //     ]
    // },
    {
        path: '/',
        component: require('./layouts/profile-setup-page'),
        meta: { requiresAuth: true },
        children: [
            {
                path: '/profile/step1',
                component: require('./views/front/auth/profile_step1'),
                name: 'profile_step1'
            },
            {
                path: '/profile/step2',
                component: require('./views/front/auth/profile_step2'),
                name: 'profile_step2'
            },
            {
                path: '/profile/step3',
                component: require('./views/front/auth/profile_step3'),
                name: 'profile_step3'
            },
            {
                path: '/profile/step4',
                component: require('./views/front/auth/profile_step6'),
                name: 'profile_step6'
            }
        ]
    },
    {
        path: '/',
        component: require('./layouts/guest-page'),
        meta: { requiresGuest: true },
        children: [
            {
                path: '/login',
                component: require('./views/front/auth/login'),
                name: 'login'
            },
            {
                path: '/password',
                component: require('./views/front/auth/password'),
                name: 'password'
            },
            {
                path: '/register/step1',
                component: require('./views/front/auth/register'),
                name: 'register'
            },
            {
                path: '/register/step2',
                component: require('./views/front/auth/register_step2'),
                name: 'register_step2'
            },
            {
                path: '/register/confirm',
                component: require('./views/front/auth/register_confirm'),
                name: 'register_confirm'
            },
            {
                path: '/register/sent_email',
                component: require('./views/front/auth/register_email_sent'),
                name: 'register_email_sent'
            },
            {
                path: '/auth/:token/activate',
                component: require('./views/front/auth/activate'),
                name: 'auth_activate'
            },
            {
                path: '/password/:token/reset',
                component: require('./views/front/auth/password_reset')
            },
            {
                path: '/password/reset/:token',
                component: require('./views/auth/reset')
            },
            {
                path: '/auth/social',
                component: require('./views/auth/social-auth')
            },
            {
                path: '/quit/complete',
                component: require('./views/front/auth/quit_complete'),
                name: 'quit_complete'
            },
            {
                path: '/admin',
                component: require('./views/auth/login'),
                name: 'admin_login'
            },
        ]
    },
    {
        path: '/',
        component: require('./layouts/guest-page'),
        children: [
            {
                path: '/terms',
                component: require('./views/other/terms_use'),
                name: 'terms_of_use'
            },
            {
                path: '/privacy',
                component: require('./views/other/privacy'),
                name: 'privacy'
            },
            {
                path: '/relax',
                component: require('./views/other/relax'),
                name: 'relax'
            },
            {
                path: '/commercial',
                component: require('./views/other/commercial'),
                name: 'commercial'
            },
            {
                path: '/photo-upload-tips',
                component: require('./views/other/photo_tip'),
                name: 'photo_tip'
            },
            {
                path: '/help',
                component: require('./views/front/pages/mypage/help'),
                name: 'guest_help'
            },
            {
                path: '/ask',
                component: require('./views/front/pages/mypage/ask'),
                name: 'guest_ask'
            },
            {
                path: '/mypage/cost',
                component: require('./views/front/pages/mypage/cost'),
                name: 'cost_setting'
            },
        ]
    },
    {
        path: '/',
        component: require('./layouts/default-page'),
        meta: { requiresAdmin: true },
        children: [
            {
                path: '/admin/dashboard',
                component: require('./views/admin/home'),
                name: 'admin_home'
            },
            {
                path: '/admin/users',
                component: require('./views/admin/users/index'),
                name: 'admin_users'
            },
            {
                path: '/admin/profiles',
                component: require('./views/admin/users/profile'),
                name: 'admin_profiles'
            },
            {
                path: '/admin/static',
                component: require('./views/admin/users/static'),
                name: 'admin_static'
            },
            {
                path: '/admin/static/download',
                name: 'admin_static_download'
            },
            {
                path: '/admin/messages/:userID',
                component: require('./views/admin/messages/index'),
                name: 'admin_messages'
            },
            {
                path: '/admin/message/detail/:firebaseKey/:messageID',
                component: require('./views/admin/messages/detail'),
                name: 'admin_message_detail'
            },
            {
                path: '/admin/all/messages',
                component: require('./views/admin/messages/list'),
                name: 'admin_all_messages'
            },
            {
                path: '/admin/users/age_approve',
                component: require('./views/admin/users/age_approve_list'),
                name: 'admin_users_age_approve'
            },
            {
                path: '/admin/users/image_approve',
                component: require('./views/admin/users/image_approve_list'),
                name: 'admin_users_image_approve'
            },
            {
                path: '/admin/user/detail/:userID/:type',
                component: require('./views/admin/users/detail'),
                name: 'admin_user_detail'
            },
            {
                path: '/admin/reports',
                component: require('./views/admin/reports/index'),
                name: 'admin_reports'
            },
            {
                path: '/admin/report/ranks',
                component: require('./views/admin/reports/rank'),
                name: 'admin_report_ranks'
            },
            {
                path: '/admin/blocks',
                component: require('./views/admin/blocks/index'),
                name: 'admin_block_users'
            },
            {
                path: '/admin/timelines',
                component: require('./views/admin/timeline/index'),
                name: 'admin_timelines'
            },
            {
                path: '/admin/notifications',
                component: require('./views/admin/notification/index'),
                name: 'admin_notifications'
            },
            {
                path: '/admin/settings',
                component: require('./views/admin/settings'),
                name: 'admin_settings'
            },
            {
                path: '/admin/telecom',
                component: require('./views/admin/telecom'),
                name: 'admin_telecom'
            },
        ]
    },
    {
        path: '*',
        component : require('./layouts/error-page'),
        children: [
            {
                path: '*',
                component: require('./views/errors/page-not-found')
            }
        ]
    }
];

const router = new VueRouter({
	routes,
    linkActiveClass: 'active',
    mode: 'history'
});

router.beforeEach((to, from, next) => {

    if (to.matched.some(m => m.meta.requiresAuth)){
        return helper.check().then(response => {
            if (response == 'Too Many Attempts.') {
                window.localStorage.setItem("too_many_attempts", 1);
                return next();
            }

            if(!response.authenticated){
                return next({ path : '/login'})
            }

            if (response.is_admin >= 1) {
                return next({ name: 'admin_home' });
            }

            if (response.status == 'activated') {
                if (to.path.startsWith('/profile') || from.path.startsWith('/profile')) {
                    if (to.path == '/search') {
                        return next({ name: 'profile_step1' });
                    }
                    return next()
                }

                return next({ name: 'profile_step1' });
            }
            
            if (to.path.startsWith('/profile')) {
                if (response.status == 'activated_profile') {
                    return next({ name: 'home' })
                }
            }

            return next()
        })
    }

    if (to.matched.some(m => m.meta.requiresMale)){
        return helper.check().then(response => {
            if (response == 'Too Many Attempts.') {
                window.localStorage.setItem("too_many_attempts", 1);
                return next();
            }

            if(!response.authenticated){
                return next({ path : '/login'})
            }

            if (response.is_admin >= 1) {
                return next({ name: 'admin_home' });
            }

            if (response.gender != 1) { // male
                return next({ name : 'home'})
            }

            return next()
        })
    }

    if (to.matched.some(m => m.meta.requiresFemale)){
        return helper.check().then(response => {
            if (response == 'Too Many Attempts.') {
                window.localStorage.setItem("too_many_attempts", 1);
                return next();
            }
            
            if(!response.authenticated){
                return next({ path : '/login'})
            }

            if (response.is_admin >= 1) {
                return next({ name: 'admin_home' });
            }

            if (response.gender != 2) { // female
                return next({ name : 'home'})
            }

            return next()
        })
    }

    if (to.matched.some(m => m.meta.requiresAdmin)){
        return helper.check().then(response => {
            if(!response.authenticated){
                return next({ name: 'admin_login' })
            }

            if (response.is_admin == 0) { // not admin
                return next({ name : 'home'})
            }

            return next()
        })
    }

    if (to.matched.some(m => m.meta.requiresGuest)){
        return helper.check().then(response => {
            if(response.authenticated) {
                if (response.is_admin >= 1) {
                    if (to.name == 'admin_login' || to.name == 'home') {
                        return next({ name: 'admin_home' });
                    } else {
                        helper.logout();

                        return next();
                    }
                }

                if (to.name === 'auth_activate') {
                    helper.logout();

                    return next();
                }
                return next({ name : 'home'})
            }

            return next()
        })
    }

    return next()
});

export default router;
