import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'

const store = new Vuex.Store({
	state: {
		auth: {
			first_name: '',
			last_name: '',
			email: '',
			avatar: '',
			status: '',
			is_admin: 0,
			profile: {},
			user: {}
		},
		masterData: {},
		config: {
			company_name: '',
			contact_person: ''
		},
		currentPath: '',
		questions: [],
		costs: [],
		notification: {
			notifications: [],
			hasUnreads: false
		},
		pointPackages: [],
		memberships: [],
		searchUsers: {
			users: [],
			hasMore: true
		},
		filters: {},
		appliedFilters: {},
		vipImages: [],
		internalAlert: {
			type: '',
			data: '',
			message: ''
		},
		likes: undefined,
		histories: undefined,
		timelines: {
			timelines: [],
			hasMore: true,
			hasLoaded: false
		},
		pointusages: {
			pointusages: [],
			hasMore: true,
			hasLoaded: false
		},
		timelineFilters: {
			city_id: -1,
			purpose_id: -1
		},
		chatrooms: {
			data: [],
			current_page: 1
		},
		footerBadge: {
			like: false,
			message: false,
			mypage: false
		},
		clientIP: 87313,
		filterMessageNotReply: false,
		lastChatroom: {
			id: undefined,
			messages: []
		}
	},
	mutations: {
		setAuthUserDetail (state, auth) {
        	for (let key of Object.keys(auth)) {
                state.auth[key] = auth[key];
            }
            if ('avatar' in auth)
            	state.auth.avatar = auth.avatar !== null ? auth.avatar : '/assets/avatar.svg';
		},
		resetAuthUserDetail (state) {
        	for (let key of Object.keys(state.auth)) {
                state.auth[key] = '';
            }
		},
		setConfig (state, config) {
        	for (let key of Object.keys(config)) {
                state.config[key] = config[key];
            }
		},
		setMasterData(state, data) {
			state.masterData = data;
		},
		updateCurrentPath(state, path) {
			state.currentPath = path
		},
		setCosts(state, costs) {
			state.costs = costs;
		},
		setQuestions(state, questions) {
			state.questions = questions;
		},
		setNotification(state, notification) {
			state.notification = notification;
		},
		setPointPackages(state, value) {
			state.pointPackages = value;
		},
		setMemberships(state, value) {
			state.memberships = value;
		},
		setSearchUsers(state, value) {
			state.searchUsers = value;
		},
		setFilters(state, value) {
			state.filters = value;
		},
		setAppliedFilters(state, value) {
			state.appliedFilters = value;
		},
		setVipImages(state, value) {
			state.vipImages = value;
		},
		setInternalAlert(state, value) {
			state.internalAlert = value;
		},
		setLikes(state, value) {
			state.likes = value;
		},
		setHistories(state, value) {
			state.histories = value;
		},
		setTimelines(state, value) {
			state.timelines = value;
		},
		setTimelineFilters(state, value) {
			state.timelineFilters = value;
		},
		setPointusages(state, value) {
			state.pointusages = value;
		},
		setChatrooms(state, value) {
			state.chatrooms = value;
		},
		setFooterBadge(state, value) {
			state.footerBadge = value;
		},
		setFilterMessageNotReply(state, value) {
			state.filterMessageNotReply = value;
		},
		setLastChatroom(state, value) {
			state.lastChatroom = value;
		}
	},
	actions: {
		setAuthUserDetail ({ commit }, auth) {
     		commit('setAuthUserDetail',auth);
     	},
     	resetAuthUserDetail ({commit}){
     		commit('resetAuthUserDetail');
     	},
		setConfig ({ commit }, data) {
     		commit('setConfig',data);
		},
		setMasterData({ commit }, data) {
			commit('setMasterData', data);
		},
		updateCurrentPath({ commit }, path) {
			commit('updateCurrentPath', path);
		},
		setCosts({ commit }, costs) {
			commit('setCosts', costs);
		},
		setQuestions({ commit }, questions) {
			commit('setQuestions', questions);
		},
		setNotification({ commit }, notification) {
			commit('setNotification', notification);
		},
		setPointPackages({ commit }, value) {
			commit('setPointPackages', value)
		},
		setMemberships({ commit }, value) {
			commit('setMemberships', value);
		},
		setSearchUsers({ commit }, value) {
			commit('setSearchUsers', value);
		},
		setFilters({ commit }, value) {
			commit('setFilters', value);
		},
		setAppliedFilters({ commit }, value) {
			commit('setAppliedFilters', value);
		},
		setVipImages({ commit }, value) {
			commit('setVipImages', value);
		},
		setInternalAlert({ commit }, value) {
			commit('setInternalAlert', value);
		},
		setLikes({ commit }, value) {
			commit('setLikes', value);
		},
		setHistories({ commit }, value) {
			commit('setHistories', value);
		},
		setTimelines({ commit }, value) {
			commit('setTimelines', value);
		},
		setTimelineFilters({ commit }, value) {
			commit('setTimelineFilters', value);
		},
		setPointusages({ commit }, value) {
			commit('setPointusages', value);
		},
		setChatrooms({ commit }, value) {
			commit('setChatrooms', value);
		},
		setFooterBadge({ commit }, value) {
			commit('setFooterBadge', value);
		},
		setFilterMessageNotReply({ commit }, value) {
			commit('setFilterMessageNotReply', value);
		},
		setLastChatroom({ commit }, value) {
			commit('setLastChatroom', value);
		}
	},
	getters: {
		getAuthUser: (state) => (name) => {
		    return state.auth[name];
		},
		getAuthUserFullName: (state) => {
		    return state.auth['first_name']+' '+state.auth['last_name'];
		},
		getConfig: (state) => (name) => {
		    return state.config[name];
		},
	},
	plugins: [
		createPersistedState({ storage: window.sessionStorage })
	]
});

export default store;