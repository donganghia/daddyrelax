export default {
    loadUsers(params) {
        return axios.post('/api/admin/users', params).then(response =>  {
            return response.data;
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return 'error';
        });
    },
    loadUsersStatic(params) {
        return axios.post('/api/admin/users/static', params).then(response =>  {
            return response.data;
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return 'error';
        });
    },
    downloadCSV(params){
        return axios.post('/api/admin/users/static/report', params, {responseType: 'blob'}).then(response =>  {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            let fileName = 'AfilReport_' + moment().format('YYYY年MM月DD日'); +'.csv';
            link.setAttribute('download', fileName);
            document.body.appendChild(link);
            link.click();
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return 'error';
        });
    },
    getUsageStatus(user) {
        if (user.is_deleted == 1) return "<span class='label label-danger'>退会</span>";
        if (user.is_deleted == 2) return "<span class='label label-warning'>強制退会</span>";

        return "<span class='label label-primary'>利用中</span>"
    },
    
    getMemberStatus(user){
        if (user.status == 'pending_activation') {
            return '<span class="label label-danger">メール確認中</span>';
        } else if (user.status == 'activated') {
            return '<span class="label label-primary">プロフィル未登録</span>';
        }
        
        if (user.profile.gender == 1) {
            if(user.profile.membership.type == 'free')
                return '<span class="label label-free">' + user.profile.membership.name + '</span>';
            else if(user.profile.membership.type == 'paid')
                return '<span class="label label-paid">' + user.profile.membership.name + '</span>';
            else if(user.profile.membership.type == 'platium')
                return '<span class="label label-platium">' + user.profile.membership.name + '</span>';
            else if(user.profile.membership.type == 'vip')
                return '<span class="label label-vip">' + user.profile.membership.name + '</span>';
            else
                return '<span class="label label-default">未登録</span>';
        } else {
            if (user.profile.is_vip == 1) {
                return '<span class="label label-vip">VIP</span>';
            }
            return '<span>-</span>';
        }        
    },
    getIdentityStatus(user) {
        if(user.profile.age_approved == 1)
            return '<span class="label label-success">確認済み</span>';
        else if(user.documents && user.documents.length > 0 && user.profile.age_approved == 0)
            return '<span class="label label-primary">提出中</span>';
        else if(user.documents && user.documents.length > 0 && user.profile.age_approved == 2)
            return '<span class="label label-primary">否認</span>';
        else
            return '<span class="label label-danger">未提出</span>';
    },
    getImageStatus(user) {
        if (user.photos && user.photos.length == 0) return '<span class="label label-danger">未提出</span>';

        let approved = 1;
        if (user.photos && user.photos.length > 0) {
            user.photos.map(item => {
                if (item.is_approved != 1) approved = 0;
            })
        }
        if(approved == 1)
            return '<span class="label label-success">確認済み</span>';
        else
            return '<span class="label label-primary">提出中</span>';
    },
    
    formatDate(date) {
        if(!date)
            return;

        return moment(date).format('YYYY年MM月DD日');
    },

    formatShortDate(date) {
        if(!date)
            return;

        return moment(date).format('MM月DD日');
    },

    formatTime(date) {
        if(!date)
            return;

        return moment(date).format('HH:mm');
    },

    formatDBDate(date) {
        if (!date)
            return;
        
        return moment(date).format('YYYY-MM-DD');
    },

    formatTimelineDateTime(date) {
        if (!date)
            return;
        
        date = moment(date);
        let now = moment();
        let diff = moment.duration(now.diff(date));
        
        if (diff.asSeconds() < 60) return Math.floor(diff.asSeconds()) + '秒前';
        if (diff.asMinutes() < 60) return Math.floor(diff.asMinutes()) + '分前';
        if (diff.asHours() < 24) return Math.floor(diff.asHours()) + '時間前';
        if (diff.asDays() < 7) return Math.floor(diff.asDays()) + '日前';

        return moment(date).format('MM月DD日');
    },

    formatDateTime(date){
        if(!date)
            return;

        return moment(date).format('YYYY年MM月DD日 HH:mm');
    },

    formatDateWithin(date) {
        if (!date)
            return "";
        
        date = moment(date);
        let now = moment(now);
        let diff = moment.duration(now.diff(date));

        if (diff.asMinutes() > 5 && diff.asMinutes() <= 10) return '10分以内';
        if (diff.asMinutes() > 10 && diff.asMinutes() <= 30) return '30分以内';
        if (diff.asMinutes() > 30 && diff.asMinutes() <= 60) return '1時間以内';
        if (diff.asHours() >= 1 && diff.asHours() <= 3) return '3時間以内';
        if (diff.asHours() >= 3 && diff.asHours() <= 6) return '6時間以内';
        if (diff.asHours() >= 6 && diff.asHours() <= 12) return '12時間以内';
        if (diff.asHours() >= 12 && diff.asHours() <= 24) return '24時間以内';
        if (diff.asDays() >= 1 && diff.asDays() <= 3) return '3日以内';
        if (diff.asDays() >= 3 && diff.asDays() <= 5) return '5日以内';
        if (diff.asDays() >= 5 && diff.asDays() <= 7) return '1週間以内';
        if (diff.asDays() >= 7 && diff.asDays() <= 14) return '2週間以内';
        
        return parseInt(diff.asWeeks()) + "週間以内";
    },

    formatMoney(value) {
        value += '';
        let rgx = /(\d+)(\d{3})/;
        while (rgx.test(value)) {
            value = value.replace(rgx, '$1' + ',' + '$2');
        }
        return value;
    },
}
