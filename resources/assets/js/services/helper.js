export default {
    logout(){
        return axios.post('/api/auth/logout').then(response =>  {
            localStorage.removeItem('auth_token');
            axios.defaults.headers.common['Authorization'] = null;
            // toastr['success'](response.data.message);
        }).catch(error => {
            console.log(error);
        });
    },

    authUser(){
        return axios.get('/api/auth/user').then(response =>  {
            return response.data;
        }).catch(error => {
            return error.response.data;
        });
    },

    check(){
        return axios.post('/api/auth/check').then(response =>  {
            // return !!response.data.authenticated;
            return response.data;
        }).catch(error =>{
            // return response.data.authenticated;
            // return response.data;
            return error.response.data.message;
        });
    },

    login(self, params) {
        return axios.post('/api/auth/login', params).then(response =>  {
            localStorage.setItem('auth_token',response.data.token);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('auth_token');
            
            window.localStorage.removeItem('profile_step1');
            window.localStorage.removeItem('profile_step2');
            window.localStorage.removeItem('profile_step3');
            window.localStorage.removeItem('profile_step5');
            window.localStorage.removeItem('profile_step6');
            response = response.data;

            self.$store.dispatch('setAuthUserDetail',{
                first_name: response.profile.first_name,
                last_name: response.profile.last_name,
                email: response.user.email,
                avatar:response.profile.avatar,
                status: response.user.status,
                is_admin: response.user.is_admin,
                profile: response.profile,
                user: response.user
            });

            self.$store.dispatch('setSearchUsers', {
                users: [],
                hasMore: true
            });
            self.$store.dispatch('setFilters', {});
            self.$store.dispatch('setAppliedFilters', {});
            self.$store.dispatch('setLikes', undefined);
            self.$store.dispatch('setHistories', undefined);
            self.$store.dispatch('setChatrooms', {
                data: [],
                current_page: 1
            });
            self.$store.dispatch('setPointusages', {
                pointusages: [],
                hasMore: true,
                hasLoaded: false
            });
            self.$store.dispatch('setTimelines', {
                timelines: [],
                hasMore: true,
                hasLoaded: false
            })

            if (response.user.provider_unique_id == null || response.user.provider_unique_id == '') {
                db.collection('users').add({
                    user_id: response.user.id,
                    updateTarget: ''
                }).then(function(docRef) {
                    axios.post('/api/user/update/firebasekey', {
                        firebaseKey: docRef.id
                    }).then(response => {
                        let user = self.$store.state.auth.user;
                        user.provider_unique_id = docRef.id
                        self.$store.dispatch('setAuthUserDetail',{
                            user: user
                        });
                    });
                });
            }

            return response.user;
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return "error";
        });
    },

    checkLikePermission(self, me, user) {
        if (me.profile.gender == 1 && me.profile.points < 20) {
            self.$store.dispatch('setInternalAlert', {
                type: 'need-point-purchase',
                data: 'remain'
            });
            return false;
        }

        if (me.profile.gender == 1) {
            if (user.profile.gender == 2 && user.profile.is_vip) {
                if (me.profile.membership.type != 'vip') {
                    self.$store.dispatch('setInternalAlert', {
                        type: 'only-vip-can-do',
                        data: ''
                    });
                    return false;
                }
            }
        }

        let alreadyLikes = me.from_likes.filter(item => {
            return item.to_user_id == user.id
        })

        if (alreadyLikes == undefined || alreadyLikes.length == 0) {
            let limit = me.profile.gender == 1 ? self.$store.state.config.male_like_limit : self.$store.state.config.female_like_limit;

            if (limit > 0) {
                if (me.todayLikes.length >= limit) {
                    self.$store.dispatch('setInternalAlert', {
                        type: 'like-limit',
                        data: 'remain'
                    });

                    return false;
                }
            }
        }

        return true;
    },
    
    guid() {
        let nav = window.navigator;
        let screen = window.screen;
        let guid = nav.mimeTypes.length;
        guid += nav.userAgent.replace(/\D+/g, '');
        guid += nav.plugins.length;
        guid += screen.height || '';
        guid += screen.width || '';
        guid += screen.pixelDepth || '';
    
        return guid;
    },

    checkLikeBackPermission(self, me, user) {
        if (me.profile.gender == 1 && me.profile.points < 20) {
            self.$store.dispatch('setInternalAlert', {
                type: 'need-point-purchase',
                data: 'remain'
            });
            return false;
        }

        return true;
    },

    getAvatar(user) {
        let avatar = user.profile.avatar;

        if (user.profile.gender == 1 && user.profile.membership.type == 'vip') {
            if (user.profile.vip_image) {
                avatar = user.profile.vip_image.url;
            } else {
                if (user.profile.avatar == undefined || user.profile.avatar == null || user.profile.avatar == '') {
                    avatar = '/assets/vip_img01.svg';
                }

                if (user.profile.image_approved == 0 || user.profile.image_approved == 2) 
                    avatar = (user.profile.gender == 1 ? '/assets/avatar-male.png' : '/assets/avatar-female.png');
            }

            return avatar;
        }

        if (user.profile.image_approved == 0 || user.profile.image_approved == 2) return (user.profile.gender == 1 ? '/assets/avatar-male.png' : '/assets/avatar-female.png');

        return avatar && avatar != '' ? avatar : (user.profile.gender == 1 ? '/assets/avatar-male.png' : '/assets/avatar-female.png');
    },

    getOnlineStatus(user) {
        // show online off
        if (user.profile.show_online == 1) return "online-status mr-4";
        if (user.is_deleted >= 1) return "online-status mr-4";

        let date = user.last_login_at ? user.last_login_at : user.created_at;

        let loginAt = moment(date);
        let now = moment();
        let diff = moment.duration(now.diff(loginAt));
        if (diff.asDays() > 14) {
            return 'online-status mr-4'; // offline
        }

        if (user.is_logged_in == 1) {
            if (diff.asMinutes() > 10) return 'online-status mr-4 timeout'; // off for 10 mins
                    
            return 'online-status mr-4 on'; // online
        } else {
            return 'online-status mr-4 timeout'; // idle
        }
    },

    hasChatroom(me, user) {
        if (me.chatrooms.length == 0) return false;

        let chatrooms = me.chatrooms.filter(item => {
            return (item.from_user_id == me.id && item.to_user_id == user.id) || (item.to_user_id == me.id && item.from_user_id == user.id);
        });

        if (chatrooms && chatrooms.length > 0) return true;

        return false;
    },

    needPlanUp(self, me) {
        if (me.profile.age_approved == 0) {
            self.$store.dispatch('setInternalAlert', {
                type: 'age-need-approval',
                data: 'remain'
            });
            return true;
        }
        // if (me.profile.gender == 1 && me.profile.membership.type == 'free') {
        //     self.$store.dispatch('setInternalAlert', {
        //         type: 'need-plan-up',
        //         data: 'remain'
        //     });
        //     return true;
        // }

        return false;
    },

    needCreditCard(self, me) {
        if (me.profile.age_approved == 1 && me.profile.gender == 1 && me.profile.is_credit_card == 0) {
            self.$store.dispatch('setInternalAlert', {
                type: 'age-need-credit-card',
                data: 'remain'
            });
            return true;
        }
       
        return false;
    },

    likeUser(self, user, timelineId = null) {
        return axios.post('/api/user/like', {
            to_user_id: user.id,
            timeline_id: timelineId
        }).then(response => {
            self.$store.dispatch('setAuthUserDetail',{
                user: response.data.data,
                profile: response.data.data.profile
            });
            db.collection('users').doc(user.provider_unique_id).update({
                updateTarget: 'likes'
            });

            let searchUsers = self.$store.state.searchUsers;
            searchUsers.hasMore = true;
            self.$store.dispatch('setSearchUsers', searchUsers);

            return response;
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return "error";
        })
    },

    sendUpdatePush(self, value) {
        let providerIDs = [];
        self.$store.state.auth.user.to_likes.map(item => {
            let hasID = providerIDs.filter(obj => {
                return obj == item.from.provider_unique_id;
            })

            if (!(hasID && hasID.length > 0)) providerIDs.push(item.from.provider_unique_id);
        })

        providerIDs.map(item => {
            if (item && item != '') {
                db.collection('users').doc(item).update({
                    updateTarget: value
                });
            }
        })

        return;
    },

    fetchUsers(self, params) {
        return axios.post('/api/search', params)
        .then(response => {
            let result = response.data.results;

            let searchUsers = {
                users: result.data,
                hasMore: result.current_page != result.last_page
            }

            self.$store.dispatch('setSearchUsers', searchUsers);

            return searchUsers;
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return 'error';
        })
    },

    skipLike(self, user) {
        return axios.post('/api/user/like/skip', {
            user_id: user.id
        }).then(response => {
            self.$store.dispatch('setAuthUserDetail',{
                user: response.data.data,
                profile: response.data.data.profile
            });

            return response;
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return "error";
        })
    },

    likeBackUser(self, user, firebaseKey, timelineId = null) {
        return axios.post('/api/user/like/back', {
            user_id: user.id,
            firebaseKey: firebaseKey,
            timeline_id: timelineId,
        }).then(response => {
            self.$store.dispatch('setAuthUserDetail',{
                user: response.data.data,
                profile: response.data.data.profile
            });

            let searchUsers = self.$store.state.searchUsers;
            searchUsers.hasMore = true;
            self.$store.dispatch('setSearchUsers', searchUsers);

            if (response.data.newChatroomId > 0) {
                db.collection('messages').add({
                    chatroom_id: response.data.newChatroomId
                }).then(function(docRef) {
                    axios.post('/api/chatroom/firebaseKey', {
                        id: response.data.newChatroomId,
                        firebaseKey: docRef.id
                    }).then(response => {
                        self.$store.dispatch('setAuthUserDetail',{
                            user: response.data.data,
                            profile: response.data.data.profile
                        });
                
                        let unreads = response.data.data.chatrooms.filter(item => {
                            return !(item.read_ids.includes(',' + self.$store.state.auth.user.id + ','));
                        })
                        let badge = self.$store.state.footerBadge;
                        badge.message = unreads && unreads.length > 0;
                        self.$store.dispatch('setFooterBadge', badge);
                    });
                })

                db.collection('users').doc(user.provider_unique_id).update({
                    updateTarget: 'messages'
                });
            }

            return response;
        }).catch(error => {
            toastr['error'](error.response.data.message);
            return "error";
        })
    },

    loadMessages(firebaseKey, limit, page) {
        return axios.post('/api/chat/messages', {
            firebaseKey: firebaseKey,
            limit: limit,
            page: page
        }).then(response => {
            return response;
        }).catch(error => {
            return [];
        })
    },

    async asyncCall(url) {
        return await axios.post(url);
    },
    async initData(self) {
        let lists = [];
        lists.push({
            url: '/api/questions',
            storeFunc: 'setQuestions'
        });
        lists.push({
            url: '/api/point/costs',
            storeFunc: 'setCosts'
        });
        lists.push({
            url: '/api/notifications',
            storeFunc: 'setNotification'
        });
        lists.push({
            url: '/api/point/packages',
            storeFunc: 'setPointPackages'
        });
        lists.push({
            url: '/api/membership/list',
            storeFunc: 'setMemberships'
        });
        lists.push({
            url: '/api/master/data',
            storeFunc: 'setMasterData'
        });
        lists.push({
            url: '/api/vip/images',
            storeFunc: 'setVipImages'
        });

        let results = await Promise.all(lists.map(item => this.asyncCall(item.url)));
        lists.map((item, index) => {
            self.$store.dispatch(item.storeFunc, results[index].data.data);
        })

        if (self.$store.state.auth.user.is_admin == 0) {
            let unCheckLikes = self.$store.state.auth.user.to_likes.filter(item => {
                return item.is_view == 0; 
            });
    
            let unreads = self.$store.state.auth.user.chatrooms.filter(item => {
                return !(item.read_ids.includes(',' + self.$store.state.auth.user.id + ','));
            })
    
            let badge = {
                like: unCheckLikes && unCheckLikes.length > 0,
                message: unreads && unreads.length > 0,
                mypage: self.$store.state.notification.hasUnreads
            }
            self.$store.dispatch('setFooterBadge', badge);
        }

        self.$store.dispatch('setTimelineFilters', {
            city_id: -1,
            purpose_id: -1
        })

        return true;
    },

    getFilterURL(data){
        let url = '';
        $.each(data, function(key,value) {
            url += (value) ? '&'+key+'='+encodeURI(value) : '';
        });
        return url;
    },

    formAssign(form, data){
        for (let key of Object.keys(form)) {
            if(key !== "originalData" && key !== "errors" && key !== "autoReset"){
                form[key] = parseInt(data[key]);
            }
        }
        return form;
    },

    taskColor(value){
        let classes = ['progress-bar','progress-bar-striped'];
        if(value < 20)
            classes.push('bg-danger');
        else if(value < 50)
            classes.push('bg-warning');
        else if(value < 80)
            classes.push('bg-info');
        else
            classes.push('bg-success');
        return classes;
    },

    changeText(message) {
        var urlRegex = /(https?:\/\/[^\s]+)/g;
        return message.replace(urlRegex, function(url) {
            return '<a href="' + url + '" target="_blank" style="text-decoration: underline;">' + url + '</a>';
        })
    },

    formatDate(date){
        if(!date)
            return;

        return moment(date).format('YYYY年MM月DD日');
    },

    formatShortDate(date){
        if(!date)
            return;

        return moment(date).format('MM月DD日');
    },

    formatTime(date) {
        if(!date)
            return;

        return moment(date).format('HH:mm');
    },

    formatTimelineDateTime(date) {
        if (!date)
            return;
        
        date = moment(date);
        let now = moment();
        let diff = moment.duration(now.diff(date));
        
        if (diff.asSeconds() < 60) return Math.floor(diff.asSeconds()) + '秒前';
        if (diff.asMinutes() < 60) return Math.floor(diff.asMinutes()) + '分前';
        if (diff.asHours() < 24) return Math.floor(diff.asHours()) + '時間前';
        if (diff.asDays() < 7) return Math.floor(diff.asDays()) + '日前';

        return moment(date).format('MM月DD日');
    },

    formatFullDateTime(date){
        if(!date)
            return;

        date = moment(date);
        let now = moment();

        return moment(date).format('YYYY年MM月DD日 HH:mm');
    },

    formatDateTime(date){
        if(!date)
            return;

        date = moment(date);
        let now = moment();
        let diff = moment.duration(now.diff(date));

        // if (diff.asDays() > 1) return moment(date).format('MM月DD日 HH:mm');

        // return moment(date).format('HH:mm');
        return moment(date).format('MM月DD日 HH:mm');
    },

    formatDateWithin(date, isOnline = false) {
        if (!date)
            return "";
        
        date = moment(date);
        let now = moment(now);
        let diff = moment.duration(now.diff(date));

        if (isOnline) {
            if (diff.asMinutes() - 10 <= 10) return '10分以内';
            if (diff.asMinutes() - 10 > 10 && diff.asMinutes() - 10 <= 30) return '30分以内';
            if (diff.asMinutes() - 10 > 30 && diff.asMinutes() - 10 <= 60) return '1時間以内';
        } else {
            if (diff.asMinutes() <= 10) return '10分以内';
            if (diff.asMinutes() > 10 && diff.asMinutes() <= 30) return '30分以内';
            if (diff.asMinutes() > 30 && diff.asMinutes() <= 60) return '1時間以内';
        }
        
        if (diff.asHours() >= 1 && diff.asHours() <= 3) return '3時間以内';
        if (diff.asHours() >= 3 && diff.asHours() <= 6) return '6時間以内';
        if (diff.asHours() >= 6 && diff.asHours() <= 12) return '12時間以内';
        if (diff.asHours() >= 12 && diff.asHours() <= 24) return '24時間以内';
        if (diff.asDays() >= 1 && diff.asDays() <= 3) return '3日以内';
        if (diff.asDays() >= 3 && diff.asDays() <= 5) return '5日以内';
        if (diff.asDays() >= 5 && diff.asDays() <= 7) return '1週間以内';
        if (diff.asDays() >= 7 && diff.asDays() <= 14) return '2週間以内';
        
        return parseInt(diff.asWeeks()) + "週間以内";
    },

    formatMoney(value) {
        value += '';
        let rgx = /(\d+)(\d{3})/;
        while (rgx.test(value)) {
            value = value.replace(rgx, '$1' + ',' + '$2');
        }
        return value;
    },

    ucword(value){
        if(!value)
            return;

        return value.toLowerCase().replace(/\b[a-z]/g, function(value) {
            return value.toUpperCase();
        });
    },

    getPointsLabel(pointType) {
        let pointsType = [
            { type: 'admin_point_purchase', label: 'ボーナス' },
            { type: 'point_purchase', label: 'オートチャージ' }, //ポイントの購入
            { type: 'paid_membership_payment', label: '有料付与' },
            { type: 'platium_membership_payment', label: 'プラチナ付与' },
            { type: 'first_register', label: '登録ボーナス' },
            { type: 'like', label: 'いいね' },
            { type: 'timeline', label: '募集投稿' },
            { type: 'point_reset', label: 'ポイント自動消滅' },
        ];

        let label = '';
        $.each( pointsType, function( index, data ) { 
            if(pointType.includes(data.type)) {
                label = data.label;
                return false;
            }
        });

        return label;
    },
}
