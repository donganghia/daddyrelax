@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            お問い合わせ<br/>
            <br/>
            <pre>{{$content['body']}}</pre>
            <br/>
            メールアドレス： {{$content['user_email']}}<br/>            
            <br/>
            @if ($content['hasUser'])
            ユーザー情報<br/>
            会員ID: {{$content['userId']}}<br/>
            ニックネーム: {{$content['nickname']}}<br/>
            @endif
        </p>
	</div>
</div>
@endsection
