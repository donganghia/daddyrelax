@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            {{$content['name']}}さま<br/>
            <br/>
            {{$content['sender_name']}} {{$content['sender_age']}}歳（{{$content['sender_city']}}）からメッセージが届きました！<br/>
            <a href="{{$content['profile_url']}}">{{$content['profile_url']}}</a><br/>
            <br/>
            メッセージ一覧<br/>
            <a href="{{$content['message_page_url']}}">{{$content['message_page_url']}}</a><br/>
            <br/>
            メール通知設定<br/>
            <a href="{{$content['not_settings_url']}}">{{$content['not_settings_url']}}</a><br/>
            <br/>
            ------------------------------------<br/>
            mentee（メンティ）<br/>
            <a href="{{$content['site_url']}}">{{$content['site_url']}}</a>
        </p>
	</div>
</div>
@endsection
