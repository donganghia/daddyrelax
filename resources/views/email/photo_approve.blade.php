@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            {{$content['name']}}さま<br/>
            <br/>
            プロフィール写真が承認されました。<br/>
            <a href="{{$content['mypage_edit_url']}}">{{$content['mypage_edit_url']}}</a><br/>
            <br/>
            相手をさがす
            <a href="{{$content['search_url']}}">{{$content['search_url']}}</a><br/>
            <br/>
            引き続き、menteeをお楽しみください。<br/>
            <br/>
            ------------------------------------<br/>
            mentee（メンティ）<br/>
            <a href="{{$content['site_url']}}">{{$content['site_url']}}</a>
        </p>
	</div>
</div>
@endsection
