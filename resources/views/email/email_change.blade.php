@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            {{$content['name']}}さま<br/>
            <br/>
            こちらよりメールアドレスの変更を完了してください。<br/>
            <a href="{{$content['url']}}">{{$content['url']}}</a><br/>
            <br/>
            ※このメールにお心当たりのない場合は、恐れ入りますがsupport@men-tee.jp よりお問合せください。<br/>
            <br/>
            ------------------------------------<br/>
            mentee（メンティ）<br/>
            <a href="{{$content['site_url']}}">{{$content['site_url']}}</a>
        </p>
	</div>
</div>
@endsection
