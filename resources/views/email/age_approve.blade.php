@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            {{$content['name']}}さま<br/>
            <br/>
            身分証明書にて年齢確認が承認されました。<br/>
            さっそく「いいね」してみましょう！<br/>
            <br/>
            相手をさがす<br/>
            <a href="{{$content['search_url']}}">{{$content['search_url']}}</a><br/>
            <br/>
            マイページ<br/>
            <a href="{{$content['mypage_url']}}">{{$content['mypage_url']}}</a><br/>
            <br/>
            今後とも、menteeをよろしくお願いいたします。<br/>
            <br/>
            ------------------------------------<br/>
            mentee（メンティ）<br/>
            <a href="{{$content['site_url']}}">{{$content['site_url']}}</a>
        </p>
	</div>
</div>
@endsection
