@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            {{$content['name']}}さま
            <br/>
            {{$content['status']}}
            <br/>
            {{$content['message']}}
            <br/>
            https://プロフィールURL
            <br/>
            {{$content['content']}}
            <br/>
            今後とも（サービス名）をよろしくお願いいたします。
            <br/>
            ------------------------------------<br/>
            （サービス名）<br/>
            <a href="{{$content['site_url']}}">{{$content['site_url']}}</a>
        </p>
	</div>
</div>
@endsection
