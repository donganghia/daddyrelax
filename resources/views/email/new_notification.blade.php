@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            {{$content['name']}}さま<br/>
            <br/>
            メンティ事務局よりお知らせが届いています。<br/>
            こちらよりご確認ください。<br/>
            <br/>
            {{$content['notification_title']}}<br/>
            <a href="{{$content['notification_url']}}">{{$content['notification_url']}}</a><br/>
            <br/>
            ------------------------------------<br/>
            mentee（メンティ）<br/>
            <a href="{{$content['site_url']}}">{{$content['site_url']}}</a>
        </p>
	</div>
</div>
@endsection
