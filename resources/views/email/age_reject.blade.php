@extends('layouts.email_layout')

@section('content')

<div class="row ">
	<div class="col-md-12 col-sm-12 notification-title">
        <p>
            {{$content['name']}}さま<br/>
            <br/>
            年齢確認書が適切ではないため、承認されませんでした。<br/>
            再度「身分証明書」をご提出ください。<br/>
            <a href="{{$content['mypage_identity']}}">{{$content['mypage_identity']}}</a><br/>
            <br/>
            マイページ<br/>
            <a href="{{$content['mypage_url']}}">{{$content['mypage_url']}}</a><br/>
            <br/>
            今後とも、menteeをよろしくお願いいたします。<br/>
            <br/>
            ------------------------------------<br/>
            mentee（メンティ）<br/>
            <a href="{{$content['site_url']}}">{{$content['site_url']}}</a>
        </p>
	</div>
</div>
@endsection
