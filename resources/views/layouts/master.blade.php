<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<title>DROSY</title>
		
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700&display=swap" rel="stylesheet">
		<meta name="description" content="ハイスペック極上な出会いDORSYマッチング「成功した富裕男性」限定のマッチングアプリ。紳士的な男性と、夢と目標がある魅力的な女性たちの集いで、上質な癒しと非日常体験で毎日に彩を添えましょう。
		上質な癒しと非日常体験で毎日に彩を添えましょう。" />
		<meta name="keywords" content="マッチングアプリ,出会い,ドロシー,DROSY" />
		<meta name="author" content="DROSY" />
		<link rel="apple-touch-icon" href="https://drosy.jp/images/bookmark.png" />
		<link href="https://fonts.googleapis.com/css?family=Noto+Serif+SC:200,300,400&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700&display=swap" rel="stylesheet">
		<meta name="robots" content=" all " />
		<meta name="googlebot" content=" all ">

		<!-- OGP -->
		<meta property="og:title" content="ハイスペック極上の出会いマッチングアプリ「ドロシー DROSY 」" />
		<meta property="og:image" content="https://drosy.jp/images/drosy_opg.png" />
		<meta property="og:url" content="https://drosy.jp" />
		<meta property="og:site_name" content="DROSY" />
		<meta property="og:description" content="「余裕のある富裕男性」と「魅力的な女性」のためのマッチングアプリ【ドロシー DROSY 】" />
		<meta property="og:type" content="website">
		<!-- Summary -->
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="ハイスペック極上の出会いマッチングアプリ「ドロシー DROSY 」">
		<meta name="twitter:description" content="「余裕のある富裕男性」と「魅力的な女性」のためのマッチングアプリ【ドロシー DROSY 】">
		<meta name="twitter:image" content="https://drosy.jp/images/drosy_opg.png" />
		<link rel="shortcut icon" href="/images/favicon.png">

	    <meta name="csrf-token" content="{{ csrf_token() }}" />
		<link rel="shortcut icon" href="/images/favicon.png">
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
		    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!--削除不可　ここから-->
		<script type="text/javascript">
			proID = [17335];
		</script>
		<script src="https://i.bannerbridge.net/js/itp.js"></script>
		<!--削除不可　ここまで-->

		<script type="text/javascript" src="/js/jquery-2.2.0.min.js"></script>
		<script type="text/javascript" src="/js/aos.js"></script>
		<script type="text/javascript" src="/js/autosize.min.js"></script>

		@laravelPWA
	</head>
	<body class="fix-header fix-sidebar card-no-border">
		<link href="/css/aos.css" rel="stylesheet">
	    <link href="/css/style.css" rel="stylesheet">
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
	    <div id="root">
	        <router-view></router-view>
	    </div>
		<script src="/js/bundle.min.js"></script>
		<script>
			AOS.init({ once: true });
			var vh = window.innerHeight * 0.01;
			document.documentElement.style.setProperty('--vh', `${vh}px`);

			window.addEventListener("resize", function() {
				var vh = window.innerHeight * 0.01;
				document.documentElement.style.setProperty('--vh', `${vh}px`);
			});
		</script>
	</body>
</html>