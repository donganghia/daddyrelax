@extends('layouts.telecom')

@section('content')
<div class="container">
    <!-- SuccessOK! -->
    <div>Success</div>
</div>
@endsection

@section('javascript')
<script>
$(function() {
    var search = location.search.substring(1);
    console.log(search)
    var params = search == '' ? {} : JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')

    $.post('https://drosy.jp/api/card/response', params);
})
</script>
@endsection