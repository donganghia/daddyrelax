<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mailContent;
    protected $viewName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mailContent, $viewName)
    {
        $this->mailContent = $mailContent;
        $this->viewName = $viewName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->mailContent['email'])->queue(new SendEmail($this->mailContent, $this->viewName));
    }
}
