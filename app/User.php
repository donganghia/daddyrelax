<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'status', 'avatar', 'password', 'is_admin', 'last_login_at', 'is_logged_in', 'provider_unique_id', 'is_deleted', 'age_request_at', 'image_request_at', 'is_first','afil_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne('App\Model\Profile')->with('daddies')
                                                 ->with('marriage')
                                                 ->with('body')
                                                 ->with('salary')
                                                 ->with('job')
                                                 ->with('drink')
                                                 ->with('smoking')
                                                 ->with('membership')
                                                 ->with('city')
                                                 ->with('vip_image');
    }

    public function photos()
    {
        return $this->hasMany('App\Model\Photo', 'user_id', 'id')->orderBy('type')->orderBy("sort_no");
    }

    public function timelines()
    {
        return $this->hasMany('App\Model\Timeline', 'user_id', 'id')->where('is_deleted', 0)->with('purpose')->orderBy('created_at', 'desc');
    }

    public function card()
    {
        return $this->hasOne('App\Model\Card', 'user_id', 'id');
    }

    public function bank()
    {
        return $this->hasOne('App\Model\Bank', 'user_id', 'id');
    }

    public function alerts()
    {
        return $this->hasMany('App\Model\Alert', 'user_id', 'id');
    }

    public function documents()
    {
        return $this->hasMany('App\Model\Document', 'user_id', 'id');
    }

    public function fromHistories()
    {
        return $this->hasMany('App\Model\History', 'from_user_id', 'id');
    }

    public function toHistories()
    {
        return $this->hasMany('App\Model\History', 'to_user_id', 'id');
    }

    public function fromLikes()
    {
        return $this->hasMany('App\Model\Like', 'from_user_id', 'id')->orderBy('created_at', 'desc')->with('to');
    }

    public function toLikes()
    {
        return $this->hasMany('App\Model\Like', 'to_user_id', 'id')->orderBy('created_at', 'desc')->with('from');
    }

    public function payments()
    {
        return $this->hasMany('App\Model\Payment', 'user_id', 'id')->orderBy('created_at', 'desc');
    }

    public function blocks()
    {
        return $this->hasMany('App\Model\Block', 'user_id', 'id')->with('blockUser')->orderBy('created_at', 'desc');
    }

    public function favorites()
    {
        return $this->hasMany('App\Model\Favorite', 'user_id', 'id')->with('favoriteUser')->orderBy('created_at', 'desc');
    }

    public function memberships()
    {
        return $this->hasMany('App\Model\UserMembership', 'user_id', 'id')->with('membership')->orderBy('membership_id', 'desc');
    }

    public function reports()
    {
        return $this->hasMany('App\Model\Report', 'to_user_id', 'id')->where('is_admin_alert', 0)->with('from')->with('to')->orderBy('created_at', 'desc');
    }

    public function last90Reports()
    {
        $config = \App\Configuration::where('name', 'last_days')->first();
        $lastDays = $config ? 0 - intval($config->value) : -90;

        return $this->hasMany('App\Model\Report', 'to_user_id', 'id')->where('is_admin_alert', 0)->where('created_at', '>=', Carbon::now()->addDays($lastDays)->format('Y-m-d 00:00:00'))->with('from')->with('to')->orderBy('created_at', 'desc');
    }

    public function adminReports()
    {
        return $this->hasMany('App\Model\Report', 'to_user_id', 'id')->where('is_admin_alert', 1)->with('from')->with('to')->orderBy('created_at', 'desc');
    }

    public function sendReports()
    {
        return $this->hasMany('App\Model\Report', 'from_user_id', 'id')->where('is_admin_alert', 0)->where('created_at', 'like', Carbon::now()->format('%Y-m%'));
    }

    public function mySendReports()
    {
        return $this->hasMany('App\Model\Report', 'from_user_id', 'id')->with('from')->with('to')->orderBy('created_at', 'desc');
    }

    public function prototypes()
    {
        return $this->hasMany('App\Model\MessagePrototype')->orderBy('created_at');
    }

    public function todayTimelines()
    {
        return $this->hasMany('App\Model\Timeline', 'user_id', 'id')->where('is_deleted', 0)
        ->where('created_at', 'like', Carbon::now()->format('%Y-m-d%'))
        ->with('purpose')->orderBy('created_at', 'desc');
    }

    public function scopeActive($query, $userID)
    {
        $user = $query->where('id', $userID)
                        ->with('profile')
                        ->with('photos')
                        ->with('timelines')
                        ->with('bank')
                        ->with('card')
                        ->with('alerts')
                        ->with('documents')
                        ->with('toHistories')
                        ->with('fromLikes')
                        ->with('toLikes')
                        ->with('payments')
                        ->with('sendReports')
                        ->with('blocks')
                        ->with('favorites')
                        ->with('todayTimelines')
                        ->with('prototypes')
                        ->with('memberships')->first();

        $user->chatrooms = \App\Model\Chatroom::mine($user->id)->get();
        $user->blockByUsers = \App\Model\Block::where('block_user_id', $user->id)
                                   ->select('user_id')->get();
        $user->configurations = \App\Configuration::where('id', '>=', 1)->select(['name', 'value'])->get();
        $user->todayLikes = \App\Model\Like::where('from_user_id', $user->id)
                                        //    ->where('is_reply', 0)
                                           ->where('created_at', 'like', Carbon::now()->format('%Y-m-d%'))
                                           ->select(['to_user_id'])
                                           ->distinct()
                                           ->get();
        $user->lastAdminReport = \App\Model\Report::where('to_user_id', $user->id)->where('is_admin_alert', 1)->orderBy('created_at', 'desc')->first();

        return $user;
    }

    public function scopeAdminActive($query, $userID)
    {
        $user = $query->where('id', $userID)
                    ->with('profile')
                    ->with('photos')
                    ->with('documents')
                    ->with('reports')
                    ->with('last90Reports')
                    ->with('adminReports')
                    ->with('mySendReports')
                    ->with('blocks')->first();
        
        $user->configurations = \App\Configuration::where('id', '>=', 1)->select(['name', 'value'])->get();
        
        return $user;
    }

    public function scopeSearch($query, $filters)
    {
        $userId = $filters['user_id'];
        $paginateSize = $filters['limit'] + $filters['currentUsersNum'];

        $query = $query->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])
                    ->where('status', 'activated_profile')
                    ->where('is_admin', 0)
                    ->where('is_deleted', 0)
                    ->whereNotIn('id', function($query) use ($userId) {
                        $query->select('block_user_id')
                            ->from(with(new \App\Model\Block)->getTable())
                            ->where('user_id', $userId);
                    })
                    ->whereNotIn('id', function($query) use ($userId) {
                        $query->select('user_id')
                            ->from(with(new \App\Model\Block)->getTable())
                            ->where('block_user_id', $userId);
                    })
                    ->whereNotIn('id', function($query) use ($userId) {
                        $query->select('to_user_id')
                              ->from(with(new \App\Model\Like)->getTable())
                              ->where('from_user_id', $userId);
                    });
        
        if (isset($filters['days'])) {
            $now = Carbon::now()->addDays(0 - intval($filters['days']));
            $query = $query->where('created_at', '>=', $now->format('Y-m-d H:i:s'));
        }


        $query->whereIn('id', function($query) use ($filters) {
            $query->select('user_id')
                ->from(with(new \App\Model\Profile)->getTable())
                ->where('gender', $filters['gender'])
                ->where('is_private', 0);

            if (isset($filters['ages'])) {
                if ($filters['ages'][0] > 0) {
                    $query->where('age', ">=", $filters['ages'][0]);
                }
                if ($filters['ages'][1] > 0) {
                    $query->where('age', "<=", $filters['ages'][1]);
                }
            }

            if (isset($filters['heights'])) {
                if ($filters['heights'][0] > 0) {
                    $query->where('height', ">=", $filters['heights'][0]);
                }
                if ($filters['heights'][1] > 0) {
                    $query->where('height', "<=", $filters['heights'][1]);
                }
            }

            if (isset($filters['city_ids'])) {
                $query->whereIn('city_id', $filters['city_ids']);
            }

            if (isset($filters['marriage_ids'])) {
                $query->whereIn('marriage_id', $filters['marriage_ids']);
            }

            if (isset($filters['salary_ids'])) {
                $query->whereIn('salary_id', $filters['salary_ids']);
            }

            if (isset($filters['job_ids'])) {
                $query->whereIn('job_id', $filters['job_ids']);
            }

            if (isset($filters['body_ids'])) {
                $query->whereIn('body_id', $filters['body_ids']);
            }

            if (isset($filters['drink_ids'])) {
                $query->whereIn('drink_id', $filters['drink_ids']);
            }

            if (isset($filters['smoking_ids'])) {
                $query->whereIn('smoking_id', $filters['smoking_ids']);
            }
        });

        return $query->orderBy('last_login_at', 'desc')
                     ->with('profile')
                     ->with('photos')
                     ->withCount('last90Reports')->paginate($paginateSize);
    }

    public function scopeAdminUsers($query, $filters)
    {
        $query->where('is_admin', 0)
              ->where('status', 'activated_profile');

        $query->whereIn('id', function($query) use ($filters) {
            $query->select('user_id')
                ->from(with(new \App\Model\Profile)->getTable())
                ->where('id', '>=', 1);

            if (isset($filters['gender']) && intval($filters['gender']) > 0) {
                $query->where('gender', $filters['gender']);
            }

            if (isset($filters['membership_type']) && $filters['membership_type'] != '') {
                $type = $filters['membership_type'];
                if ($type != 'vip') {
                    $query->where('gender', 1);
                }
                $query->whereIn('membership_id', function($query1) use ($type) {
                    $query1->select('id')
                           ->from(with(new \App\Model\Master\Membership)->getTable())
                           ->where('type', $type);
                });

                if ($type == 'vip') {
                    $query->orWhere('is_vip', 1);
                }
            }

            if (isset($filters['city_id']) && intval($filters['city_id']) > 0) {
                $query->where('city_id', $filters['city_id']);
            }

            if (isset($filters['nickname']) && $filters['nickname'] != '') {
                $query->where('nickname', 'like', '%' . $filters['nickname'] . '%');
            }

            if (isset($filters['job_id']) && intval($filters['job_id']) > 0) {
                $query->where('job_id', $filters['job_id']);
            }

            if (isset($filters['marriage_id']) && intval($filters['marriage_id']) > 0) {
                $query->where('marriage_id', $filters['marriage_id']);
            }

            // if (isset($filters['image_approved']) && (intval($filters['image_approved']) == 1 || intval($filters['image_approved']) == 0)) {
            //     $query->where('image_approved', $filters['image_approved']);
            // } else if (isset($filters['image_approved']) && intval($filters['image_approved']) == 2) {
            //     $query->where('image_approved', 0);
            // }

            if (isset($filters['age_approved']) && (intval($filters['age_approved']) == 1 || intval($filters['age_approved']) == 0)) {
                $query->where('age_approved', $filters['age_approved']);
            } else if (isset($filters['age_approved']) && intval($filters['age_approved']) == 2) {
                $query->where('age_approved', 0);
            }

            // 運営からの警告回数
            if (isset($filters['alertNumStart'])) {
                $query->where('alert_status', '>=', $filters['alertNumStart']);
            }

            if (isset($filters['alertNumEnd'])) {
                $query->where('alert_status', '<=', $filters['alertNumEnd']);
            }
        });

        // 登録メアド
        if (isset($filters['email'])) {
            $query->where('email', 'like', '%' . $filters['email'] . '%');
        }

        if (isset($filters['image_approved']) && intval($filters['image_approved']) == 0) {
            $query->doesntHave('photos');
        } else if (isset($filters['image_approved'])) {
            $query->where('is_deleted', 0)
                  ->has('photos')
                  ->whereIn('id', function($query) {
                    $query->select('user_id')
                          ->from(with(new \App\Model\Photo)->getTable())
                          ->whereIn('is_approved', [0]);
                  });
        }

        if (isset($filters['age_approved']) && intval($filters['age_approved']) == 0) {
            $query->doesntHave('documents');
        } else if (isset($filters['age_approved']) && intval($filters['age_approved']) == 2) {
            $query->has('documents')
                    ->whereIn('id', function($query) {
                        $query->select('user_id')
                            ->from(with(new \App\Model\Profile)->getTable())
                            ->whereIn('age_approved', [0]);
                    });
        }

        if (isset($filters['start_date'])) {
            $query->where('created_at', '>=', $filters['start_date']);
        }

        if (isset($filters['end_date'])) {
            $query->where('created_at', '<=', $filters['end_date']);
        }

        // 通算通報回数
        if (isset($filters['reportNumStart']) || isset($filters['reportNumEnd'])) {
            $query->whereIn('id', function($query) use ($filters) {
                $users = \App\User::select('id')
                                  ->where('is_admin', 0)
                                  ->withCount('reports')->get();
                $ids = [];
                foreach ($users as $user) {
                    $flag = false;
                    if (isset($filters['reportNumStart']) && isset($filters['reportNumEnd'])) {
                        $flag = $user->reports_count >= intval($filters['reportNumStart']) && $user->reports_count <= intval($filters['reportNumEnd']);
                    } else if (isset($filters['reportNumStart'])) {
                        $flag = $user->reports_count >= intval($filters['reportNumStart']);
                    } else if (isset($filters['reportNumEnd'])) {
                        $flag = $user->reports_count <= intval($filters['reportNumEnd']);
                    }
                    if ($flag) {
                        $ids[] = $user->id;
                    }
                }
    
                $query->select('id')
                    ->from(with(new \App\User)->getTable())
                    ->whereIn('id', $ids);
            });
        }

        // 過去90日の通報回数
        if (isset($filters['limitReportNumStart']) || isset($filters['limitReportNumEnd'])) {
            $query->whereIn('id', function($query) use ($filters) {
                $users = \App\User::select('id')
                                  ->where('is_admin', 0)
                                  ->withCount('last90Reports')->get();

                $ids = [];
                foreach ($users as $user) {
                    $flag = false;
                    if (isset($filters['limitReportNumStart']) && isset($filters['limitReportNumEnd'])) {
                        $flag = $user->last90_reports_count >= intval($filters['limitReportNumStart']) && $user->last90_reports_count <= intval($filters['limitReportNumEnd']);
                    } else if (isset($filters['limitReportNumStart'])) {
                        $flag = $user->last90_reports_count >= intval($filters['limitReportNumStart']);
                    } else if (isset($filters['limitReportNumEnd'])) {
                        $flag = $user->last90_reports_count <= intval($filters['limitReportNumEnd']);
                    }
                    if ($flag) {
                        $ids[] = $user->id;
                    }
                }
    
                $query->select('id')
                    ->from(with(new \App\User)->getTable())
                    ->whereIn('id', $ids);
            });
        }

        // 会員区分
        if (isset($filters['user_status']) && intval($filters['user_status']) >= 0) {
            $query->where('is_deleted', $filters['user_status']);
        }

        // 会員ID
        if (isset($filters['userId']) && $filters['userId'] != '') {
            $query->where('id', $filters['userId']);
        }
       
        if (isset($filters['image_approved'])) {
            $query->orderBy('image_request_at', 'asc');
        } else if (isset($filters['age_approved'])) {
            $query->orderBy('age_request_at', 'asc');
        } else {
            $query->orderBy('created_at', 'desc');
        }

        return $query->with('profile')
                     ->with('photos')
                     ->with('documents')
                     ->with('reports')
                     ->withCount('last90Reports')
                     ->with('blocks')->paginate($filters['limit']);
    }

    public function scopeAfilUsers($query, $filters){
        $query->where('is_admin', 0)
            ->where('status', 'activated_profile');
        $query->whereNotNull('afil_id');
        if(!empty($filters['afilId'])){
            $query->where('afil_id', $filters['afilId']);
        }
        if(!empty($filters['gender'])){
            $query->whereHas('profile', function($q) use ($filters){
                $q->where('gender', $filters['gender']);
            });
        }
        if (isset($filters['start_date'])) {
            $query->where('created_at', '>=', $filters['start_date']);
        }
        if (isset($filters['end_date'])) {
            $query->where('created_at', '<=', $filters['end_date']);
        }
        return $query->with('profile');
    }
}
