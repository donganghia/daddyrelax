<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use App\Model\Notification;
use App\Jobs\SendEmailJob;
use App\Services\FirebaseService;

class NotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var FirebaseService
     */
    private $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        FirebaseService $service
    )
    {
        $this->service = $service;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->format("Y-m-d H:i:59");

        $notifications = Notification::where('delivery_at', '<=', $now)
                                     ->where('is_delivered', 0)
                                     ->whereIn('target', ['all', 'male', 'female'])->get();

        if (sizeof($notifications) > 0) {
            $users = User::where('is_admin', 0)
                        ->where('is_deleted', 0)
                        ->with('profile')->get();
            $notUsers = [];
            foreach ($users as $user) {
                if ('' . ($user->profile->notification_settings & 32) == '32') {
                    $notUsers[] = $user;
                }
            }

            foreach ($notifications as $notification) {
                foreach ($notUsers as $user) {
                    if ($notification->target === 'male') {
                        if ($user->profile->gender !== 1) continue;
                    }
                    if ($notification->target === 'female') {
                        if ($user->profile->gender !== 2) continue;
                    }

                    $content = [
                        'email' => $user->email,
                        'name' => $user->profile->nickname,
                        'subject' => '【DR】' . $notification->title,
                        'notification_url' => config('values.site_url') . "/notification/" . $notification->id . '/detail',
                        'notification_title' => $notification->title,
                        'site_url' => config('values.site_url')
                    ];

                    $this->service->updateUserData($user->provider_unique_id, 'has_alert_notification');
    
                    SendEmailJob::dispatch($content, 'email.new_notification');
                }

                $notification->is_delivered = 1;
                $notification->save();
            }
        }
        
    }
}
