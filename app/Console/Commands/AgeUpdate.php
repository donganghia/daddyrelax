<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Model\Profile;
use Carbon\Carbon;

class AgeUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'age:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Age update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereIn('id', function($query) {
            $query->select('user_id')
                ->from(with(new \App\Model\Profile)->getTable())
                ->whereNotNull('birthday');
        })->get();

        foreach ($users as $user) {
            $birthday = new Carbon($user->profile->birthday);

            $user->profile->age = $birthday->age;
            $user->profile->save();
        }
    }
}
