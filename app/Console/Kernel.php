<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\User;
use App\Model\Profile;
use Carbon\Carbon;
use App\Model\TelecomResponse;
use Exception;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\NotifyUsers',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            // Birthday Change
            $users = User::whereIn('id', function($query) {
                $query->select('user_id')
                    ->from(with(new \App\Model\Profile)->getTable())
                    ->whereNotNull('birthday');
            })->get();

            foreach ($users as $user) {
                $birthday = new Carbon($user->profile->birthday);

                $user->profile->age = $birthday->age;
                $user->profile->save();
            }
        })->daily()
        ->runInBackground();

        $schedule->command('notify:users')
            ->everyMinute()->runInBackground();

        // Points Deduction
        $schedule->call(function() {
            $users = User::where('is_deleted', 0)->with('profile')->get();
            $config = \App\Configuration::where('name', 'point_reset_days')->first();
            $days = $config ? 0 - intval($config->value) : -180;
            $days = $days > 0 ? 0 - $days : $days;
            $date = Carbon::now()->addDays($days);

            foreach ($users as $user) {
                $points = \App\Model\Payment::where('user_id', $user->id)
                                  ->where('created_at', 'like', $date->format('Y-m-d') . '%')->sum('points');
                if (intval($points) > 0) {
                    $usagePoints = \App\Model\PointUsage::where('user_id', $user->id)
                                ->whereIn('type', ['like', 'timeline', 'point_reset'])
                                ->where('created_at', '>=', $date->format('Y-m-d 00:00:00'))
                                ->where('updated_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))->sum('points');

                    if (intval($points) > intval($usagePoints)) {
                        $profilePoints = $user->profile->points - (intval($points) - intval($usagePoints));
                        $user->profile->points = $profilePoints < 0 ? 0 : $profilePoints;
                        $user->profile->save();

                        \App\Model\PointUsage::create([
                            'user_id' => $user->id,
                            'points' => intval($points) - intval($usagePoints),
                            'type' => 'point_reset'
                        ]);
                    }
                }
            }
        })->daily()->runInBackground();

        // Membership Upgrade
        $schedule->call(function() {
            // Check Expire
            $users = User::where('is_deleted', 0)
                        ->whereIn('id', function ($query) {
                            $query->select('user_id')
                                ->from(with(new \App\Model\Profile)->getTable())
                                ->where('membership_id', '>', 1);
                        })->get();
            $now = Carbon::now();

            $oneMonth = intval(config('values.month'));
            foreach ($users as $user) {
                $memberships = $user->memberships;
                $needChange = false;
                $membership_id = 1;
                foreach ($memberships as $membership) {
                    $expireAt = Carbon::parse($membership->expire_at);
                    $membership_id = $membership_id == 1 ? $membership->membership->id : 1;

                    if ($expireAt->lt($now)) {
                        $needUpdate = 0;
                        if ($membership->auto_update === 1) {
                            $needUpdate = 1;
                        }

                        // Before 14 days checking
                        if ($needUpdate === 0) {
                            if ($membership->auto_update_at) {
                                try {
                                    $autoUpdateAt = Carbon::parse($membership->auto_update_at);
                                    $before = Carbon::now()->addDays(-14);
                                    if ($before->lt($autoUpdateAt)) {
                                        $needUpdate = 2;
                                    }
                                } catch (\Throwable $e) {}
                            }
                        }

                        if ($needUpdate >= 1) {
                            if (!is_null($user->card)) {
                                try {
                                    $period = $membership->membership->period;

                                    $telecomResponse = TelecomResponse::where('sendid', $user->card->sendid)
                                                  ->where('type', 'payment')->first();
                                    if (!$telecomResponse) {
                                        $telecomResponse = TelecomResponse::create([
                                            'sendid' => $user->card->sendid,
                                            'type' => 'payment'
                                        ]);
                                    }

                                    $telecomResponse->response = 'processing';
                                    $telecomResponse->save();
                                    
                                    $client = new \GuzzleHttp\Client();
                                    $response = $client->request('GET', 'https://secure.telecomcredit.co.jp/inetcredit/secure/cont.pl?clientip=' . config('values.clientip') . '&sendid=' . $user->card->sendid . '&money=' . $membership->membership->price * $period);

                                    // 40 seconds delay
                                    sleep(40);

                                    $telecomResponse = TelecomResponse::where('sendid', $user->card->sendid)
                                                  ->where('type', 'payment')->first();
                                    
                                    if ($telecomResponse->response == 'yes') {
                                        $membership->last_paid_at = Carbon::now()->format("Y-m-d 00:00:00");
                                        $membership->expire_at = Carbon::now()->addDays($period * $oneMonth)->format("Y-m-d 00:00:00");
                                        $membership->save();
        
                                        $user->profile->points = $user->profile->points + $membership->membership->points;
                                        $user->profile->save();
        
                                        \App\Model\Payment::create([
                                            'user_id' => $user->id,
                                            'type' => 'membership_payment',
                                            'amount' => $membership->membership->price * $period,
                                            'points' => $membership->membership->points
                                        ]);

                                        \App\Model\PointUsage::create([
                                            'user_id' => $user->id,
                                            'type' => $membership->membership->type . '_membership_payment',
                                            'amount' => $membership->membership->price,
                                            'points' => $membership->membership->points
                                        ]);
                                    } else {
                                        $needChange = true;
                                        $membership_id = 1;
                                        $membership->delete();

                                        if ($membership->membership->type == 'paid') {
                                            \App\Model\UserMembership::where('user_id', $user->id)
                                                    ->where('membership_id', 5)->update(['auto_update' => 0]);
                                        }
                                    }
                                } catch (Exception $e) {
    
                                }
                            }
                        } else {
                            $needChange = true;
                            $membership_id = 1;
                            $membership->delete();

                            if ($membership->membership->type == 'paid') {
                                \App\Model\UserMembership::where('user_id', $user->id)
                                        ->where('membership_id', 5)->update(['auto_update' => 0, 'auto_update_at' => Carbon::now()]);
                            }
                        }
                    }
                }

                if ($needChange == true) {
                    $user->profile->membership_id = $membership_id;
                    $user->profile->save();
                }
            }

            // Auto Payment
            // $users = User::where('is_deleted', 0)
            //             ->whereIn('id', function ($query) {
            //                 $query->select('user_id')
            //                     ->from(with(new \App\Model\Profile)->getTable())
            //                     ->where('membership_id', '>', 1);
            //             })->get();

            // foreach ($users as $user) {
            //     $memberships = $user->memberships;
            //     foreach ($memberships as $membership) {
            //         $lastPaidAt = Carbon::parse($membership->last_paid_at)->addDays($membership->membership->period * $oneMonth);

            //         if ($lastPaidAt->lt($now)) {
            //             if (!is_null($user->card)) {
            //                 try {
            //                     $client = new \GuzzleHttp\Client();
            //                     $response = $client->request('GET', 'https://secure.telecomcredit.co.jp/inetcredit/secure/cont.pl?clientip=' . config('values.clientip') . '&sendid=' . $user->card->sendid . '&money=' . $membership->membership->price);

            //                     $membership->last_paid_at = Carbon::now()->format("Y-m-d 00:00:00");
            //                     if ($membership->membership->type == 'platium') {
            //                         $membership->expire_at = Carbon::now()->addDays($membership->membership->period * $oneMonth)->format("Y-m-d 00:00:00");
            //                     }
            //                     $membership->save();

            //                     $user->profile->points = $user->profile->points + $membership->membership->points;
            //                     $user->profile->save();

            //                     \App\Model\Payment::create([
            //                         'user_id' => $user->id,
            //                         'type' => 'membership_payment',
            //                         'amount' => $membership->membership->price,
            //                         'points' => $membership->membership->points
            //                     ]);
            //                 } catch (Exception $e) {

            //                 }
            //             }
            //         }
            //     }
            // }
        })->daily()->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
