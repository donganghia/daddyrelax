<?php
namespace App\Model;
use Eloquent;

class Photo extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'thumb_url',
                            'photo_url',
                            'sort_no',
                            'type',
                            'is_approved'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'photos';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }
}
