<?php
namespace App\Model;
use Eloquent;

class ProfileDaddy extends Eloquent {

    protected $fillable = [
                            'profile_id',
                            'daddy_id'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'profile_daddies';
    public $timestamps = false;
}
