<?php
namespace App\Model;
use Eloquent;

class UserMembership extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'membership_id',
                            'auto_update',
                            'is_enabled',
                            'expire_at',
                            'last_paid_at',
                            'auto_update_at'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'user_memberships';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function membership()
    {
        return $this->hasOne('App\Model\Master\Membership', 'id', 'membership_id');
    }
}
