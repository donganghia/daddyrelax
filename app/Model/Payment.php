<?php
namespace App\Model;
use Eloquent;

class Payment extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'type',
                            'amount',
                            'points',
                            'memo'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'payments';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }
}
