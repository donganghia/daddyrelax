<?php
namespace App\Model;
use Eloquent;

class Token extends Eloquent {

    protected $fillable = [
                            'email',
                            'token',
                            'content'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'tokens';

}
