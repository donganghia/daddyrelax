<?php
namespace App\Model;
use Eloquent;

class History extends Eloquent {

    protected $fillable = [
                            'from_user_id',
                            'to_user_id',
                            'message',
                            'is_reply'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'histories';

    public function from()
    {
        return $this->belongsTo('App\User', 'from_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }

    public function to()
    {
        return $this->belongsTo('App\User', 'to_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile');
    }
}
