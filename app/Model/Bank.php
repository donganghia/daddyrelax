<?php
namespace App\Model;
use Eloquent;

class Bank extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'branch_name',
                            'bank_name',
                            'branch_code',
                            'account_number',
                            'account_name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'banks';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }
}
