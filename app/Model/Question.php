<?php
namespace App\Model;
use Eloquent;

class Question extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'category_id',
                            'content',
                            'gender',
                            'publish',
                            'is_read'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'questions';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }

    public function category()
    {
        return $this->belongsTo('App\Model\Master\QuestionCategory', 'category_id');
    }

    public function answer()
    {
        return $this->hasOne('App\Model\Answer', 'question_id', 'id');
    }
}
