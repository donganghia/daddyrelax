<?php
namespace App\Model;
use Eloquent;

class Notification extends Eloquent {

    protected $fillable = [
                            'title',
                            'content',
                            'target',
                            'read_ids',
                            'delivery_at',
                            'is_delivered',
                            'show_alert',
                            'alert_read_ids'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'notifications';
}
