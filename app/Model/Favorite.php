<?php
namespace App\Model;
use Eloquent;

class Favorite extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'favorite_user_id'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'favorites';

    public function favoriteUser()
    {
        return $this->belongsTo('App\User', 'favorite_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos');
    }
}
