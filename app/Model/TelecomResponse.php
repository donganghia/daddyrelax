<?php
namespace App\Model;
use Eloquent;

class TelecomResponse extends Eloquent {

    protected $fillable = [
                            'sendid',
                            'type',
                            'response'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'telecom_responses';
}
