<?php
namespace App\Model;
use Eloquent;

class Document extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'url'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'documents';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }
}
