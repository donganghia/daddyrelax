<?php
namespace App\Model\Master;
use Eloquent;

class Body extends Eloquent {

    protected $fillable = [
                            'name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_bodies';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'body_id', 'id')->with('user');
    }
}
