<?php
namespace App\Model\Master;
use Eloquent;

class Marriage extends Eloquent {

    protected $fillable = [
                            'name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_marriages';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'marriage_id', 'id')->with('user');
    }
}
