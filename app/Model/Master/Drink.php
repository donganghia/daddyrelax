<?php
namespace App\Model\Master;
use Eloquent;

class Drink extends Eloquent {

    protected $fillable = [
                            'name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_drinks';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'drink_id', 'id')->with('user');
    }
}
