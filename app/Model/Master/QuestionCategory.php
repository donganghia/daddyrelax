<?php
namespace App\Model\Master;
use Eloquent;

class QuestionCategory extends Eloquent {

    protected $fillable = [
                            'name',
                            'gender',
                            'sort_no'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_question_categories';
    public $timestamps = false;

    public function questions()
    {
        return $this->hasMany('App\Model\Question', 'category_id', 'id');
    }
}
