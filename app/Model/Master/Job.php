<?php
namespace App\Model\Master;
use Eloquent;

class Job extends Eloquent {

    protected $fillable = [
                            'name',
                            'gender'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_jobs';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'job_id', 'id')->with('user');
    }
}
