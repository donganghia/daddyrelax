<?php
namespace App\Model\Master;
use Eloquent;

class City extends Eloquent {

    protected $fillable = [
                            'name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_cities';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'city_id', 'id')->with('user');
    }
}
