<?php
namespace App\Model\Master;
use Eloquent;

class Purpose extends Eloquent {

    protected $fillable = [
                            'name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_purposes';
    public $timestamps = false;
}
