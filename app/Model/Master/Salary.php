<?php
namespace App\Model\Master;
use Eloquent;

class Salary extends Eloquent {

    protected $fillable = [
                            'name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_salaries';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'salary_id', 'id')->with('user');
    }
}
