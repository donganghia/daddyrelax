<?php
namespace App\Model\Master;
use Eloquent;

class VipImage extends Eloquent {

    protected $fillable = [
                            'url'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_vip_images';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'vip_image_id', 'id')->with('user');
    }
}
