<?php
namespace App\Model\Master;
use Eloquent;

class Cost extends Eloquent {

    protected $fillable = [
                            'name', 'points'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_costs';
    public $timestamps = false;
}
