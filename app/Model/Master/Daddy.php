<?php
namespace App\Model\Master;
use Eloquent;

class Daddy extends Eloquent {

    protected $fillable = [
                            'name',
                            'asset'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_daddys';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'daddy_id', 'id')->with('user');
    }
}
