<?php
namespace App\Model\Master;
use Eloquent;

class Package extends Eloquent {

    protected $fillable = [
                            'name',
                            'price',
                            'original_price',
                            'points',
                            'sort_no'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_packages';
    public $timestamps = false;
}
