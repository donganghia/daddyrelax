<?php
namespace App\Model\Master;
use Eloquent;

class Smoking extends Eloquent {

    protected $fillable = [
                            'name'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_smokings';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'smoking_id', 'id')->with('user');
    }
}
