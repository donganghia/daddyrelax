<?php
namespace App\Model\Master;
use Eloquent;

class Membership extends Eloquent {

    protected $fillable = [
                            'name',
                            'type',
                            'price',
                            'original_price',
                            'period',
                            'off_percent',
                            'points',
                            'search_days',
                            'sort_no'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'mtb_memberships';
    public $timestamps = false;

    public function profiles()
    {
        return $this->hasMany('App\Model\Profile', 'membership_id', 'id')->with('user');
    }
}
