<?php
namespace App\Model;
use Eloquent;

class PointUsage extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'type',
                            'points',
                            'amount'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'point_usages';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile');
    }

    public function pointUsageDetails()
    {
        return $this->hasMany('App\Model\PointUsageDetail', 'point_usage_id', 'id')->with('user')->orderBy('created_at', 'asc');
    }

    public function scopeSearch($query, $filters)
    {
        $paginateSize = $filters['limit'] + $filters['page'];

        if (isset($filters['user_id']) && intval($filters['user_id']) > 0) {
            $query->where('user_id', $filters['user_id']);
        }

        return $query->orderBy('created_at', 'desc')->with('user')->with('pointUsageDetails')->paginate($paginateSize);
    }
}
