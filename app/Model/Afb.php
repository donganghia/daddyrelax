<?php
namespace App\Model;
use Eloquent;

class Afb extends Eloquent {

    protected $fillable = [
                            'unique_id',
                            'abm',
                            'email'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'afb';
}
