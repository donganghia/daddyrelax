<?php
namespace App\Model;
use Eloquent;

class Timeline extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'purpose_id',
                            'city_id',
                            'content',
                            'place',
                            'recruitment_date',
                            'is_deleted'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'timelines';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }

    public function purpose()
    {
        return $this->belongsTo('App\Model\Master\Purpose');
    }

    public function city()
    {
        return $this->belongsTo('App\Model\Master\City');
    }

    public function scopeSearch($query, $filters)
    {
        $paginateSize = $filters['limit'] + $filters['currentTimelinesNum'];

        $query = $query->where('is_deleted', 0)
                       ->whereIn('user_id', $filters['ids'])
                       ->whereNotIn('user_id', function($query) use ($filters) {
                            $query->select('block_user_id')
                                ->from(with(new \App\Model\Block)->getTable())
                                ->where('user_id', $filters['user_id']);
                        })
                        ->whereNotIn('user_id', function($query) use ($filters) {
                            $query->select('user_id')
                                ->from(with(new \App\Model\Block)->getTable())
                                ->where('block_user_id', $filters['user_id']);
                        })
                        ->whereNotIn('user_id', function($query) use ($filters) {
                            $query->select('id')
                                ->from(with(new \App\User)->getTable())
                                ->where('is_deleted', '>=', 1);
                        })
                        ->whereNotIn('user_id', function($query) use ($filters) {
                            $query->select('from_user_id')
                                ->from(with(new \App\Model\Chatroom)->getTable())
                                ->where('to_user_id', $filters['user_id']);
                        })
                        ->whereNotIn('user_id', function($query) use ($filters) {
                            $query->select('to_user_id')
                                ->from(with(new \App\Model\Chatroom)->getTable())
                                ->where('from_user_id', $filters['user_id']);
                        });
        
        if (isset($filters['purpose_id']) && intval($filters['purpose_id']) > 0) {
            $query->where('purpose_id', $filters['purpose_id']);
        }

        if (isset($filters['city_id']) && intval($filters['city_id']) > 0) {
            $query->where('city_id', $filters['city_id']);
        }

        return $query->orderBy('recruitment_date', 'asc')->orderBy('created_at', 'desc')
                     ->with('user')
                     ->with('purpose')
                     ->with('city')->paginate($paginateSize);
    }
}
