<?php
namespace App\Model;
use Eloquent;

class Alert extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'alert_status'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'alerts';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }
}
