<?php
namespace App\Model;
use Eloquent;

class Report extends Eloquent {

    protected $fillable = [
                            'from_user_id',
                            'to_user_id',
                            'content',
                            'is_admin_alert'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'reports';

    public function from()
    {
        return $this->belongsTo('App\User', 'from_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos');
    }

    public function to()
    {
        return $this->belongsTo('App\User', 'to_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos');
    }

    public function tos()
    {
        return $this->belongsToMany('App\User', 'reports', 'id', 'to_user_id');
    }
}
