<?php
namespace App\Model;
use Eloquent;

class Answer extends Eloquent {

    protected $fillable = [
                            'question_id',
                            'content'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'answers';

    public function question()
    {
        return $this->belongsTo('App\Model\Question');
    }
}
