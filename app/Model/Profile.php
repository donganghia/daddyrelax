<?php
namespace App\Model;
use Eloquent;

class Profile extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'first_name',
                            'last_name',
                            'gender',
                            'facebook_profile',
                            'twitter_profile',
                            'google_plus_profile',
                            'avatar',
                            'nickname',
                            'age_approved',
                            'avatar_thumb',
                            'address',
                            'height',
                            'job_id',
                            'daddy_id',
                            'marriage_id',
                            'dream',
                            'salary_id',
                            'body_id',
                            'drink_id',
                            'smoking_id',
                            'membership_id',
                            'content',
                            'work',
                            'points',
                            'notification_settings',
                            'likes',
                            'liked',
                            'is_private',
                            'is_credit_card',
                            'is_vip',
                            'alert_status',
                            'city_id',
                            'show_online',
                            'vip_image_id',
                            'image_approved',
                            'age',
                            'memo',
                            'birthday',
                            'alert_viewed'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'profiles';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }

    public function daddies()
    {
        return $this->belongsToMany('App\Model\Master\Daddy', 'profile_daddies', 'profile_id', 'daddy_id');
    }

    public function marriage()
    {
        return $this->belongsTo('App\Model\Master\Marriage', 'marriage_id');
    }

    public function salary()
    {
        return $this->belongsTo('App\Model\Master\Salary', 'salary_id');
    }

    public function body()
    {
        return $this->belongsTo('App\Model\Master\Body', 'body_id');
    }

    public function job()
    {
        return $this->belongsTo('App\Model\Master\Job', 'job_id');
    }

    public function drink()
    {
        return $this->belongsTo('App\Model\Master\Drink', 'drink_id');
    }

    public function smoking()
    {
        return $this->belongsTo('App\Model\Master\Smoking', 'smoking_id');
    }

    public function membership()
    {
        return $this->belongsTo('App\Model\Master\Membership', 'membership_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Model\Master\City', 'city_id');
    }

    public function vip_image()
    {
        return $this->belongsTo('App\Model\Master\VipImage', 'vip_image_id');
    }
}
