<?php
namespace App\Model;
use Eloquent;

class Chatroom extends Eloquent {

    protected $fillable = [
                            'from_user_id',
                            'to_user_id',
                            'firebaseKey',
                            'last_message',
                            'is_show',
                            'is_top',
                            'is_matching_message',
                            'read_ids',
                            'order_at',
                            'sender_id'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'chatrooms';

    public function from()
    {
        return $this->belongsTo('App\User', 'from_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }

    public function to()
    {
        return $this->belongsTo('App\User', 'to_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }

    public function scopeActive($query, $userID)
    {
        return $query->whereIn('id', function ($query) use ($userID) {
                                $query->select('id')
                                    ->from(with(new \App\Model\Chatroom)->getTable())
                                    ->where('from_user_id', $userID)
                                    ->whereIn('is_show', [1, 3])
                                    ->whereNotIn('is_top', [1, 3])
                                    ->whereNotIn('to_user_id', function($query) use ($userID) {
                                        $query->select('block_user_id')
                                            ->from(with(new \App\Model\Block)->getTable())
                                            ->where('user_id', $userID);
                                    });
                                    // ->whereNotIn('to_user_id', function($query) use ($userID) {
                                    //     $query->select('user_id')
                                    //         ->from(with(new \App\Model\Block)->getTable())
                                    //         ->where('block_user_id', $userID);
                                    // });
                            })->orWhereIn('id', function ($query) use ($userID) {
                                $query->select('id')
                                    ->from(with(new \App\Model\Chatroom)->getTable())
                                    ->where('to_user_id', $userID)
                                    ->whereIn('is_show', [2, 3])
                                    ->whereNotIn('is_top', [2, 3])
                                    ->whereNotIn('from_user_id', function($query) use ($userID) {
                                        $query->select('block_user_id')
                                            ->from(with(new \App\Model\Block)->getTable())
                                            ->where('user_id', $userID);
                                    });
                                    // ->whereNotIn('from_user_id', function($query) use ($userID) {
                                    //     $query->select('user_id')
                                    //         ->from(with(new \App\Model\Block)->getTable())
                                    //         ->where('block_user_id', $userID);
                                    // });
                            })->with('from')
                            ->with('to')
                            ->orderBy('order_at', 'desc');
    }

    public function scopeAdminActive($query, $userID)
    {
        return $query->whereIn('id', function ($query) use ($userID) {
                                $query->select('id')
                                    ->from(with(new \App\Model\Chatroom)->getTable())
                                    ->where('from_user_id', $userID);
                            })->orWhereIn('id', function ($query) use ($userID) {
                                $query->select('id')
                                    ->from(with(new \App\Model\Chatroom)->getTable())
                                    ->where('to_user_id', $userID);
                            })->with('from')
                            ->with('to')
                            ->orderBy('order_at', 'desc');
    }

    public function scopeMine($query, $userID)
    {
        return $query->whereIn('id', function ($query) use ($userID) {
                        $query->select('id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('from_user_id', $userID);
                            // ->whereIn('is_show', [1, 3]);
                    })->orWhereIn('id', function ($query) use ($userID) {
                        $query->select('id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('to_user_id', $userID);
                            // ->whereIn('is_show', [2, 3]);
                    })->select(['id', 'from_user_id', 'to_user_id', 'read_ids', 'firebaseKey'])
                    ->orderBy('is_top');
                    // ->orderBy('order_at', 'desc');
    }

    public function scopeMatched($query, $fromUserID, $toUserID)
    {
        return $query->whereIn('id', function ($query) use ($fromUserID, $toUserID) {
                        $query->select('id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('from_user_id', $fromUserID)
                            ->where('to_user_id', $toUserID);
                    })->orWhereIn('id', function ($query) use ($fromUserID, $toUserID) {
                        $query->select('id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('from_user_id', $toUserID)
                            ->where('to_user_id', $fromUserID);
                    })->select(['id']);
    }

    public function scopeUnread($query, $userID)
    {
        return $query->whereIn('id', function ($query) use ($userID) {
                        $query->select('id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('from_user_id', $userID)
                            ->where('read_ids', 'not like', '%,' . $userID . ',%')
                            ->whereIn('is_show', [1, 3]);
                    })->orWhereIn('id', function ($query) use ($userID) {
                        $query->select('id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('to_user_id', $userID)
                            ->where('read_ids', 'not like', '%,' . $userID . ',%')
                            ->whereIn('is_show', [2, 3]);
                    })->select(['id'])
                    ->orderBy('is_top');
                    // ->orderBy('order_at', 'desc');
    }
}
