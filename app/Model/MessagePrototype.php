<?php
namespace App\Model;
use Eloquent;

class MessagePrototype extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'message'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'message_prototypes';
}
