<?php
namespace App\Model;
use Eloquent;

class Block extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'block_user_id'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'blocks';

    public function blockUser()
    {
        return $this->belongsTo('App\User', 'block_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }
}
