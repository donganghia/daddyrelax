<?php
namespace App\Model;
use Eloquent;

class Like extends Eloquent {

    protected $fillable = [
                            'from_user_id',
                            'to_user_id',
                            'is_reply',
                            'is_view',
                            'timeline_id'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'likes';

    public function from()
    {
        return $this->belongsTo('App\User', 'from_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }

    public function to()
    {
        return $this->belongsTo('App\User', 'to_user_id')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile')->with('photos')->withCount('last90Reports');
    }

    public function timeline()
    {
        return $this->belongsTo('App\Model\Timeline', 'timeline_id');
    }
}
