<?php
namespace App\Model;
use Eloquent;

class PointUsageDetail extends Eloquent
{
    protected $fillable = [
        'target_id',
        'point_usage_id',
        'points',
        'type',
        'recruitment_start_date',
        'recruitment_end_date'
    ];

    protected $primaryKey = 'id';
    protected $table = 'point_usage_details';

    public function user()
    {
        return $this->belongsTo('App\User', 'target_id')
                    ->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])
                    ->with('profile');
    }
}
