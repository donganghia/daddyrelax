<?php
namespace App\Model;
use Eloquent;

class Message extends Eloquent {

    protected $fillable = [
                            'chatroom_id',
                            'user_id',
                            'message',
                            'is_read',
                            'is_image',
                            'recruitment_status'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'messages';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted'])->with('profile');
    }

    public function chatroom()
    {
        return $this->belongsTo('App\Model\Chatroom');
    }
}
