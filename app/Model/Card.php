<?php
namespace App\Model;
use Eloquent;

class Card extends Eloquent {

    protected $fillable = [
                            'user_id',
                            'name',
                            'card_number',
                            'expire',
                            'secret',
                            'telno',
                            'username',
                            'sendid',
                            'email',
                            'cont'
                        ];
    protected $primaryKey = 'id';
    protected $table = 'cards';

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id', 'provider_unique_id', 'status', 'last_login_at', 'is_logged_in', 'is_deleted']);
    }
}
