<?php
namespace App\Model;
use Eloquent;

class PasswordReset extends Eloquent {

    protected $fillable = [
                            'email',
                            'token'
                        ];
    protected $primaryKey = 'token';
    protected $table = 'password_resets';
    public $timestamps = false;

}
