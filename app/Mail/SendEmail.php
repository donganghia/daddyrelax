<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $mailContent;
    public $viewName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailContent, $viewName)
    {
        $this->mailContent = $mailContent;
        $this->viewName = $viewName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromEmail = config('values.admin_email');
        // if ($this->viewName == 'email.register' || $this->viewName == 'email.password_reset') {
        //     $fromEmail = "no-reply@" . config('values.domain');
        // } else {
        //     $fromEmail = "no-reply@" . config('values.domain');
        // }
        $fromEmail = "no-reply@" . config('values.domain');
        return $this->from(['address' => $fromEmail, 'name' => 'DROSY'])
                    ->subject($this->mailContent['subject'])
                    ->view($this->viewName)
                    ->with([
                        'content' => $this->mailContent
                    ]);
    }

    public function render()
    {
        $fromEmail = config('values.admin_email');
        // if ($this->viewName == 'email.register' || $this->viewName == 'email.password_reset') {
        //     $fromEmail = "no-reply@" . config('values.domain');
        // } else {
        //     $fromEmail = "no-reply@" . config('values.domain');
        // }
        $fromEmail = "no-reply@" . config('values.domain');
        return $this->from(['address' => $fromEmail, 'name' => 'DROSY'])
                    ->subject($this->mailContent['subject'])
                    ->view($this->viewName)
                    ->with([
                        'content' => $this->mailContent
                    ]);
    }
}
