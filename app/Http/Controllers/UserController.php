<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Master\City;
use App\Model\Master\Body;
use App\Model\Master\Daddy;
use App\Model\Master\Drink;
use App\Model\Master\Job;
use App\Model\Master\Marriage;
use App\Model\Master\Salary;
use App\Model\Master\Smoking;
use App\Model\Master\Package;
use App\Model\ProfileDaddy;
use App\Model\Master\Membership;
use App\User;
use App\Model\Block;
use App\Model\Token;
use App\Model\PasswordReset;
use Illuminate\Support\Str;
use App\Jobs\SendEmailJob;
use App\Model\Photo;
use App\Model\Chatroom;
use App\Model\Message;
use App\Model\Document;
use Carbon\Carbon;
use App\Model\Card;
use App\Model\Like;
use App\Model\Report;
use App\Model\Payment;
use App\Model\History;
use App\Model\Profile;
use App\Model\TelecomResponse;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Hash;
use Cookie;
use Excel;

class UserController extends Controller
{

    protected $avatar_path = 'images/users/';

	public function index(){
		$users = \App\User::with('profile');

		if(request()->has('first_name'))
            $query->whereHas('profile',function($q) use ($request){
                $q->where('first_name','like','%'.request('first_name').'%');
            });

		if(request()->has('last_name'))
            $query->whereHas('profile',function($q) use ($request){
                $q->where('last_name','like','%'.request('last_name').'%');
            });

		if(request()->has('email'))
			$users->where('email','like','%'.request('email').'%');

        if(request()->has('status'))
            $users->whereStatus(request('status'));

        if(request('sortBy') == 'first_name' || request('sortBy') == 'last_name')
            $users->with(['profile' => function ($q) {
              $q->orderBy(request('sortBy'), request('order'));
            }]);
        else
            $users->orderBy(request('sortBy'),request('order'));

		return $users->paginate(request('pageLength'));
	}

    public function updateProfile(Request $request){

        $validation = Validator::make($request->all(),[
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'date_of_birth' => 'required|date_format:Y-m-d',
            'gender' => 'required|in:male,female'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = JWTAuth::parseToken()->authenticate();
        $profile = $user->Profile;

        $profile->first_name = request('first_name');
        $profile->last_name = request('last_name');
        $profile->date_of_birth = request('date_of_birth');
        $profile->gender = request('gender');
        $profile->twitter_profile = request('twitter_profile');
        $profile->facebook_profile = request('facebook_profile');
        $profile->google_plus_profile = request('google_plus_profile');
        $profile->save();

        return response()->json(['message' => 'Your profile has been updated!','user' => $user]);
    }

    public function updateAvatar(Request $request){
        $validation = Validator::make($request->all(), [
            'avatar' => 'required|image'
        ]);

        if ($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = JWTAuth::parseToken()->authenticate();
        $profile = $user->Profile;

        if($profile->avatar && \File::exists($this->avatar_path.$profile->avatar))
            \File::delete($this->avatar_path.$profile->avatar);

        $extension = $request->file('avatar')->getClientOriginalExtension();
        $filename = uniqid();
        $file = $request->file('avatar')->move($this->avatar_path, $filename.".".$extension);
        $img = \Image::make($this->avatar_path.$filename.".".$extension);
        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($this->avatar_path.$filename.".".$extension);
        $profile->avatar = $filename.".".$extension;
        $profile->save();

        return response()->json(['message' => 'Avatar updated!','profile' => $profile]);
    }

    public function removeAvatar(Request $request){

        $user = JWTAuth::parseToken()->authenticate();

        $profile = $user->Profile;
        if(!$profile->avatar)
            return response()->json(['message' => 'No avatar uploaded!'],422);

        if(\File::exists($this->avatar_path.$profile->avatar))
            \File::delete($this->avatar_path.$profile->avatar);

        $profile->avatar = null;
        $profile->save();

        return response()->json(['message' => 'Avatar removed!']);
    }

    public function destroy(Request $request, $id){
        if(env('IS_DEMO'))
            return response()->json(['message' => 'You are not allowed to perform this action in this mode.'],422);

        $user = \App\User::find($id);

        if(!$user)
            return response()->json(['message' => 'Couldnot find user!'],422);

        if($user->avatar && \File::exists($this->avatar_path.$user->avatar))
            \File::delete($this->avatar_path.$user->avatar);

        $user->delete();

        return response()->json(['success','message' => 'User deleted!']);
    }

    public function dashboard(){
      $users_count = \App\User::count();
      $tasks_count = \App\Task::count();
      $recent_incomplete_tasks = \App\Task::whereStatus(0)->orderBy('due_date','desc')->limit(5)->get();
      return response()->json(compact('users_count','tasks_count','recent_incomplete_tasks'));
    }

    public function getMasterData()
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        return response()->json(['data' => [
            'bodies' => Body::all(),
            'cities' => City::all(),
            'daddies' => Daddy::all(),
            'drinks' => Drink::all(),
            'jobs' => Job::where('gender', $user->profile->gender == 1 ? 2 : 1)->get(),
            'sameJobs' => Job::where('gender', $user->profile->gender)->get(),
            'allJobs' => Job::all(),
            'marriages' => Marriage::all(),
            'salaries' => Salary::all(),
            'smokings' => Smoking::all(),
            'purposes' => \App\Model\Master\Purpose::all(),
            'memberships' => \App\Model\Master\Membership::where('type', '!=', 'vip')->get()
        ]]);
    }

    public function uploadImage(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        $image = $this->uploadPhoto($request, $user->id);

        $data = $request->all();

        $photo = Photo::where('user_id', $user->id)
                      ->where('type', $data['type'])
                      ->where('sort_no', $data['sort_no'])->first();

        if ($photo) {
            $photo->thumb_url = $image['thumb_url'];
            $photo->photo_url = $image['photo_url'];
            $photo->is_approved = 0;
            $photo->save();
        } else {
            $photo = Photo::create(['photo_url' => $image['photo_url'],
                                    'thumb_url' => $image['thumb_url'],
                                    'user_id' => $user->id,
                                    'type' => $data['type'],
                                    'sort_no' => $data['sort_no']]);
        }

        $user->image_request_at = Carbon::now();
        $user->save();

        return response()->json($photo);
    }

    public function uploadImageAndUpdateProfile(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        $image = $this->uploadPhoto($request, $user->id);

        $data = $request->all();

        $photo = Photo::where('user_id', $user->id)
                      ->where('type', $data['type'])
                      ->where('sort_no', $data['sort_no'])->first();

        if ($photo) {
            $photo->thumb_url = $image['thumb_url'];
            $photo->photo_url = $image['photo_url'];
            $photo->is_approved = 0;
            $photo->save();
        } else {
            $photo = Photo::create(['photo_url' => $image['photo_url'],
                                    'thumb_url' => $image['thumb_url'],
                                    'user_id' => $user->id,
                                    'type' => $data['type'],
                                    'sort_no' => $data['sort_no']]);
        }

        if ($photo->type == 'main_photo') {
            $user->Profile->avatar = $photo->thumb_url;
            $user->Profile->image_approved = 0;
            $user->Profile->save();
        }

        $user->image_request_at = Carbon::now();
        $user->save();

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function deleteImageAndUpdateProfile(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        Photo::where('user_id', $user->id)
             ->where('type', $request->get('type'))
             ->where('sort_no', $request->get('sort_no'))->delete();

        if ($request->get('type') == 'main_photo') {
            $user->Profile->avatar = '';
            $user->Profile->save();
        }

        // return response()->json($photo);
        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function updateUserFirebaseKey(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $user->provider_unique_id = $request->get('firebaseKey');
        $user->save();

        return response()->json(['data' => 'success']);
    }

    public function updateUserProfile(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $user = User::where('id', $user->id)->with('profile')->first();

        $data = $request->all();
        $profile = $user->profile;

        if (isset($data['alert_viewed'])) {
            $user->alert_viewed = $data['alert_viewed'];
            $user->save();

            $user = User::active($user->id);

            return response()->json(['data' => $user]);
        }

        if (isset($data['is_first'])) {
            $user->is_first = $data['is_first'];
            $user->save();

            $user = User::active($user->id);

            return response()->json(['data' => $user]);
        }

        if (isset($data['data'])) {

            $profileData = json_decode($data['data'], true);
            if (isset($profileData['daddies'])) {
                $daddies = $profileData['daddies'];
                unset($profileData['daddies']);
                ProfileDaddy::where('profile_id', $profile->id)->delete();
                foreach ($daddies as $daddy) {
                    ProfileDaddy::create([
                        'profile_id' => $profile->id,
                        'daddy_id' => $daddy
                    ]);
                }
            }

            if (isset($profileData['first_register'])) { // first register
                $membership = Membership::where('type', 'free')->first();
                if ($profile->gender == 1 && isset($membership)) {
                    $profileData['membership_id'] = $membership->id;
                    $profileData['points'] = $membership->points;

                    \App\Model\PointUsage::create([
                        'user_id' => $user->id,
                        'type' => 'first_register',
                        'amount' => 0,
                        'points' => $membership->points
                    ]);
                }
                unset($profileData['first_register']);
            }
            
            if (isset($profileData['membership_id'])) {
                $membership = Membership::where('id', $profileData['membership_id'])->first();
                if ($profile->gender == 1 && isset($membership)) {
                    $profileData['points'] = $profile->points + $membership->points;
                }
            }

            $profile->update($profileData);
        }
        
        if ($user->status == 'activated') {
            $user->status="activated_profile";
            $user->save();
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function unblockUser(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $block = Block::where('user_id', $user->id)
                      ->where('block_user_id', $request->get('block_user_id'))->delete();

        $user = User::active($user->id);
        $profile = $user->Profile;

        // return response()->json(compact('user','profile'));
        return response()->json(['data' => $user]);
    }

    public function emailChange(Request $request)
    {
        $user = User::where('email', $request->get('email'))->first();

        if (isset($user)) {
            return response()->json(['message' => '同じメールアドレスが既に登録されています。'], 422);
        }

        $currentUser = JWTAuth::parseToken()->authenticate();
        Token::where('email', $request->get('email'))->delete();
        $token = Token::create([
            'email' => $request->get('email'),
            'token' => Str::random(32),
            'content' => $currentUser->email
        ]);

        $content = [
            'email' => $request->get('email'),
            'name' => $currentUser->Profile->nickname != '' ? $currentUser->Profile->nickname : $request->get('email'),
            'subject' => '【DR】メールアドレス変更',
            'url' => config('values.site_url') . '/auth/' . $token->token . '/activate',
            'site_url' => config('values.site_url')
        ];
        // $emailJob = (new SendEmailJob($content, 'email.email_change'))->delay(Carbon::now()->addSeconds(1));
        // dispatch($emailJob);
        SendEmailJob::dispatch($content, 'email.email_change');

        // $currentUser->status = 'activated_email_change';
        $currentUser->save();

        return response()->json(['message' => 'success']);
    }

    public function loginUserPasswordChange(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        if (Hash::check($request->get('oldPassword'), $currentUser->password)) {
            // The passwords match...
            $currentUser->password = bcrypt(request('newPassword'));
            $currentUser->save();

            return response()->json(['data' => 'success']);
        }

        return response()->json(['data' => 'not match']);
    }

    public function passwordChange(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        PasswordReset::where('email', $request->get('email'))->delete();
        $t = Str::random(32);
        $token = PasswordReset::create([
            'email' => $request->get('email'),
            'token' => $t,
            'created_at' => Carbon::now()
        ]);

        $content = [
            'email' => $request->get('email'),
            'name' => $currentUser->Profile->nickname != '' ? $currentUser->Profile->nickname : $request->get('email'),
            'subject' => '【DR】パスワード変更',
            'url' => config('values.site_url') . '/password/' . $t . '/reset',
            'site_url' => config('values.site_url')
        ];
        // $emailJob = (new SendEmailJob($content, 'email.password_reset'))->delay(Carbon::now()->addSeconds(1));
        // dispatch($emailJob);
        SendEmailJob::dispatch($content, 'email.password_reset');

        return response()->json(['message' => 'success']);
    }

    public function updateCreditCard(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        $card = Card::where('user_id', $currentUser->id)->first();
        $data = $request->all();

        if (!$card) {
            $data['user_id'] = $currentUser->id;
            Card::create($data);
        } else {
            $card->update($data);
        }

        $profile = Profile::where('user_id', $currentUser->id)->first();
        $profile->is_credit_card = 1;
        $profile->save();

        $user = User::active($currentUser->id);
        $profile = $user->Profile;

        return response()->json(compact('user','profile'));
    }

    public function quitMember(Request $request)
    {
        $currentUser = User::where('email', $request->get('email'))->first();
        if ($currentUser) {
            $currentUser->is_deleted = 1;
            $currentUser->email = $currentUser->email . '___' . Str::random(6);
            $currentUser->save();

            if (sizeof($currentUser->memberships) > 0) {
                foreach ($currentUser->memberships as $membership) {
                    $membership->auto_update = 0;
                    $membership->is_enabled = 0;
                    $membership->save();
                }
            }
        }
        
        return response()->json(['data' => 'success']);
    }

    public function uploadIdentityImage(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        $image = $this->uploadPhoto($request, $user->id);

        $document = Document::where('user_id', $user->id)->first();
        if ($document) {
            $document->url = $image['photo_url'];
            $document->save();
        } else {
            $document = Document::create([
                'user_id' => $user->id,
                'url' => $image['photo_url']
            ]);
        }

        $profile = $user->Profile;
        $profile->age_approved = 0;
        $profile->save();

        $user->age_request_at = Carbon::now();
        $user->save();

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function likeUser(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        DB::beginTransaction();
        try {
            Like::create([
                'from_user_id' => $user->id,
                'to_user_id' => $request->get('to_user_id'),
                'timeline_id' => $request->get('timeline_id')
            ]);
            $profile = $user->Profile;
            if ($profile->gender == 1) {
                $configuration = \App\Configuration::where('name', 'like_point_usage')->first();
                $pointUsage = $configuration ? intval($configuration->value) : 20;

                $profile->points = $profile->points - $pointUsage;
                $profile->save();

                \App\Model\PointUsage::create([
                    'user_id' => $user->id,
                    'points' => $pointUsage,
                    'type' => 'like'
                ]);
            }

            // History::where('from_user_id', $user->id)
            //        ->where('to_user_id', $request->get('to_user_id'))->delete();
            // History::where('to_user_id', $user->id)
            //        ->where('from_user_id', $request->get('to_user_id'))->delete();

            $targetUser = User::find($request->get('to_user_id'));

            if (('' . $targetUser->profile->notification_settings & 2) == '2') { // like notification
                $content = [
                    'email' => $targetUser->email,
                    'name' => $targetUser->profile->nickname,
                    'subject' => '【DR】' . $profile->nickname . ' ' . $profile->age . '歳（' . $profile->city->name . '）いいね通知',
                    'sender_name' => $profile->nickname,
                    'sender_age' => $profile->age,
                    'sender_city' => $profile->city->name,
                    'profile_url' => config('values.site_url') . '/user/' . $user->provider_unique_id,
                    'like_page_url' => config('values.site_url') . '/likes',
                    'not_settings_url' => config('values.site_url') . '/notification-settings',
                    'site_url' => config('values.site_url')
                ];

                SendEmailJob::dispatch($content, 'email.like');
            } 

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function reportUser(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        DB::beginTransaction();
        try {
            Report::create([
                'from_user_id' => $user->id,
                'to_user_id' => $request->get('to_user_id'),
                'content' => $request->get('content')
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function blockUser(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        $block = Block::where('user_id', $user->id)
                      ->where('block_user_id', $request->get('to_user_id'))->first();
        if ($block) {
            $user = User::active($user->id);

            return response()->json(['data' => $user]);
        }

        DB::beginTransaction();
        try {
            Block::create([
                'user_id' => $user->id,
                'block_user_id' => $request->get('to_user_id')
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function leaveViewHistory(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        // $needToLeave = Chatroom::matched($user->id, $request->get('to_user_id'))->count() == 0;
        $needToLeave = true;
        
        // $like = Like::where('from_user_id', $user->id)
        //             ->where('to_user_id', $request->get('to_user_id'))->first();
        // if ($like) $needToLeave = false;

        if ($needToLeave) {
            DB::beginTransaction();
            try {
                History::create([
                    'from_user_id' => $user->id,
                    'to_user_id' => $request->get('to_user_id'),
                    'message' => ''
                ]);

                $profile = $user->Profile;
                $targetUser = User::find($request->get('to_user_id'));

                if (('' . $targetUser->profile->notification_settings & 16) == '16') { // leave history notification
                    $content = [
                        'email' => $targetUser->email,
                        'name' => $targetUser->profile->nickname,
                        'subject' => '【DR】' . $profile->nickname . ' ' . $profile->age . '歳（' . $profile->city->name . '）足あと通知',
                        'sender_name' => $profile->nickname,
                        'sender_age' => $profile->age,
                        'sender_city' => $profile->city->name,
                        'profile_url' => config('values.site_url') . '/user/' . $user->provider_unique_id,
                        'history_page_url' => config('values.site_url') . '/likes',
                        'not_settings_url' => config('values.site_url') . '/notification-settings',
                        'site_url' => config('values.site_url')
                    ];
    
                    SendEmailJob::dispatch($content, 'email.leave_history');
                } 
    
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(['message' => 'エラーが発生しました。'], 422);
            }
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function likeBackUser(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();
        $newChatroomId = -1;

        DB::beginTransaction();
        try {
            $like = Like::where('from_user_id', $request->get('user_id'))
                        ->where('to_user_id', $user->id)
                        ->with('timeline', 'timeline.purpose')
                        ->orderBy('created_at', 'desc')->first();
            
            $timelineId = $like->timeline_id;

            $chatroom = Chatroom::where('from_user_id', $user->id)
                                ->where('to_user_id', $request->get('user_id'))->first();
            $hasChatroom = false;
            if ($chatroom) $hasChatroom = true;
            
            $chatroom = Chatroom::where('to_user_id', $user->id)
                                ->where('from_user_id', $request->get('user_id'))->first();
            if ($chatroom) $hasChatroom = true;

            if ($hasChatroom == false) {
                $chatroom = Chatroom::create([
                    'from_user_id' => $user->id,
                    'to_user_id' => $request->get('user_id'),
                    'firebaseKey' => $request->get('firebaseKey'),
                    'last_message' => $timelineId == null? 'マッチングしました♡' : '募集マッチングしました♡',
                    'is_matching_message' => 1,
                    'read_ids' => ',' . $user->id . ',',
                    'is_show' => 3,
                    'order_at' => Carbon::now(),
                    'sender_id' => $user->id
                ]);

                $targetUserProfile = Profile::where('user_id', $request->get('user_id'))->first();

                if($timelineId) {
                    $timelineContent = $like->timeline->content;
                    $purposeName = $like->timeline->purpose->name;
                    Message::create([
                        'chatroom_id' => $chatroom->id,
                        'user_id' => $user->id,
                        'message' => '【'.$purposeName .'】' . '<br/>' . $timelineContent,
                        'is_read' => 0,
                        'created_at' => $like->created_at,
                        'updated_at' => $like->updated_at,
                        'is_image' => 2
                    ]);
                }

                Message::create([
                    'chatroom_id' => $chatroom->id,
                    'user_id' => $request->get('user_id'),
                    'message' => 'いいね  はじめまして！<br/>' . $targetUserProfile->city->name . 'の' . $targetUserProfile->job->name . ' ' . $targetUserProfile->age . '歳です。',
                    'is_read' => 0,
                    'created_at' => $like->created_at,
                    'updated_at' => $like->updated_at,
                    'is_image' => 2
                ]);

                Message::create([
                    'chatroom_id' => $chatroom->id,
                    'user_id' => $user->id,
                    'message' => 'いいね ありがとう！<br/>' . $user->profile->city->name . 'の' . $user->profile->job->name . ' ' . $user->profile->age . '歳です。',
                    'is_read' => 0,
                    'is_image' => 2
                ]);
            }
            Like::where('to_user_id', $user->id)
                ->where('from_user_id', $request->get('user_id'))
                ->where('is_reply', 0)->update([
                    'is_reply' => 1
                ]);
            Like::create([
                'from_user_id' => $user->id,
                'to_user_id' => $request->get('user_id'),
                'is_reply' => 1,
                'is_view' => 1,
                'timeline_id' => $timelineId
            ]);

            // History::where('from_user_id', $user->id)
            //        ->where('to_user_id', $request->get('user_id'))->delete();
            // History::where('to_user_id', $user->id)
            //        ->where('from_user_id', $request->get('user_id'))->delete();

            $profile = $user->Profile;
            if ($profile->gender == 1) {
                $configuration = \App\Configuration::where('name', 'like_back_point_usage')->first();
                $pointUsage = $configuration ? intval($configuration->value) : 20;

                $profile->points = $profile->points - $pointUsage;
                $profile->save();

                \App\Model\PointUsage::create([
                    'user_id' => $user->id,
                    'points' => $pointUsage,
                    'type' => 'like'
                ]);
            }

            $targetUser = User::find($request->get('user_id'));

            if ($hasChatroom) {
                if (('' . $targetUser->profile->notification_settings & 2) == '2') { // like notification
                    $content = [
                        'email' => $targetUser->email,
                        'name' => $targetUser->profile->nickname,
                        'subject' => '【DR】' . $profile->nickname . ' ' . $profile->age . '歳（' . $profile->city->name . '）いいね通知',
                        'sender_name' => $profile->nickname,
                        'sender_age' => $profile->age,
                        'sender_city' => $profile->city->name,
                        'profile_url' => config('values.site_url') . '/user/' . $user->provider_unique_id,
                        'like_page_url' => config('values.site_url') . '/likes',
                        'not_settings_url' => config('values.site_url') . '/notification-settings',
                        'site_url' => config('values.site_url')
                    ];
    
                    SendEmailJob::dispatch($content, 'email.like');
                }
            } else {
                if (('' . $targetUser->profile->notification_settings & 4) == '4') { // matching notification
                    $content = [
                        'email' => $targetUser->email,
                        'name' => $targetUser->profile->nickname,
                        'subject' => '【DR】' . $profile->nickname . ' ' . $profile->age . '歳（' . $profile->city->name . '）マッチング通知',
                        'sender_name' => $profile->nickname,
                        'sender_age' => $profile->age,
                        'sender_city' => $profile->city->name,
                        'profile_url' => config('values.site_url') . '/user/' . $user->provider_unique_id,
                        'message_page_url' => config('values.site_url') . '/messages',
                        'not_settings_url' => config('values.site_url') . '/notification-settings',
                        'site_url' => config('values.site_url')
                    ];
    
                    // $emailJob = (new SendEmailJob($content, 'email.matched'))->delay(Carbon::now()->addSeconds(1));
                    // dispatch($emailJob);
                    SendEmailJob::dispatch($content, 'email.matched');   
                }

                $newChatroomId = $chatroom->id;
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $newChatroomId = -1;
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user, 'newChatroomId' => $newChatroomId]);
    }

    public function passwordForgot(Request $request)
    {
        PasswordReset::where('email', $request->get('email'))->delete();
        $t = Str::random(32);
        $token = PasswordReset::create([
            'email' => $request->get('email'),
            'token' => $t,
            'created_at' => Carbon::now()
        ]);

        $content = [
            'email' => $request->get('email'),
            'name' => $request->get('email'),
            'subject' => '【DR】パスワード変更',
            'url' => config('values.site_url') . '/password/' . $t . '/reset',
            'site_url' => config('values.site_url')
        ];
        // $emailJob = (new SendEmailJob($content, 'email.password_reset'))->delay(Carbon::now()->addSeconds(1));
        // dispatch($emailJob);
        SendEmailJob::dispatch($content, 'email.password_reset');

        return response()->json(['message' => 'success']);
    }

    public function adminUsers(Request $request)
    {
        $filters = $request->all();

        $users = User::adminUsers($filters);

        return response()->json(['data' => $users]);
    }

    public function setCookieAfil($afilId){
        Cookie::queue('afi', $afilId, config('values.cookie_timeout'));
        return redirect()->route('index');
    }

    public function staticUsers(Request $request)
    {
        $filters = $request->all();
        $users = User::afilUsers($filters)->orderby('id', 'desc')->paginate($filters['limit']);
        return response()->json([
            'data'  => $users,
        ]);
    }

    public function createReportByAfil(Request $request)
    {
        $filters = $request->all();
        $users = User::afilUsers($filters)->get();
        $date = new Carbon();
        $reportFileName = 'AfilReport_'. $date->year . '年'. $date->month . '月' . $date->day.'日';

        return Excel::create($reportFileName, function($excel) use ($users) {
            $excel->sheet('mySheet', function($sheet) use ($users)
            {
                // 1. Header
                $header = ['affilID', 'ID', '性別','メールアドレス', '登録日'];
                $sheet->appendRow($header);
                // 2. Data
                foreach ($users as $row){
                    $cols = [];
                    $cols[] = $row->afil_id;
                    $cols[] = $row->id;
                    $cols[] = $row->profile->gender == 1 ? '男性' : '女性';
                    $cols[] = $row->email;
                    $cols[] = $row->created_at;
                    $sheet->appendRow($cols);
                }
            });
        })->download('csv');
    }

    public function approveUser(Request $request)
    {
        $data = $request->all();
        $user = User::where('id', $data['user_id'])->first();

        unset($data['user_id']);
        $profile = $user->profile;
        $profile->update($data);

        // Sending Email
        if (isset($data['image_approved'])) {
            $content = [
                'email' => $user->email,
                'name' => $profile->nickname,
                'site_url' => config('values.site_url')
            ];

            Photo::where('user_id', $user->id)
                 ->update([
                     'is_approved' => $data['image_approved']
                 ]);

            $template = '';
            if (intval($data['image_approved']) == 1) {
                $content['subject'] = '【DR】プロフィール写真が承認されました';
                $content['search_url'] = config('values.site_url') . "/search";
                $content['mypage_edit_url'] = config('values.site_url') . "/mypage/edit";
                $template = 'email.photo_approve';

                \App\Model\Notification::create([
                    'title' => 'プロフィール写真が承認されました',
                    'content' => 'プロフィール写真が承認されました。引き続き、DROSYをお楽しみください。',
                    'target' => $user->id,
                    'read_ids' => '',
                    'delivery_at' => Carbon::now()
                ]);
            } else {
                $content['subject'] = '【DR】プロフィール写真を再度ご提出ください';
                $content['search_url'] = config('values.site_url') . "/search";
                $content['mypage_edit_url'] = config('values.site_url') . "/mypage/edit";
                $content['photo_url'] = config('values.site_url') . "/photo-upload-tips";
                $template = 'email.photo_reject';

                \App\Model\Notification::create([
                    'title' => 'プロフィール写真を再度ご提出ください',
                    'content' => 'プロフィール写真が適切ではないため、再度ご提出ください。',
                    'target' => $user->id,
                    'read_ids' => '',
                    'delivery_at' => Carbon::now()
                ]);
            }

            SendEmailJob::dispatch($content, $template);
        }

        if (isset($data['age_approved'])) {
            $content = [
                'email' => $user->email,
                'name' => $profile->nickname,
                'site_url' => config('values.site_url')
            ];

            $template = '';
            if (intval($data['age_approved']) == 1) {
                $content['subject'] = '【DR】身分証明書が承認されました';
                $content['search_url'] = config('values.site_url') . "/search";
                $content['mypage_url'] = config('values.site_url') . "/mypage";
                $template = 'email.age_approve';

                \App\Model\Notification::create([
                    'title' => '身分証明書が承認されました',
                    'content' => '身分証明書が承認されました。引き続き、DROSYをお楽しみください。',
                    'target' => $user->id,
                    'read_ids' => '',
                    'delivery_at' => Carbon::now()
                ]);
            } else {
                $content['subject'] = '【DR】身分証明書を再度ご提出ください';
                $content['mypage_url'] = config('values.site_url') . "/mypage";
                $content['mypage_identity'] = config('values.site_url') . "/mypage/identity";
                $template = 'email.age_reject';

                \App\Model\Notification::create([
                    'title' => '身分証明書を再度ご提出ください',
                    'content' => '身分証明書が適切ではないため、再度ご提出ください。',
                    'target' => $user->id,
                    'read_ids' => '',
                    'delivery_at' => Carbon::now()
                ]);
            }

            SendEmailJob::dispatch($content, $template);
        }

        return response()->json(['data' => User::adminActive($user->id)]);
    }

    public function reportFromAdmin(Request $request)
    {
        $user = User::where('id', $request->get('user_id'))->first();
        DB::beginTransaction();
        try {
            Report::create([
                'from_user_id' => JWTAuth::parseToken()->authenticate()->id,
                'to_user_id' => $user->id,
                'content' => $request->get('content'),
                'is_admin_alert' => 1
            ]);

            $profile = $user->profile;
            $profile->alert_status = $request->get('alert_status');
            $profile->save();

            $user->alert_viewed = 0;
            $user->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        return response()->json(['data' => User::adminActive($user->id)]);
    }

    public function adminUserDetail(Request $request)
    {
        $user = User::where('provider_unique_id', $request->get('user_id'))
                    ->with('profile')
                    ->with('photos')
                    ->with('documents')
                    ->with('reports')
                    ->with('last90Reports')
                    ->with('adminReports')
                    ->with('mySendReports')
                    ->with('blocks')->first();

        return response()->json(['data' => $user]);
    }

    public function approveOneImage(Request $request)
    {
        $data = $request->all();
        $user = User::where('id', $data['user_id'])->first();
        $profile = $user->profile;

        DB::beginTransaction();
        try {
            Photo::where('id', $data['photo_id'])
                 ->where('type', $data['type'])
                 ->update([
                     'is_approved' => $data['image_approved']
                 ]);
            
            if ($data['type'] == 'main_photo') {
                $content = [
                    'email' => $user->email,
                    'name' => $profile->nickname,
                    'site_url' => config('values.site_url')
                ];
    
                $template = '';
                if (intval($data['image_approved']) == 1) {
                    $profile->update([
                        'image_approved' => $data['image_approved']
                    ]);

                    $content['subject'] = '【DR】プロフィール写真が承認されました';
                    $content['mypage_edit_url'] = config('values.site_url') . "/mypage/edit";
                    $content['search_url'] = config('values.site_url') . "/search";
                    $template = 'email.photo_approve';

                    \App\Model\Notification::create([
                        'title' => 'プロフィール写真が承認されました',
                        'content' => 'プロフィール写真が承認されました。引き続き、DROSYをお楽しみください。',
                        'target' => $data['user_id'],
                        'read_ids' => '',
                        'delivery_at' => Carbon::now()
                    ]);
                } else {
                    $profile->update([
                        'image_approved' => $data['image_approved']
                    ]);
                    
                    $content['subject'] = '【DR】プロフィール写真を再度ご提出ください';
                    $content['photo_url'] = config('values.site_url') . "/photo-upload-tips";
                    $content['mypage_edit_url'] = config('values.site_url') . "/mypage/edit";
                    $content['search_url'] = config('values.site_url') . "/search";
                    $template = 'email.photo_reject';

                    \App\Model\Notification::create([
                        'title' => 'プロフィール写真を再度ご提出ください',
                        'content' => 'プロフィール写真が適切ではないため、再度ご提出ください。',
                        'target' => $data['user_id'],
                        'read_ids' => '',
                        'delivery_at' => Carbon::now()
                    ]);
                }

                SendEmailJob::dispatch($content, $template);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        return response()->json(['data' => User::adminActive($user->id)]);
    }

    public function adminAddPoint(Request $request)
    {
        $user = User::find($request->get('user_id'));

        DB::beginTransaction();
        try {
            Payment::create([
                'user_id' => $user->id,
                'type' => 'point_purchase',
                'amount' => 0,
                'points' => $request->get('points'),
                'memo' => 'admin_point_purchase'
            ]);

            \App\Model\PointUsage::create([
                'user_id' => $user->id,
                'type' => 'admin_point_purchase',
                'amount' => 0,
                'points' => $request->get('points')
            ]);

            $user->profile->points = $user->profile->points + intval($request->get('points'));
            $user->profile->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        return response()->json(['data' => User::adminActive($user->id)]);
    }

    public function adminUpdate(Request $request)
    {
        $user = User::find($request->get('user_id'));

        $data = $request->all();

        if (isset($data['memo'])) {
            $user->profile->memo = $data['memo'];
            $user->profile->save();
        } else if (isset($data['nickname'])) {
            $user->profile->nickname = $data['nickname'];
            $user->profile->save();
        } else if (isset($data['work'])) {
            $user->profile->work = $data['work'];
            $user->profile->save();
        } else if (isset($data['content'])) {
            $user->profile->content = $data['content'];
            $user->profile->save();
        } else if (isset($data['status'])) {
            $user->is_deleted = $data['status'];
            if (intval($data['status']) == 1) { // deleted
                $user->email = explode("___", $user->email)[0] . '___' . Str::random(6);
            } else if (intval($data['status']) == 0) { // reuse
                $user->email = explode("___", $user->email)[0];
            }

            if (intval($data['status']) >= 1) {
                if (sizeof($user->memberships) > 0) {
                    foreach ($user->memberships as $membership) {
                        $membership->auto_update = 0;
                        $membership->is_enabled = 0;
                        $membership->save();
                    }
                }
            }
            $user->save();
        } else if (isset($data['birthday'])) {
            $birthday = new Carbon($data['birthday']);

            $user->profile->birthday = $birthday->format('Y-m-d');
            $user->profile->age = $birthday->age;
            $user->profile->save();
        }

        return response()->json(['data' => User::adminActive($user->id)]);
    }

    public function reports(Request $request)
    {
        $data = $request->all();

        $date = Carbon::parse($data['current'] . '-01');

        // if (intval($data['next']) == -1) {
        //     $date = $date->addDays(-2);
        // } else if (intval($data['next']) == 1) {
        //     $date = $date->addDays(32);
        // }

        $date = $date->addMonths(intval($data['next']));

        $days = $date->daysInMonth;

        $newUsers = User::where('created_at', '>=', Carbon::parse($date->year . '/' . ($date->month) . '/01 00:00:00')->format('Y-m-d H:i:s'))
                        ->where('created_at', '<=', Carbon::parse($date->year . '/' . ($date->month) . '/' . $days . ' 23:59:59')->format('Y-m-d H:i:s'))->get();

        $deleteUsers = User::where('updated_at', '>=', Carbon::parse($date->year . '/' . ($date->month) . '/01 00:00:00')->format('Y-m-d H:i:s'))
                        ->where('updated_at', '<=', Carbon::parse($date->year . '/' . ($date->month) . '/' . $days . ' 23:59:59')->format('Y-m-d H:i:s'))
                        ->where('is_deleted', 1)->get();

        $res = [];
        $totalNews = 0;
        $totalQuits = 0;
        for ($i = 1; $i <= $days; $i++) {
            $news = 0;
            $deletes = 0;
            foreach ($newUsers as $user) {
                if (Carbon::parse($user->created_at)->day == $i) $news++;
            }

            foreach ($deleteUsers as $user) {
                if (Carbon::parse($user->updated_at)->day == $i) $deletes++;
            }

            $res[] = [
                'news' => $news,
                'quits' => $deletes,
                'date' => Carbon::parse($date->year . '/' . ($date->month) . '/' . $i)->format("d日")
            ];

            $totalNews += $news;
            $totalQuits += $deletes;
        }

        return response()->json(['data' => $res, 'total' => ['news' => $totalNews, 'quits' => $totalQuits]]);
    }

    public function telecomCheck(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        
        if ($request->get('type') == 'credit_card') {
            $user = User::active($user->id);

            return response()->json(['data' => $user]);
        }

        $telecomResponse = TelecomResponse::where('sendid', $user->card->sendid)
                                          ->where('type', $request->get('type'))->first();

        if ($telecomResponse) {
            return response()->json(['data' => $telecomResponse->response]);
        }

        return response()->json(['data' => 'fail']);
    }

    public function telecomReset(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $card = Card::where('user_id', $user->id)->first();
        if ($card) {
            $card->cont = $request->get('reset');

            $card->save();
        }

        return response()->json(['data' => User::active($user->id)]);
    }

    public function pay(Request $request)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            if ($user) {
                if (is_null($user->card)) {
                    return response()->json(['data' => 'error']);
                }

                $telecomResponse = TelecomResponse::where('sendid', $user->card->sendid)
                                                  ->where('type', $request->get('type'))->first();
                if (!$telecomResponse) {
                    $telecomResponse = TelecomResponse::create([
                        'sendid' => $user->card->sendid,
                        'type' => $request->get('type')
                    ]);
                }

                $telecomResponse->response = 'processing';
                $telecomResponse->save();
                
                $client = new \GuzzleHttp\Client();
                $response = $client->request('GET', 'https://secure.telecomcredit.co.jp/inetcredit/secure/cont.pl?clientip=' . config('values.clientip') . '&sendid=' . $user->card->sendid . '&money=' . $request->get('money'));

                return response()->json(['data' => 'success', 'response' => $response]);
            }
        } catch (Exception $e) {
            return response()->json(['data' => 'error']);
        }

        return response()->json(['data' => 'no user']);
    }

    public function cardResponse(Request $request)
    {
        $data = $request->all();
        $userId = intval($data['sendid']);
        $card = Card::where('user_id', $userId)->first();

        if (!$card) {
            $card = Card::create([
                'user_id' => $userId
            ]);
        }

        if ($data['rel'] == 'yes') {
            if ($data['cont'] == 'no') { // card validation
                $card->name = $data['username'] ? $data['username'] : '';
                $card->username = $data['username'] ? $data['username'] : '';
                $card->telno = $data['telno'] ? $data['telno'] : '';
                $card->sendid = $data['sendid'] ? $data['sendid'] : '';
                $card->cont = $data['cont'] ? $data['cont'] : '';
                $card->email = $data['email'] ? $data['email'] : '';
                $card->card_number = $data['email'] ? $data['email'] : '';
                
                $card->save();
            } else if ($data['cont'] == 'yes') { // Payment
                $telecomResponse = TelecomResponse::where('sendid', $data['sendid'])->first();
                if ($telecomResponse) {
                    $telecomResponse->response = $data['rel'];
                    $telecomResponse->save();
                }
            }
        } else {
            if ($data['cont'] == 'yes') { // Payment
                $telecomResponse = TelecomResponse::where('sendid', $data['sendid'])->first();
                if ($telecomResponse) {
                    $telecomResponse->response = $data['rel'];
                    $telecomResponse->save();
                }
            }
        }

        return response()->make('SuccessOK');
    }

    public function cardRemoveResponse(Request $request)
    {
        $data = $request->all();
        $userId = intval($data['sendid']);
        $card = Card::where('user_id', $userId)->first();

        if (!$card) {
            $card = Card::create([
                'user_id' => $userId
            ]);
        }

        if ($data['rel'] == 'yes') {
            if ($data['cont'] == 'no') { // card validation
                $card->name = $data['username'] ? $data['username'] : '';
                $card->username = $data['username'] ? $data['username'] : '';
                $card->telno = $data['telno'] ? $data['telno'] : '';
                $card->sendid = $data['sendid'] ? $data['sendid'] : '';
                $card->cont = $data['cont'] ? $data['cont'] : '';
                $card->email = $data['email'] ? $data['email'] : '';
                $card->card_number = $data['email'] ? $data['email'] : '';
                
                $card->save();
            }
        }

        return response()->make('SuccessOK');
    }

    public function checkBirthday(Request $request)
    {
        $birthday = $request->get("birthday");
        $gender = $request->get('gender');

        $users = User::whereIn('id', function($query) use ($birthday, $gender) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('gender', $gender)
                        ->where('birthday', $birthday);
                })->where('is_deleted', 2)->with('profile')->with('photos')->get();
        
        return response()->json(['data' => $users]);
    }

    public function telecomTrends(Request $request)
    {
        $data = $request->all();

        $packages = Package::orderBy('price')->get();
        $packageTrends = array();
        foreach ($packages as $package) {
            $query = Payment::where('type', 'point_purchase')
                            ->where('amount', $package->price)
                            ->whereIn('user_id', function($query) use ($data, $request) {
                                $query->select('user_id')
                                    ->from(with(new \App\Model\Profile)->getTable())
                                    ->where('gender', 1);
                                
                                if (intval($data['job_id']) > 0) {
                                    $query->where('job_id', $data['job_id']);
                                }

                                if (intval($data['city_id']) > 0) {
                                    $query->where('city_id', $data['city_id']);
                                }

                                if (intval($data['salary_id']) > 0) {
                                    $query->where('salary_id', $data['salary_id']);
                                }

                                if ($request->get('start_register_date')) {
                                    $query->where('created_at', '>=', $request->get('start_register_date'));
                                }
                        
                                if ($request->get('end_register_date')) {
                                    $query->where('created_at', '<=', $request->get('end_register_date'));
                                }
                            });
            if ($request->get('start_date')) {
                $query->where('created_at', '>=', $request->get('start_date'));
            }
    
            if ($request->get('end_date')) {
                $query->where('created_at', '<=', $request->get('end_date'));
            }

            $packageTrends[] = [
                'price' => $package->price,
                'purchase' => $query->count()
            ];
        }

        $memberships = Membership::whereIn('type', ['paid', 'platium'])
                                 ->orderBy('id')->get();
        $membershipTrends = array();
        foreach ($memberships as $membership) {
            $query = Payment::where('type', 'membership_payment')
                            ->where('amount', $membership->price * $membership->period)
                            ->where('memo', 'first_purchase')
                            ->whereIn('user_id', function($query) use ($data, $request) {
                                $query->select('user_id')
                                    ->from(with(new \App\Model\Profile)->getTable())
                                    ->where('gender', 1);
                                
                                if (intval($data['job_id']) > 0) {
                                    $query->where('job_id', $data['job_id']);
                                }

                                if (intval($data['city_id']) > 0) {
                                    $query->where('city_id', $data['city_id']);
                                }

                                if (intval($data['salary_id']) > 0) {
                                    $query->where('salary_id', $data['salary_id']);
                                }

                                if ($request->get('start_register_date')) {
                                    $query->where('created_at', '>=', $request->get('start_register_date'));
                                }
                        
                                if ($request->get('end_register_date')) {
                                    $query->where('created_at', '<=', $request->get('end_register_date'));
                                }
                            });

            if ($request->get('start_date')) {
                $query->where('created_at', '>=', $request->get('start_date'));
            }
    
            if ($request->get('end_date')) {
                $query->where('created_at', '<=', $request->get('end_date'));
            }
            
            $membershipTrends[] = [
                'price' => $membership->price,
                'type' => $membership->type,
                'period' => $membership->period,
                'purchase' => $query->count()
            ];
        }

        return response()->json(['packages' => $packageTrends, 'memberships' => $membershipTrends]);
    }

    public function skipUserLike(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        DB::beginTransaction();
        try {
            Like::where('from_user_id', $request->get('user_id'))
                        ->where('to_user_id', $user->id)->update([
                            'is_reply' => 1,
                            'is_view' => 1
                        ]);

            History::where('from_user_id', $request->get('user_id'))
                        ->where('to_user_id', $user->id)->update([
                            'is_reply' => 1
                        ]);
           
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $newChatroomId = -1;
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }
}
