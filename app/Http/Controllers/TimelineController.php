<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Timeline;
use App\User;
use Illuminate\Support\Facades\DB;
use Exception;

class TimelineController extends Controller
{
    public function allTimelines(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $filters = $request->all();
        $filters['ids'] = \App\Model\Profile::where('gender', $user->Profile->gender == 1 ? 2 : 1)
                                 ->select('user_id')->get();

        $filters['user_id'] = $user->id;
        $results = Timeline::search($filters);
        
        return response()->json(['data' => $results]);
    }

    public function allAdminTimelines(Request $request)
    {
        $query = Timeline::where('is_deleted', 0);

        if ($request->get('nickname') != '') {
            $query->whereIn('user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('nickname', 'like', '%' . $request->get('nickname') . '%');
                });
        }

        if (intval($request->get('gender')) > 0) {
            $query->whereIn('user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('gender', $request->get('gender'));
                });
        }

        if ($request->get('userId') != '') {
            $query->where('user_id', $request->get('userId'));
        }

        if (intval($request->get('purpose_id')) > 0) {
            $query->where('purpose_id', $request->get('purpose_id'));
        }

        if (intval($request->get('city_id')) > 0) {
            $query->where('city_id', $request->get('city_id'));
        }

        if ($request->get('start_date')) {
            $query->where('created_at', ">=", $request->get('start_date'));
        }

        if ($request->get('end_date')) {
            $query->where('created_at', '<=', $request->get('end_date'));
        }

        $timelines = $query->with('user')
                            ->with('purpose')
                            ->with('city')
                            ->orderBy('created_at', 'desc')
                            ->paginate($request->get('limit'));

        $users = \App\User::whereIn('id', function ($query) {
            $query->select('user_id')
                ->from(with(new \App\Model\Timeline)->getTable())
                ->where('id', ">=", 1);
        })->select(['id'])->with('profile')->get();
        
        return response()->json(['data' => $timelines, 'users' => $users]);
    }

    public function addTimeline(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        DB::beginTransaction();
        try {
            Timeline::create([
                'user_id' => $user->id,
                'purpose_id' => 1,
                'city_id' => $user->Profile->city->id,
                'content' => $request->get('content'),
                'place' => $request->get('place'),
                'recruitment_date' => $request->get('recruitment_date')
            ]);

            $profile = $user->Profile;
            if ($profile->gender == 1) {
                $configuration = \App\Configuration::where('name', 'timeline_point_usage')->first();
                $pointUsage = $configuration ? intval($configuration->value) : 20;

                $profile->points = $profile->points - $pointUsage;
                $profile->save();

                \App\Model\PointUsage::create([
                    'user_id' => $user->id,
                    'points' => $pointUsage,
                    'type' => 'timeline'
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        $filters = [
            'limit' => 20,
            'currentTimelinesNum' => $request->get('currentTimelinesNum'),
            'purpose_id' => $request->get('filter_purpose_id'),
            'city_id' => $request->get('filter_city_id')
        ];

        $filters['ids'] = \App\Model\Profile::where('gender', $user->Profile->gender == 1 ? 2 : 1)
                                 ->select('user_id')->get();
        $filters['user_id'] = $user->id;
        $timelines = Timeline::search($filters);

        return response()->json(['data' => $user, 'timelines' => $timelines]);
    }

    public function adminDeleteTimeline(Request $request)
    {
        DB::beginTransaction();
        try {
            Timeline::find($request->get('id'))->update([
                'is_deleted' => 1
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        return response()->json(['data' => 'success']);
    }

    public function deleteTimeline(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        
        DB::beginTransaction();
        try {
            Timeline::find($request->get('id'))->update([
                'is_deleted' => 1
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        $filters = [
            'limit' => 20,
            'currentTimelinesNum' => $request->get('currentTimelinesNum'),
            'purpose_id' => $request->get('filter_purpose_id'),
            'city_id' => $request->get('filter_city_id')
        ];

        $filters['ids'] = \App\Model\Profile::where('gender', $user->Profile->gender == 1 ? 2 : 1)
                                 ->select('user_id')->get();

        $filters['user_id'] = $user->id;
        $timelines = Timeline::search($filters);

        return response()->json(['data' => $user, 'timelines' => $timelines]);
    }
}
