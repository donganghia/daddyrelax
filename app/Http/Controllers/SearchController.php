<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\User;
use App\Model\Profile;
use App\Model\Like;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;
use App\Model\History;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $filters = $request->all();
        $filters['gender'] = $user->Profile->gender == 1 ? 2 : 1;
        $filters['user_id'] = $user->id;

        $users = User::search($filters);

        return response()->json(['results' => $users]);
    }

    public function getUser(Request $request)
    {
        $user = User::where('provider_unique_id', $request->get('user_id'))
                     ->with('profile')
                     ->with('photos')
                     ->with('documents')
                     ->with('reports')
                     ->withCount('last90Reports')
                     ->with('blocks')->first();
        
        return response()->json(['data' => $user]);
    }

    public function likes(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $fromUsers = Like::where('to_user_id', $user->id)
                    ->select(['from_user_id', 'id', 'created_at'])
                    ->orderBy('from_user_id', 'desc')
                    ->orderBy('created_at', 'desc')->get();
        $ids = array();
        $fromUserIDs = array();
        foreach ($fromUsers as $fromUser) {
            if (! in_array($fromUser->from_user_id, $fromUserIDs)) {
                $ids[] = $fromUser->id;
                $fromUserIDs[] = $fromUser->from_user_id;
            }
        }

        $userId = $user->id;

        $users = Like::where('to_user_id', $user->id)
                     ->where('is_reply', 0)
                     ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('block_user_id')
                            ->from(with(new \App\Model\Block)->getTable())
                            ->where('user_id', $userId);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('user_id')
                            ->from(with(new \App\Model\Block)->getTable())
                            ->where('block_user_id', $userId);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('id')
                            ->from(with(new \App\User)->getTable())
                            ->where('is_deleted', ">=", 1);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('from_user_id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('to_user_id', $userId);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('to_user_id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('from_user_id', $userId);
                    })
                     ->whereIn('id', $ids)
                     ->orderBy('created_at', 'desc')
                     ->with('from')->paginate($request->get('limit'));

        return response()->json(['data' => $users]);
    }

    public function viewHistories(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $fromUsers = History::where('to_user_id', $user->id)
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('block_user_id')
                            ->from(with(new \App\Model\Block)->getTable())
                            ->where('user_id', $userId);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('user_id')
                            ->from(with(new \App\Model\Block)->getTable())
                            ->where('block_user_id', $userId);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('id')
                            ->from(with(new \App\User)->getTable())
                            ->where('is_deleted', ">=", 1);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('from_user_id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('to_user_id', $userId);
                    })
                    ->whereNotIn('from_user_id', function($query) use ($userId) {
                        $query->select('to_user_id')
                            ->from(with(new \App\Model\Chatroom)->getTable())
                            ->where('from_user_id', $userId);
                    })
                    ->select(['from_user_id', 'id', 'created_at'])
                    ->orderBy('from_user_id', 'desc')
                    ->orderBy('created_at', 'desc')->get();
        $ids = array();
        $fromUserIDs = array();
        foreach ($fromUsers as $fromUser) {
            if (! in_array($fromUser->from_user_id, $fromUserIDs)) {
                $ids[] = $fromUser->id;
                $fromUserIDs[] = $fromUser->from_user_id;
            }
        }

        $users = History::where('to_user_id', $user->id)
                     ->where('is_reply', 0)
                     ->whereIn('id', $ids)
                     ->orderBy('created_at', 'desc')
                     ->with('from')->paginate($request->get('limit'));

        return response()->json(['data' => $users, 'user' => User::active($user->id)]);
    }

    public function openLikePage(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        Like::where('to_user_id', $user->id)
            ->update(['is_view' => 1]);
        
        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }
}
