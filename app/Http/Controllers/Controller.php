<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Model\Photo;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function uploadPhoto(Request $request, $userId, $size = 500)
    {
        $data = $request->all();
        $image = $data['image'];

        $path = Storage::putFile('public/photos', $image);
        $parsePath = str_replace('public/photos/', 'photos/', $path);
        $thumbnail = Image::make( Storage::disk('public')->get($parsePath));

        if ($thumbnail->width() < $size || $thumbnail->height() < $size) {
            $thumbnail = $thumbnail->resize($thumbnail->width(), $thumbnail->height())->stream();
        } else {
            if ($thumbnail->width() > $thumbnail->height()) {
                $width = intval(doubleval($thumbnail->width()) / (doubleval($thumbnail->height()) / doubleval($size)));
                $thumbnail = $thumbnail->resize($width, $size)->stream();
            } else {
                $height = intval(doubleval($thumbnail->height()) / (doubleval($thumbnail->width()) / doubleval($size)));
                $thumbnail = $thumbnail->resize($size, $height)->stream();
            }    
        }
        
        $thumbPath = str_replace('photos/', 'photos/thumbnail_', $parsePath);
        Storage::disk('public')->put($thumbPath, $thumbnail);

        return ['thumb_url' => '/storage/' . $thumbPath, 'photo_url' => '/storage/' . $parsePath];
    }
}
