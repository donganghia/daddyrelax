<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Report;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function allReports(Request $request)
    {
        $query = Report::where('is_admin_alert', 0);

        if ($request->get('from_nickname') != '') {
            $query->whereIn('from_user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('nickname', 'like', '%' . $request->get('from_nickname') . '%');
                });
        }

        if ($request->get('to_nickname') != '') {
            $query->whereIn('to_user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('nickname', 'like', '%' . $request->get('to_nickname') . '%');
                });
        }

        if (intval($request->get('from_gender')) > 0) {
            $query->whereIn('from_user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('gender', $request->get('from_gender'));
                });
        }

        if (intval($request->get('to_gender')) > 0) {
            $query->whereIn('to_user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('gender', $request->get('to_gender'));
                });
        }

        if ($request->get('from_user_id') != '') {
            $query->where('from_user_id', $request->get('from_user_id'));
        }

        if ($request->get('to_user_id') != '') {
            $query->where('to_user_id', $request->get('to_user_id'));
        }

        if ($request->get('start_date')) {
            $query->where('created_at', '>=', $request->get('start_date'));
        }

        if ($request->get('end_date')) {
            $query->where('created_at', '<=', $request->get('end_date'));
        }

        $reports = $query->with('from')
                         ->with('to')
                         ->orderBy('created_at', 'desc')
                         ->paginate($request->get('limit'));
        
        $froms = \App\User::whereIn('id', function ($query) {
                    $query->select('from_user_id')
                        ->from(with(new \App\Model\Report)->getTable())
                        ->where('id', ">=", 1);
        })->select(['id'])->with('profile')->get();

        $tos = \App\User::whereIn('id', function ($query) {
            $query->select('to_user_id')
                ->from(with(new \App\Model\Report)->getTable())
                ->where('id', ">=", 1);
        })->select(['id'])->with('profile')->get();
        
        return response()->json(['data' => $reports, 'froms' => $froms, 'tos' => $tos]);
    }

    public function allReportRanks(Request $request)
    {
        $fromQuery = Report::where('is_admin_alert', 0);
        $toQuery = Report::where('is_admin_alert', 0);

        if (intval($request->get('gender')) > 0) {
            $fromQuery->whereIn('from_user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('gender', $request->get('gender'));
                });

            $toQuery->whereIn('to_user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('gender', $request->get('gender'));
                });
        }

        $fromQuery->whereIn('id', function($query) use ($request) {
            $query->select('id')
                  ->from(with(new \App\Model\Report)->getTable());
            
            if ($request->get('start_date')) {
                $query->where('created_at', '>=', $request->get('start_date'));
            }
    
            if ($request->get('end_date')) {
                $query->where('created_at', '<=', $request->get('end_date'));
            }
        });

        $toQuery->whereIn('id', function($query) use ($request) {
            $query->select('id')
                  ->from(with(new \App\Model\Report)->getTable());
            
            if ($request->get('start_date')) {
                $query->where('created_at', '>=', $request->get('start_date'));
            }
    
            if ($request->get('end_date')) {
                $query->where('created_at', '<=', $request->get('end_date'));
            }
        });

        $tos = $toQuery->select(DB::raw('count(*) as report_count, to_user_id'))
                    ->groupBy('to_user_id')
                    ->orderBy('report_count', 'desc')
                    ->with('to')
                    ->get();

        $froms = $fromQuery->select(DB::raw('count(*) as report_count, from_user_id'))
                    ->groupBy('from_user_id')
                    ->orderBy('report_count', 'desc')
                    ->with('from')
                    ->get();

        $config = \App\Configuration::where('name', 'last_days')->first();
        $lastDays = $config ? 0 - intval($config->value) : -90;

        $from90s = $fromQuery->where('created_at', '>=', Carbon::now()->addDays($lastDays)->format('Y-m-d 00:00:00'))->get();

        $to90s = $toQuery->where('created_at', '>=', Carbon::now()->addDays($lastDays)->format('Y-m-d 00:00:00'))->get();

        foreach ($froms as $from) {
            $from->report_count_90 = 0;
            foreach ($from90s as $from90) {
                if ($from->from_user_id == $from90->from_user_id) {
                    $from->report_count_90 = $from90->report_count;
                }
            }
        }

        foreach ($tos as $to) {
            $to->report_count_90 = 0;
            foreach ($to90s as $to90) {
                if ($to->to_user_id == $to90->to_user_id) {
                    $to->report_count_90 = $to90->report_count;
                }
            }
        }
        
        return response()->json(['froms' => $froms, 'tos' => $tos]);
    }
}
