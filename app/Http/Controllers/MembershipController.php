<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Master\Membership;
use App\Model\Payment;
use App\Model\UserMembership;
use App\User;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;

class MembershipController extends Controller
{
    public function allMemberships(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $all = Membership::orderBy('sort_no')->get();
        
        return response()->json(['data' => $all]);
    }

    public function upgradePlan(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $plan = Membership::where('id', $request->get('membership_id'))->first();

        DB::beginTransaction();
        try {
            $expireAt = Carbon::now();
            if ($plan->type == 'paid') {
                $paidPlanIDs = Membership::where('type', 'paid')->select('id')->get();
                UserMembership::where('user_id', $user->id)
                              ->whereIn('membership_id', $paidPlanIDs)->delete();
                $expireAt = $expireAt->addDays($plan->period * intval(config('values.month'))); 
            } else {
                UserMembership::where('user_id', $user->id)
                              ->where('membership_id', $plan->id)->delete();
                $expireAt = $expireAt->addDays(intval(config('values.month')) * $plan->period); 
            }

            UserMembership::create([
                'user_id' => $user->id,
                'membership_id' => $plan->id,
                'auto_update' => 1,
                'is_enabled' => 1,
                'expire_at' => $expireAt->format("Y-m-d 00:00:00"),
                'last_paid_at' => Carbon::now()->format("Y-m-d 00:00:00")
            ]);

            Payment::create([
                'user_id' => $user->id,
                'type' => 'membership_payment',
                'amount' => $plan->price * $plan->period,
                'points' => $plan->points,
                'memo' => 'first_purchase'
            ]);

            \App\Model\PointUsage::create([
                'user_id' => $user->id,
                'type' => $plan->type . '_membership_payment',
                'amount' => $plan->price,
                'points' => $plan->points
            ]);

            $profile = $user->Profile;
            $profile->membership_id = $plan->id;
            $profile->points += $plan->points;
            $profile->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function updatePlan(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $membership = UserMembership::where('id', $request->get('membership_id'))->first();
     
        DB::beginTransaction();
        try {
            $membership->auto_update = $request->get('auto_update');
            if ($membership->auto_update == 0) {
                $membership->auto_update_at = Carbon::now();
            }
            $membership->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function getUserMemberships(Request $request)
    {
        $memberships = UserMembership::where('user_id', $request->get('user_id'))
                                     ->with('membership')->orderBy('membership_id')->get();
        $payments = \App\Model\Payment::where('user_id', $request->get('user_id'))
                                      ->where('type', 'membership_payment')
                                      ->orderBy('created_at', 'desc')->paginate($request->get('limit'));

        return response()->json(['data' => $memberships, 'payments' => $payments]);
    }
}
