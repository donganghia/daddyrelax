<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Favorite;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;
use App\User;

class FavoriteController extends Controller
{
    public function removeFavorite(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        Favorite::where('user_id', $user->id)
             ->where('favorite_user_id', $request->get('favorite_user_id'))->delete();

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function addFavorite(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        Favorite::create([
            'user_id' => $user->id,
            'favorite_user_id' => $request->get('favorite_user_id')
        ]);

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }
}
