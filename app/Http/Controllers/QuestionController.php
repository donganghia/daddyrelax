<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Question;
use App\Model\Answer;
use App\Model\Master\QuestionCategory;
use App\User;
use App\Jobs\SendEmailJob;

class QuestionController extends Controller
{
    public function allQuestions(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $questions = QuestionCategory::whereIn('gender', [0, $user->Profile->gender])
                                     ->with(array('questions' => function($query) {
                                         $query->where('questions.publish', 1)
                                               ->with('answer');
                                     }))->orderBy('sort_no')->orderBy('id')->get();
        
        return response()->json(['data' => $questions]);
    }

    public function askQuestion(Request $request)
    {
        $content['body'] = $request->get('content');
        $content['email'] = config('values.admin_email');
        $content['subject'] = $request->get('category');

        if ($request->get('email') == '') {
            Question::create([
                'category_id' => $request->get('category_id'),
                'user_id' => JWTAuth::parseToken()->authenticate()->id,
                'content' => $request->get('content'),
                'publish' => 0
            ]);
            $content['hasUser'] = true;
            $user = JWTAuth::parseToken()->authenticate();
            $content['user_email'] = $user->email;
            $content['userId'] = $user->id;
            $content['nickname'] = $user->profile->nickname;
        } else {
            Question::create([
                'category_id' => $request->get('category_id'),
                'content' => $request->get('content'),
                'email' => $request->get('email'),
                'publish' => 0
            ]);
            $content['hasUser'] = false;
            $content['user_email'] = $request->get('email');
        }

        SendEmailJob::dispatch($content, 'email.question');

        return response()->json(['data' => 'success']);
    }
}
