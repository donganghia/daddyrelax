<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Master\VipImage;
use App\User;

class VipImageController extends Controller
{
    public function allVipImages(Request $request)
    {
        $images = VipImage::all();

        return response()->json(['data' => $images]);
    }
}
