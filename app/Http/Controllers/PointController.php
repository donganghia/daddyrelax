<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Master\Cost;
use App\Model\Master\Package;
use App\Model\Payment;
use App\User;
use App\Model\Bank;
use App\Model\Card;
use App\Model\PointUsage;
use Illuminate\Support\Facades\DB;
use Exception;

class PointController extends Controller
{
    public function allPackages(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $packages = Package::orderBy('sort_no')->get();
        
        return response()->json(['data' => $packages]);
    }

    public function purchasePoints(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $package = Package::where('id', $request->get('package_id'))->first();
        $profile = $user->Profile;

        DB::beginTransaction();
        try {
            $profile->points += $package->points;
            $profile->save();

            Payment::create([
                'user_id' => $user->id,
                'type' => 'point_purchase',
                'amount' => $package->price,
                'points' => $package->points
            ]);

            \App\Model\PointUsage::create([
                'user_id' => $user->id,
                'type' => 'point_purchase',
                'amount' => $package->price,
                'points' => $package->points
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }

        $user = User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function getUserPointPurchases(Request $request)
    {
        $payments = \App\Model\PointUsage::where('user_id', $request->get('user_id'))
                           ->orderBy('created_at', 'desc')
                           ->paginate($request->get('limit'));
        $card = Card::where('user_id', $request->get('user_id'))->first();

        $cardNumber = '***********';
        if ($card) $cardNumber .= substr($card->card_number, -4);
        
        return response()->json(['data' => $payments, 'cardNumber' => $cardNumber]);
    }

    public function history(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $filters = [
            'limit' => $request->get('limit'),
            'page' => $request->get('page'),
            'user_id' => $user->id
        ];
        $pointUsages = PointUsage::search($filters);
        return response()->json(['data' => $pointUsages]);
    }

    public function allCosts(Request $request)
    {
        $costs = Cost::all();
        return response()->json(['data' => $costs]);
    }

    public function saveCost(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        DB::beginTransaction();
        try {
            $profile = $user->Profile;
            if ($profile->gender == 2) {
                $profile->cost = $request->get('cost');
                $profile->updated_at = date("Y-m-d H:i:s");
                $profile->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'エラーが発生しました。'], 422);
        }

        return response()->json(['status' => true]);
    }

    public function saveBanks(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $bank = Bank::where('user_id', $user->id)->first();

        if (!$bank) {
            $bank = Bank::create([
                'user_id' => $user->id,
                'bank_name' => $request->bank_name,
                'branch_code' => $request->branch_code,
                'branch_name' => $request->branch_name,
                'account_number' => $request->account_number,
                'account_name' => $request->account_name,
            ]);
        } else {
            $bank->bank_name = $request->bank_name;
            $bank->branch_code = $request->branch_code;
            $bank->branch_name = $request->branch_name;
            $bank->account_number = $request->account_number;
            $bank->account_name = $request->account_name;

            $bank->save();
        }

        return response()->json(['status' => true]);
    }

    public function exchangePoints(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $profile = $user->Profile;
        if($profile->points > 0) {
            //Create point usage
            $pointUsage = PointUsage::create([
                'user_id' => $user->id,
                'points' => $profile->points,
                'amount' => 0,
                'type' => 'exchange_points'
            ]);

            //update profule
            $profile->points;
            $profile->points = 0;
            $profile->save();
        }
       
        return response()->json(['status' => true]);
    }

    public function saveReceipt(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

       //$request->get('email');

        return response()->json(['status' => true]);
    }
}
