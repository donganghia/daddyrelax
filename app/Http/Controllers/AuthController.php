<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Notifications\Activation;
use App\Notifications\Activated;
use App\Notifications\PasswordReset;
use App\Notifications\PasswordResetted;
use App\Jobs\SendEmailJob;
use App\Model\Token;
use App\Model\Notification;
use Illuminate\Support\Str;
use App\User;
use App\Model\Profile;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Model\Afb;
use Exception;
use Cookie;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if ($request->get('no_password') != null) {
                $user = \App\User::where("email", $request->get('email'))->first();
                $token = JWTAuth::fromUser($user);
            } else {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['message' => 'メールアドレス、またはパスワードが誤っています。'], 422);
                }
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'メールアドレス、またはパスワードが誤っています。'], 500);
        }

        $user = \App\User::whereEmail(request('email'))->first();

        if ($request->get('is_admin')) {
            if ($user->is_admin == 0) {
                return response()->json(['message' => 'メールアドレス、またはパスワードが誤っています。'], 500);
            }
        }

        if($user->status == 'pending_activation')
            return response()->json(['message' => 'まだこのメールアドレスが利用できません。'], 422);

        // if ($user->status == 'activated_email_change') {
        //     return response()->json(['message' => '新しいのメールアドレスをご確認してください。'], 422);
        // }

        if ($user->is_deleted == 1) {
            return response()->json(['message' => 'このメールアドレスは退会しました。'], 422);
        }

        // if ($user->is_deleted == 2) {
        //     return response()->json(['message' => 'このメールアドレスは強制退会しました。'], 422);
        // }

        $user->last_login_at = Carbon::now();
        $user->is_logged_in = 1;
        $user->update();

        $profile = $user->Profile;
        $user = User::active($user->id);
        $user->alertNotification = Notification::whereIn('target', ['all', '' . $user->id, $user->profile->gender === 1 ? 'male' : 'female'])
                ->where('show_alert', 1)
                ->where('alert_read_ids', 'not like', '%,' . $user->id . ',%')
                ->where('delivery_at', '>=', $user->created_at)
                ->where('is_delivered', 1)->count();

        return response()->json(['message' => 'ログインしました。','token' => $token, 'status' => $user->status, 'user' => $user, 'profile' => $profile]);
    }

    public function getAuthUser(){
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();
        $profile = $user->Profile;
        $social_auth = ($user->password) ? 0 : 1;

        $user = User::active($user->id);
        $user->alertNotification = Notification::whereIn('target', ['all', '' . $user->id, $user->profile->gender === 1 ? 'male' : 'female'])
            ->where('show_alert', 1)
            ->where('alert_read_ids', 'not like', '%,' . $user->id . ',%')
            ->where('delivery_at', '>=', $user->created_at)
            ->where('is_delivered', 1)->count();

        return response()->json(compact('user','profile','social_auth'));
    }

    public function check()
    {
        try {
            $token = JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response(['authenticated' => false, 'error' => $e]);
        }

        // JWTAuth::parseToken()->invalidate();

        $user = JWTAuth::parseToken()->authenticate();
        $loginAt = new Carbon($user->last_login_at);
        $now = Carbon::now();
        
        if ($loginAt->diffInMinutes($now) >= 9) {
            $user->last_login_at = Carbon::now();
            $user->save();
        }

        if ($user->is_logged_in != 1) {
            $user->is_logged_in = 1;
            $user->save();
        }

        return response(['authenticated' => true, 'status' => JWTAuth::parseToken()->authenticate()->status, 'gender' => JWTAuth::parseToken()->authenticate()->Profile->gender, 'is_admin' => JWTAuth::parseToken()->authenticate()->is_admin]);
    }

    public function logout()
    {

        try {
            $user = JWTAuth::parseToken()->authenticate();
            $user->is_logged_in = 0;
            $user->save();

            $token = JWTAuth::getToken();

            if ($token) {
                JWTAuth::invalidate($token);
            }

        } catch (JWTException $e) {
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(['message' => 'You are successfully logged out!']);
    }

    public function leave()
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $user->last_login_at = Carbon::now();
            $user->is_logged_in = 2;
            $user->save();
        } catch (JWTException $e) {
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(['message' => 'You are successfully logged out!']);
    }

    public function register(Request $request)
    {
        $user = \App\User::where('email', request('email'))->first();
        if ($user && $user->status != 'pending_activation') {
            return response()->json(['message' => 'このメールアドレスは既に使われています。'], 422);
        }
        $content = [];
        DB::beginTransaction();
        try {
            // update AFb
            $afb = Afb::where('unique_id', $request->get('unique_id'))->first();
            if (isset($afb)) {
                $afb->email = request('email');
                $afb->save();
            }

            if ($user && $user->status == 'pending_activation') {
                $user->delete();
            }
            $afilId = $request->cookie('afi');
            $user = \App\User::create([
                'email' => request('email'),
                'status' => 'pending_activation',
                'is_admin' => 0,
                'password' => bcrypt(request('password')),
                'is_first' => 1,
                'afil_id'   => $afilId,
            ]);
    
            $user->activation_token = generateUuid();
            $user->save();
            $profile = new \App\Model\Profile;
            $profile->content = "";
            $profile->age = $request->get('age');
            $profile->gender = $request->get('gender');
            $profile->show_online = 0;
            $profile->image_approved = 0;
            $profile->notification_settings = 14;
            $user->profile()->save($profile);
    
            // $user->notify(new Activation($user));
            Token::where('email', $request->get('email'))->delete();
            $token = Token::create([
                'email' => $request->get('email'),
                'token' => Str::random(32)
            ]);
            $content = [
                'email' => $request->get('email'),
                'name' => $request->get('email'),
                'subject' => '【DR】メールアドレス確認',
                'url' => config('values.site_url') . '/auth/' . $token->token . '/activate',
                'site_url' => config('values.site_url')
            ];
            // $emailJob = (new SendEmailJob($content, 'email.register'))->delay(Carbon::now()->addSeconds(1));
            // dispatch($emailJob);
            SendEmailJob::dispatch($content, 'email.register');

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。', 'data' => $content], 422);
        }

        return response()->json(['message' => 'You have registered successfully. Please check your email for activation!']);
    }

    public function activatePasswordReset($token)
    {
        $token = \App\Model\PasswordReset::where('token', $token)->first();
        if (!$token) {
            return response()->json(['status' => "invalid token"],422);
        }

        $user = \App\User::where('email', $token->email)->first();

        if(!$user)
            return response()->json(['status' => 'invalid token'],422);
        
        return response()->json(['status' => 'success']);
    }

    public function activate($activation_token){
        $token = Token::where('token', $activation_token)->first();
        if (!$token) {
            return response()->json(['status' => "invalid token"],422);
        }

        if ($token->content == '') { // New Email Confirm
            $email = $token->email;
            $user = \App\User::where('email', $token->email)->first();

            if(!$user)
                return response()->json(['status' => 'invalid token'],422);
    
            if($user->status == 'activated')
                return response()->json(['status' => 'activated']);
    
            $user->status = 'activated';
            $user->save();
            $token->delete();

            return response()->json(['status' => 'activated', 'email' => $email]);
        } else { // Email Change Confirm
            $email = $token->email;
            $user = \App\User::where('email', $token->content)->first();

            $user->status = 'activated_profile';
            $user->email = $token->email;
            $user->save();
            $token->delete();

            return response()->json(['status' => 'email_changed', 'email' => $email]);
        }
    }

    public function password(Request $request){

        $validation = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = \App\User::whereEmail(request('email'))->first();

        if(!$user)
            return response()->json(['message' => 'We couldn\'t found any user with this email. Please try again!'],422);

        $token = generateUuid();
        \DB::table('password_resets')->insert([
            'email' => request('email'),
            'token' => $token
        ]);
        $user->notify(new PasswordReset($user,$token));

        return response()->json(['message' => 'We have sent reminder email. Please check your inbox!']);
    }

    public function validatePasswordReset(Request $request){
        $validate_password_request = \DB::table('password_resets')->where('token','=',request('token'))->first();

        if(!$validate_password_request)
            return response()->json(['message' => 'Invalid password reset token!'],422);

        if(date("Y-m-d H:i:s", strtotime($validate_password_request->created_at . "+30 minutes")) < date('Y-m-d H:i:s'))
            return response()->json(['message' => 'Password reset token is expired. Please request reset password again!'],422);

        return response()->json(['message' => '']);
    }

    public function reset(Request $request){

        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = \App\User::whereEmail(request('email'))->first();

        if(!$user)
            return response()->json(['message' => 'We couldn\'t found any user with this email. Please try again!'],422);

        $validate_password_request = \DB::table('password_resets')->where('email','=',request('email'))->where('token','=',request('token'))->first();

        if(!$validate_password_request)
            return response()->json(['message' => 'Invalid password reset token!'],422);

        if(date("Y-m-d H:i:s", strtotime($validate_password_request->created_at . "+30 minutes")) < date('Y-m-d H:i:s'))
            return response()->json(['message' => 'Password reset token is expired. Please request reset password again!'],422);

        $user->password = bcrypt(request('password'));
        $user->save();

        $user->notify(new PasswordResetted($user));

        return response()->json(['message' => 'Your password has been reset. Please login again!']);
    }

    public function changePassword(Request $request){
        if(env('IS_DEMO'))
            return response()->json(['message' => 'You are not allowed to perform this action in this mode.'],422);

        $validation = Validator::make($request->all(),[
            'current_password' => 'required',
            'new_password' => 'required|confirmed|different:current_password|min:6',
            'new_password_confirmation' => 'required|same:new_password'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = JWTAuth::parseToken()->authenticate();

        if(!\Hash::check(request('current_password'),$user->password))
            return response()->json(['message' => 'Old password does not match! Please try again!'],422);

        $user->password = bcrypt(request('new_password'));
        $user->save();

        return response()->json(['message' => 'Your password has been changed successfully!']);
    }

    public function setNewPassword(Request $request)
    {
        $token = \App\Model\PasswordReset::where('token', $request->get('password_token'))->first();

        if (!$token) {
            return response()->json(['message' => 'error'], 422);
        }

        $email = $token->email;

        $user = User::where('email', $token->email)->first();
        $user->password = bcrypt($request->get('password'));
        $user->save();

        $token->delete();

        return response()->json(['message' => 'success', 'email' => $email]);
    }
}
