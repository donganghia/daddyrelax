<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Timeline;
use App\User;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Model\Chatroom;
use App\Model\Message;
use App\Model\Profile;
use App\Model\PointUsage;
use App\Model\PointUsageDetail;
use App\Jobs\SendEmailJob;
use App\Model\Block;
use Carbon\Carbon;

class ChatController extends Controller
{
    const recruitmentStatusAccept = "2";
    const recruitmentStatusMeeting = "3";
    const recruitmentStatusCancel = "6";
    const recruitmentStatusNotificationWaitingComplete ="9";
    const recruitmentStatusNotificationComplete ="10";

    public function chatrooms(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $chatrooms = Chatroom::active($user->id)->paginate($request->get('limit'));

        $userID = $user->id;
        $topChatrooms = Chatroom::whereIn('id', function ($query) use ($userID) {
                    $query->select('id')
                        ->from(with(new \App\Model\Chatroom)->getTable())
                        ->where('from_user_id', $userID)
                        ->whereIn('is_top', [1, 3])
                        ->whereNotIn('to_user_id', function($query) use ($userID) {
                            $query->select('block_user_id')
                                ->from(with(new \App\Model\Block)->getTable())
                                ->where('user_id', $userID);
                        });
                })->orWhereIn('id', function ($query) use ($userID) {
                    $query->select('id')
                        ->from(with(new \App\Model\Chatroom)->getTable())
                        ->where('to_user_id', $userID)
                        ->whereIn('is_top', [2, 3])
                        ->whereNotIn('from_user_id', function($query) use ($userID) {
                            $query->select('block_user_id')
                                ->from(with(new \App\Model\Block)->getTable())
                                ->where('user_id', $userID);
                        });
                })->with('from')
                ->with('to')
                ->orderBy('order_at', 'desc')->get();

        return response()->json(['data' => $chatrooms, 'top' => $topChatrooms]);
    }

    public function adminChatrooms(Request $request)
    {
        $user = \App\User::where('provider_unique_id', $request->get('user_id'))->first();

        $chatrooms = Chatroom::adminActive($user->id)->paginate($request->get('limit'));

        return response()->json(['data' => $chatrooms]);
    }

    public function adminMessages(Request $request)
    {
        $chatroom = Chatroom::where('firebaseKey', $request->get('firebaseKey'))->with('from')->first();

        $messages = Message::where('chatroom_id', $chatroom->id)
                           ->with('user')
                           ->orderBy('created_at', 'desc')
                           ->paginate($request->get('limit'));

        return response()->json(['data' => $messages, 'me' => $chatroom->from]);
    }

    public function adminAllMessages(Request $request)
    {
        $query = Message::whereIn('is_image', [0, 1, 3]);

        if ($request->get('start_date')) {
            $query->where('created_at', '>=', $request->get('start_date'));
        }

        if ($request->get('end_date')) {
            $query->where('created_at', '<=', $request->get('end_date'));
        }

        $messages = $query->with('user')
                        ->with('chatroom')
                        ->orderBy('created_at', 'desc')
                        ->paginate($request->get('limit'));

        return response()->json(['data' => $messages]);
    }

    public function messages(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        
        $chatroom = Chatroom::where('firebaseKey', $request->get('firebaseKey'))
                            ->with('from')
                            ->with('to')->first();
        if (!$chatroom) {
            return response()->json(['data' => 'エラーが発生しました。'], 422);
        }

        $block = Block::where('user_id', $chatroom->from_user_id)
                      ->where('block_user_id', $chatroom->to_user_id)->first();
        if (!$block) {
            $block = Block::where('user_id', $chatroom->to_user_id)
                          ->where('block_user_id', $chatroom->from_user_id)->first();
        }

        $query = Message::where('chatroom_id', $chatroom->id)
                        ->where('is_image', '!=', 3)
                           ->with('user');
        
        if ($block) {
            $query->where('created_at', '>=', $block->created_at);
        }

        $targetUser = $chatroom->from->id == $user->id ? $chatroom->to : $chatroom->from;
        if ($targetUser->is_deleted >= 1) {
            $query->where('created_at', '>=', Carbon::now());
        }

        $messages = $query->orderBy('created_at', 'desc')
                          ->paginate($request->get('limit'));

        foreach ($messages as $message) {
            if ($message->user->id != $user->id && $message->is_read == 0) {
                $message->is_read = 1;
                $message->save();
            }
        }

        if (strpos($chatroom->read_ids, "," . $user->id . ",") === false) {
            $chatroom->read_ids = $chatroom->read_ids . ',' . $user->id . ',';
            $chatroom->save();
        }

        $targetUser = $chatroom->from_user_id == $user->id ? $chatroom->to : $chatroom->from;
        $isFromUser = $chatroom->from_user_id == $user->id;
        $targetUserHidden = false;
        $thisUserHidden = false;
        if ($isFromUser) {
            if ($chatroom->is_show == 0 || $chatroom->is_show == 1) $targetUserHidden = true;
            if ($chatroom->is_show == 0 || $chatroom->is_show == 2) $thisUserHidden = true;
        } else {
            if ($chatroom->is_show == 0 || $chatroom->is_show == 2) $targetUserHidden = true;
            if ($chatroom->is_show == 0 || $chatroom->is_show == 1) $thisUserHidden = true;
        }

        $blockChat = $block ? true : false;

        if ($targetUser->is_deleted >= 1) $blockChat = true;

        return response()->json(['data' => $messages, 
                    'isFromUser' => $isFromUser,
                    'targetUser' => $targetUser,
                    'targetUserHidden' => $targetUserHidden,
                    'thisUserHidden' => $thisUserHidden,
                    'blockChat' => $blockChat]);     
    }

    public function updateChatroomFirebaseKey(Request $request)
    {
        Chatroom::where('id', $request->get('id'))->update([
            'firebaseKey' => $request->get('firebaseKey'),
            'order_at' => Carbon::now()
        ]);

        $user = JWTAuth::parseToken()->authenticate();

        return response()->json(['data' => \App\User::active($user->id)]);
    }

    public function send(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        
        $chatroom = Chatroom::where('firebaseKey', $request->get('firebaseKey'))->first();
        if (!$chatroom) {
            return response()->json(['data' => 'エラーが発生しました。'], 422);
        }

        DB::beginTransaction();
        try {
            if (intval($request->get('is_image')) == 3) { // image delete
                $message = Message::where('id', $request->get('messageID'))->first();

                $lastMessage = Message::where('chatroom_id', $chatroom->id)->orderBy('created_at', 'desc')->first();
                
                $message->is_image = 3;
                $message->save();
                if ($message->id == $lastMessage->id) {
                    $lastMessage = Message::where('chatroom_id', $chatroom->id)
                                          ->where('is_image', '!=', 3)->orderBy('created_at', 'desc')->first();
                    
                    $chatroom->last_message = $lastMessage->message;
                    $chatroom->sender_id = $user->id;
                    if ($lastMessage->is_read === 1) {
                        $chatroom->read_ids = ',' . $chatroom->from_user_id . ",," . $chatroom->to_user_id;
                    }
                    if ($request->get('hasBlockedBy') == 0)
                        $chatroom->order_at = Carbon::now();
                    $chatroom->save();
                }
            } else {
                $message = Message::create([
                    'chatroom_id' => $chatroom->id,
                    'user_id' => $user->id,
                    'message' => $request->get('message'),
                    'is_read' => 0,
                    'is_image' => $request->get('is_image'),
                    'recruitment_status' => $request->get('recruitment_status')
                ]);
    
                $chatroom->is_matching_message = 0;
                $chatroom->last_message = $request->get('message');
                $chatroom->sender_id = $user->id;
                $chatroom->read_ids = "," . $user->id . ",";
                if ($request->get('hasBlockedBy') == 0)
                    $chatroom->order_at = Carbon::now();
                $chatroom->is_show = 3;
                $chatroom->save();
            }

            DB::commit();

            return response()->json(['data' => $message]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'エラーが発生しました。'], 422);
        }
    }

    public function sendPushNotification(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $chatroom = Chatroom::where('firebaseKey', $request->get('firebaseKey'))->first();
        if (!$chatroom) {
            return response()->json(['data' => 'no_push']);
        }

        $targetUser = $chatroom->from_user_id == $user->id ? $chatroom->to_user_id : $chatroom->from_user_id;
        $targetUser = User::find($targetUser);

        if (('' . $targetUser->profile->notification_settings & 8) == '8') {
            $profile = $user->Profile;
            $content = [
                'email' => $targetUser->email,
                'name' => $targetUser->profile->nickname,
                'subject' => '【DR】' . $profile->nickname . ' ' . $profile->age . '歳（' . $profile->city->name . '）メッセージ通知',
                'sender_name' => $profile->nickname,
                'sender_age' => $profile->age,
                'sender_city' => $profile->city->name,
                'profile_url' => config('values.site_url') . '/user/' . $user->provider_unique_id,
                'message_page_url' => config('values.site_url') . '/messages',
                'not_settings_url' => config('values.site_url') . '/notification-settings',
                'site_url' => config('values.site_url')
            ];

            SendEmailJob::dispatch($content, 'email.message');
        }

        return response()->json(['data' => 'success']);
    }

    public function read(Request $request)
    {
        Message::find($request->get('id'))->update([
            'is_read' => 1
        ]);

        $user = JWTAuth::parseToken()->authenticate();
        // $chatroom = Chatroom::find($request->get('chatroom_id'));
        // $chatroom->read_ids = $chatroom->read_ids . "," . $user->id . ",";
        // $chatroom->order_at = Carbon::now();
        // $chatroom->save();

        return response()->json(['data' => 'success']);
    }

    public function pointPayment(Request $request)
    {
        $message = $request->get('message');
        $male = $request->get('male');
        $female = $request->get('female');
        $status = $request->get('status');
        $email_male = $request->get('email_male');
        $email_female = $request->get('email_female');
        $pointPaid = 0;

        if($male && $female) {
            $arr = explode("\n", $message);
            $date = explode("-", $arr[1]);
            $current = time();
            $startTime = $this->convertJpDateToTime($date[0]);
            $endTime = $this->convertJpDateToTime($date[1]);

            $moment = $endTime - $startTime; 
            $minutes = $moment < 0 ? 0 : ceil($moment/60);

            $time = $startTime - $current;
            $timeOneDay =  24*60*60;
            $timeSixHours =  6*60*60;
            if($time > $timeOneDay) {
                // 前日まで 30％男性課金
                $pointPaid = ceil(0.3 * $minutes * $female["cost"] / 30);
                $msg = "前日まで 30％男性課金".$pointPaid."P獲得";
            } else if($time <= $timeOneDay && $time > $timeSixHours) {
                //24時間以内 50％男性課金
                $pointPaid = ceil(0.5 * $minutes * $female["cost"] / 30);
                $msg = "24時間以内 50％男性課金".$pointPaid."P獲得";
            } else {
                //6時間以内 100％男性課金
                $pointPaid = ceil($minutes * $female["cost"] / 30);
                $msg = "6時間以内 100％男性課金".$pointPaid."P獲得";
            }
            
            //延長30分刻みで計算（延長三割増し）
            $extPointPaid = 0;
            if($current > $endTime) {
                $extPointPaid = ceil(1.3 * $female["cost"] * ($current - $endTime)/30 );
            }

            //開始時刻が夜23時-5時 or 終了時刻が夜0時-6時の場合は一律4000P加算
            $startHour = date('H', $startTime);
            $endHour = date('H', $current);
            $overPointPaid = 0;
            if($startHour >= 23 || $startHour < 6 || $endHour <= 6 ) {
                $overPointPaid = 4000;
            }

            //check exist point in account
            
            //update point
            Profile::find($female["id"])->update([
                'points' => $female["points"] + $pointPaid + $extPointPaid + $overPointPaid
            ]);

            Profile::find($male["id"])->update([
                'points' => $male["points"] - $pointPaid - $extPointPaid - $overPointPaid
            ]);
           
            //Create point usage
            $femalePointUsage = PointUsage::create([
                'user_id' => $female["id"],
                'points' => $pointPaid + $extPointPaid + $overPointPaid,
                'amount' => 0,
                'type' => 'bonus_meeting'
            ]);
            $malePointUsage = PointUsage::create([
                'user_id' => $male["id"],
                'points' => $pointPaid + $extPointPaid + $overPointPaid,
                'amount' => 0,
                'type' => 'paid_meeting'
            ]);

           PointUsageDetail::create([
                'point_usage_id' => $malePointUsage->id,
                'target_id' => $female["id"],
                'points' => $pointPaid,
                'type' =>1,
                'recruitment_start_date' => date("Y-m-d H:i:s", $startTime),
                'recruitment_end_date' => date("Y-m-d H:i:s", $endTime)
            ]);

            PointUsageDetail::create([
                'point_usage_id' => $femalePointUsage->id,
                'target_id' => $male["id"],
                'points' => $pointPaid,
                'type' =>1,
                'recruitment_start_date' => date("Y-m-d H:i:s", $startTime),
                'recruitment_end_date' => date("Y-m-d H:i:s", $endTime)
            ]);

            PointUsageDetail::create([
                'point_usage_id' => $malePointUsage->id,
                'target_id' => $female["id"],
                'points' => $extPointPaid,
                'type' =>2,
                'recruitment_start_date' => $extPointPaid > 0 ? date("Y-m-d H:i:s", $endTime) : null,
                'recruitment_end_date' => $extPointPaid > 0 ? date("Y-m-d H:i:s", $current) : null
            ]);

            PointUsageDetail::create([
                'point_usage_id' => $femalePointUsage->id,
                'target_id' => $male["id"],
                'points' => $extPointPaid,
                'type' =>2,
                'recruitment_start_date' => $extPointPaid > 0 ? date("Y-m-d H:i:s", $endTime) : null,
                'recruitment_end_date' => $extPointPaid > 0 ? date("Y-m-d H:i:s", $current) : null
            ]);

            PointUsageDetail::create([
                'point_usage_id' => $malePointUsage->id,
                'target_id' => $female["id"],
                'points' => $overPointPaid,
                'type' =>3,
                'recruitment_start_date' => null,
                'recruitment_end_date' => null
            ]);

            PointUsageDetail::create([
                'point_usage_id' => $femalePointUsage->id,
                'target_id' => $male["id"],
                'points' => $overPointPaid,
                'type' =>3,
                'recruitment_start_date' => null,
                'recruitment_end_date' => null
            ]);

            if($status == self::recruitmentStatusCancel) {    
                //send mail male
                $content = [
                    'email' => $email_male,
                    'subject' => '約束キャンセル',
                    'name' => $male['nickname'],
                    'status' => "お約束はキャンセルになりました。",
                    'message' => $message,
                    'content' => "[キャンセル料金]<br/>".$msg,
                    'site_url' => config('values.site_url')
                ];
                SendEmailJob::dispatch($content, 'email.booking_info');

                //send mail male female
                $content = [
                    'email' => $email_female,
                    'subject' => '約束キャンセル',
                    'name' => $male['nickname'],
                    'status' => "お約束はキャンセルになりました。",
                    'message' => $message,
                    'content' => '',
                    'site_url' => config('values.site_url')
                ];
                SendEmailJob::dispatch($content, 'email.booking_cancel');
            } else {
                //$pointPaid + $extPointPaid + $overPointPaid
                $request->request->set('pointPaid', $pointPaid);
                $request->request->set('extPointPaid', $extPointPaid);
                $request->request->set('overPointPaid', $overPointPaid);

                $this->sendNoticeEmail($request);
            }
        }
        return response()->json(['minutes' => $minutes, 'msg' => $msg]);
    }

    public function sendNoticeEmail(Request $request)
    {
        $message = $request->get('message');
        $male = $request->get('male');
        $female = $request->get('female');
        $recruitmentStatus = $request->get('status');
        $email_male = $request->get('email_male');
        $email_female = $request->get('email_female');
        $subject = '';

        switch($recruitmentStatus) {
            //①集合開始案内
            case self::recruitmentStatusAccept:
                $email = $email_male;
                $subject = '集合開始されました';
                $name = $male['nickname'];
                $status = '集合開始されました。';
                $message .= "<br/>".$female['nickname'];
                $content = '';
                break;
            //②終了5分前通知
            case self::recruitmentStatusMeeting:
                $email = $email_male;
                $subject = '終了5分前通知';
                $name = $male['nickname'];
                $status = 'お時間5分前になりました。<br/>終了される場合は「解散終了」ボタンを押して下さい。';
                $message .= "<br/>".$female['nickname'];
                $content = '';
                break;
            //③解散終了申請
            case self::recruitmentStatusNotificationWaitingComplete:
                $email = $email_female;
                $subject = '解散終了申請';
                $name = $female['nickname'];
                $status = '終了のお時間になりました。<br/>メッセージ内「承認する」ボタンを押して下さい。';
                $message .= "<br/>".$male['nickname'];
                $content = '';
                break;
            //④ご利用明細
            case self::recruitmentStatusNotificationComplete:
                $pointPaid = $request->get('pointPaid');
                $extPointPaid = $request->get('extPointPaid');
                $overPointPaid = $request->get('overPointPaid');

                $email = $email_male;
                $subject = '解散終了簡易明細';
                $name = $male['nickname'];
                $status = '終了のお時間になりました。<br/>すみやかに解散をお願いいたします。';
                $message .= "<br/>".$female['nickname'];
                $content = $message.'<br/>';
                $content .= '基本 ＝'. $pointPaid.'P';
                $content .= '延長 ＝'. $extPointPaid.'P';
                $content .= '深夜手当 ＝'. $overPointPaid.'P';
                break;
            default:
                break;
        }
        $content = [
            'email' => $email,
            'subject' => $subject,
            'name' => $name,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            'site_url' => config('values.site_url')
        ];
        SendEmailJob::dispatch($content, 'email.booking_info');

        return response()->json(['data' => true]);
    }

    private function convertJpDateToTime($d) {
        $d = str_replace("年","-", $d);
        $d = str_replace("月","-", $d);
        $d = str_replace("日","", $d);
        return strtotime($d);
    }

    public function updateRecruitment(Request $request)
    {
        Message::find($request->get('id'))->update([
            'recruitment_status' => $request->get('recruitment_status')
        ]);
        return response()->json(['data' => 'success']);
    }

    public function makeTop(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $chatroom = Chatroom::find($request->get('chatroom_id'));
        if ($chatroom->from_user_id == $user->id) {
            if ($chatroom->is_top == 1 || $chatroom->is_top == 3) {
                $chatroom->is_top--;
            } else {
                $chatroom->is_top++;
            }
        } else {
            if ($chatroom->is_top == 2 || $chatroom->is_top == 3) {
                $chatroom->is_top -= 2;
            } else {
                $chatroom->is_top += 2;
            }
        }

        $chatroom->save();

        return response()->json(['data' => 'success']);
    }

    public function hideChatroom(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $chatroom = Chatroom::find($request->get('chatroom_id'));
        if ($chatroom->from_user_id == $user->id) {
            if ($chatroom->is_show == 1 || $chatroom->is_show == 3) {
                $chatroom->is_show--;
            } else {
                $chatroom->is_show++;
            }
        } else {
            if ($chatroom->is_show == 2 || $chatroom->is_show == 3) {
                $chatroom->is_show -= 2;
            } else {
                $chatroom->is_show += 2;
            }
        }

        // $chatroom->order_at = Carbon::now();
        $chatroom->save();

        return response()->json(['data' => 'success']);
    }

    public function uploadMessagePhoto(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        $image = $this->uploadPhoto($request, 0);

        return response()->json($image);
    }

    public function createPrototype(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if (intval($request->get('id')) === -1) {
            \App\Model\MessagePrototype::create([
                'user_id' => $user->id,
                'message' => $request->get('content')
            ]);
        } else {
            \App\Model\MessagePrototype::where('id', $request->get('id'))->update([
                'message' => $request->get('content')
            ]);
        }

        $user = \App\User::active($user->id);

        return response()->json(['data' => $user]);
    }

    public function deletePrototype(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        \App\Model\MessagePrototype::where('id', $request->get('id'))->delete();

        $user = \App\User::active($user->id);

        return response()->json(['data' => $user]);
    }
}
