<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Notification;
use App\User;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function allNotifications(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $notifications = Notification::whereIn('target', ['all', '' . $user->id, $user->profile->gender === 1 ? 'male' : 'female'])
                                    ->where('delivery_at', '<=', Carbon::now())
                                    ->where('delivery_at', '>=', $user->created_at)
                                    ->where('is_delivered', 1)
                                    ->orderBy('delivery_at', 'desc')
                                    ->orderBy('created_at', 'desc')
                                    ->get();
        
        $results = array();
        $hasUnreads = false;
        foreach ($notifications as $notification) {
            $results[] = [
                'id' => $notification->id,
                'title' => $notification->title,
                'content' => $notification->content,
                'created_at' => $notification->created_at->format("Y/m/d H:i:s"),
                'delivery_at' => $notification->delivery_at != null ? Carbon::parse($notification->delivery_at)->format('Y/m/d H:i:s') : $notification->created_at->format("Y/m/d H:i:s"),
                'isRead' => strpos($notification->read_ids, "," . $user->id . ",") !== false
            ];

            if (strpos($notification->read_ids, "," . $user->id . ",") === false) {
                $hasUnreads = true;
            }
        }
        
        return response()->json(['data' => ['notifications' => $results, 'hasUnreads' => $hasUnreads]]);
    }

    public function allAdminNotifications(Request $request)
    {
        $notifications = Notification::whereIn('target', ['all', 'male', 'female'])
                                     ->orderBy('delivery_at', 'desc')
                                     ->orderBy('created_at', 'desc')
                                     ->paginate($request->get('limit'));
        return response()->json(['data' => $notifications]);
    }

    public function newNotification(Request $request)
    {
        Notification::create([
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'target' => $request->get('target'),
            'read_ids' => '',
            'is_delivered' => 0,
            'delivery_at' => $request->get('delivery_at'),
            'show_alert' => $request->get('show_alert'),
            'alert_read_ids' => ''
        ]);

        $notifications = Notification::whereIn('target', ['all', 'male', 'female'])
                                     ->orderBy('created_at', 'desc')
                                     ->paginate($request->get('limit'));

        return response()->json(['data' => $notifications]);
    }

    public function sendPushNotification(Request $request)
    {
        $notification = Notification::find($request->get('id'));

        return response()->json(['data' => 'success']);
    }

    public function deleteNotification(Request $request)
    {
        Notification::where('id', $request->get('id'))->delete();

        $notifications = Notification::whereIn('target', ['all', 'male', 'female'])
                                     ->orderBy('created_at', 'desc')
                                     ->paginate($request->get('limit'));
                                     
        return response()->json(['data' => $notifications]);
    }

    public function alertRead(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $notifications = Notification::whereIn('target', ['all', '' . $user->id, $user->profile->gender === 1 ? 'male' : 'female'])
            ->where('show_alert', 1)
            ->where('alert_read_ids', 'not like', '%,' . $user->id . ',%')
            ->where('is_delivered', 1)->get();
        
        foreach ($notifications as $notification) {
            $notification->alert_read_ids .= ',' . $user->id . ',';
            $notification->save();
        }
        
        return response()->json(['data' => 'success']);
    }

    public function readNotification(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $notification = Notification::where('id', $request->get('notification_id'))->first();
        $notification->read_ids = $notification->read_ids . "," . $user->id . ",";
        $notification->save();

        $notifications = Notification::whereIn('target', ['all', '' . $user->id, $user->profile->gender === 1 ? 'male' : 'female'])
                                    ->where('delivery_at', '<=', Carbon::now())
                                    ->where('delivery_at', '>=', $user->created_at)
                                    ->where('is_delivered', 1)
                                    ->orderBy('delivery_at', 'desc')
                                    ->orderBy('created_at', 'desc')
                                    ->get();
        
        $results = array();
        $hasUnreads = false;
        foreach ($notifications as $notification) {
            $results[] = [
                'id' => $notification->id,
                'title' => $notification->title,
                'content' => $notification->content,
                'created_at' => $notification->created_at->format("Y/m/d H:i:s"),
                'isRead' => strpos($notification->read_ids, "," . $user->id . ",") !== false
            ];

            if (strpos($notification->read_ids, "," . $user->id . ",") === false) {
                $hasUnreads = true;
            }
        }

        return response()->json(['notifications' => $results, 'hasUnreads' => $hasUnreads]);
    }
}
