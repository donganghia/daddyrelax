<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Block;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;

class BlockController extends Controller
{
    public function allBlocks(Request $request)
    {
        $query = Block::where('id', ">=", 1);

        if ($request->get('username') != '') {
            $query->whereIn('user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('nickname', 'like', '%' . $request->get('username') . '%');
                });
        }

        if ($request->get('blockUsername') != '') {
            $query->whereIn('block_user_id', function($query) use ($request) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Profile)->getTable())
                        ->where('nickname', 'like', '%' . $request->get('blockUsername') . '%');
                });
        }

        if ($request->get('user_id') != '') {
            $query->where('user_id', $request->get('user_id'));
        }

        if ($request->get('block_user_id') != '') {
            $query->where('block_user_id', $request->get('block_user_id'));
        }

        if ($request->get('start_date')) {
            $query->where('created_at', '>=', $request->get('start_date'));
        }

        if ($request->get('end_date')) {
            $query->where('created_at', '<=', $request->get('end_date'));
        }

        $blocks = $query->with('user')
                         ->with('blockUser')
                         ->orderBy('created_at', 'desc')
                         ->paginate($request->get('limit'));

        $froms = \App\User::whereIn('id', function ($query) {
                    $query->select('user_id')
                        ->from(with(new \App\Model\Block)->getTable())
                        ->where('id', ">=", 1);
        })->select(['id'])->with('profile')->get();

        $tos = \App\User::whereIn('id', function ($query) {
            $query->select('block_user_id')
                ->from(with(new \App\Model\Block)->getTable())
                ->where('id', ">=", 1);
        })->select(['id'])->with('profile')->get();
        
        return response()->json(['data' => $blocks, 'froms' => $froms, 'tos' => $tos]);
    }

    public function removeBlock(Request $request)
    {
        $block = Block::find($request->get('id'));
        Block::where('user_id', $block->user_id)
             ->where('block_user_id', $block->block_user_id)->delete();

        return response()->json(['data' => 'success']);
    }
}
