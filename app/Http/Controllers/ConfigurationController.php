<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;

class ConfigurationController extends Controller
{
	public function store(Request $request){
        $data = $request->all();
        foreach ($data as $key => $value) {
            $config = \App\Configuration::where('name', $key)->first();
            if ($config) {
                $config->value = $value;
                $config->save();
            }
        }

        return response()->json(['data' => 'success']);
	}

	public function index(){
		$config = \App\Configuration::all()->pluck('value','name')->all();
		return response()->json(compact('config'));
    }
}
