<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use App\Model\Afb;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;

class AfbController extends Controller
{
    public function create(Request $request)
    {
        $afb = Afb::where('unique_id', $request->get('unique_id'))->first();

        if (isset($afb)) {
            $afb->update([
                'unique_id' => $request->get("unique_id") ? $request->get("unique_id") : '',
                'abm' => $request->get('abm') ? $request->get('abm') : '',
                'email' => $request->get('email') ? $request->get('email') : ''
            ]);
        } else {
            $afb = Afb::create([
                'unique_id' => $request->get("unique_id") ? $request->get("unique_id") : '',
                'abm' => $request->get('abm') ? $request->get('abm') : '',
                'email' => $request->get('email') ? $request->get('email') : ''
            ]);
        }

        return response()->json(['data' => 'success']);
    }

    public function get(Request $request)
    {
        if ($request->get('email') !== null) {
            $afb = Afb::where('email', $request->get('email'))->first();

            if ($afb) {
                $abm = $afb->abm;
                Afb::where('email', $request->get('email'))->delete();

                return response()->json(['data' => $abm]);
            }
        }

        return response()->json(['data' => '']);
    }
}
