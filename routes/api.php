<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/card/response', 'UserController@cardResponse');
Route::post('/card/remove', 'UserController@cardRemoveResponse');
Route::post('/afb/create', 'AfbController@create');
Route::post('/afb/get', 'AfbController@get');

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login','AuthController@authenticate');
    Route::post('/logout','AuthController@logout');
    Route::post('/check','AuthController@check');
    Route::post('/register','AuthController@register')->middleware('encrypt.cookie');
    Route::post('/activate/{token}','AuthController@activate');
    Route::post('/activate/password/{token}','AuthController@activatePasswordReset');
    Route::post('/password','AuthController@password');
    Route::post('/validate-password-reset','AuthController@validatePasswordReset');
    Route::post('/reset','AuthController@reset');
    Route::post('/social/token','SocialAuthController@getToken');
    Route::post('/password/reset','AuthController@setNewPassword');
    Route::post('/user/quit', 'UserController@quitMember');
    Route::post('/user/password/forogt', 'UserController@passwordForgot');
    Route::post('/leave', 'AuthController@leave');
});

// Question
Route::post('/question/ask', 'QuestionController@askQuestion');

Route::group(['middleware' => ['jwt.auth']], function () {
  Route::get('/auth/user','AuthController@getAuthUser');
  Route::post('/task','TaskController@store');
  Route::get('/task','TaskController@index');
  Route::delete('/task/{id}','TaskController@destroy');
  Route::get('/task/{id}','TaskController@show');
  Route::patch('/task/{id}','TaskController@update');
  Route::post('/task/status','TaskController@toggleStatus');

    Route::get('/configuration/fetch','ConfigurationController@index');
    Route::post('/configuration','ConfigurationController@store');

    Route::get('/user','UserController@index');
    Route::post('/user/change-password','AuthController@changePassword');
    Route::post('/user/update-profile','UserController@updateProfile');
    Route::post('/user/update-avatar','UserController@updateAvatar');
    Route::post('/user/remove-avatar','UserController@removeAvatar');
    Route::delete('/user/{id}','UserController@destroy');
    Route::get('/user/dashboard','UserController@dashboard');

    Route::post('todo','TodoController@store');
    Route::get('/todo','TodoController@index');
    Route::delete('/todo/{id}','TodoController@destroy');
    Route::post('/todo/status','TodoController@toggleStatus');

    // Master Data
    Route::post('/master/data', 'UserController@getMasterData');
    Route::post('/upload_image', 'UserController@uploadImage');
    Route::post('/upload_image_save', 'UserController@uploadImageAndUpdateProfile');
    Route::post('/delete_image', 'UserController@deleteImageAndUpdateProfile');
    Route::post('/user/update/profile', 'UserController@updateUserProfile');
    Route::post('/user/update/firebasekey', 'UserController@updateUserFirebaseKey');
    Route::post('/questions', 'QuestionController@allQuestions');
    Route::post('/vip/images', 'VipImageController@allVipImages');

    // User
    Route::post('/user/unblock', 'UserController@unblockUser');
    Route::post('/user/email/change', 'UserController@emailChange');
    Route::post('/user/password/change', 'UserController@passwordChange');
    Route::post('/login/user/password/change', 'UserController@loginUserPasswordChange');
    Route::post('/user/card/update', 'UserController@updateCreditCard');
    Route::post('/user/identity/upload', 'UserController@uploadIdentityImage');
    Route::post('/user/like', 'UserController@likeUser');
    Route::post('/user/report', 'UserController@reportUser');
    Route::post('/user/block', 'UserController@blockUser');
    Route::post('/user/leave/history', 'UserController@leaveViewHistory');
    Route::post('/user/like/back', 'UserController@likeBackUser');
    Route::post('/user/like/skip', 'UserController@skipUserLike');
    
    // Favorites
    Route::post('/favorite/remove', 'FavoriteController@removeFavorite');
    Route::post('/favorite/add', 'FavoriteController@addFavorite');

    // Notification
    Route::post('/notifications', 'NotificationController@allNotifications');
    Route::post('/notification/read', 'NotificationController@readNotification');
    Route::post('/notification/alert/read', 'NotificationController@alertRead');

    // Point
    Route::post('/point/packages', 'PointController@allPackages');
    Route::post('/point/purchase', 'PointController@purchasePoints');
    Route::post('/point/costs', 'PointController@allCosts');
    Route::post('/point/save', 'PointController@saveCost');
    Route::post('/point/history', 'PointController@history');
    Route::post('/point/receipt', 'PointController@saveReceipt');
    Route::post('/point/save-banks', 'PointController@saveBanks');
    Route::post('/point/exchange-points', 'PointController@exchangePoints');

    // Membership
    Route::post('/membership/list', 'MembershipController@allMemberships');
    Route::post('/membership/upgrade', 'MembershipController@upgradePlan');
    Route::post('/membership/update', 'MembershipController@updatePlan');

    // Search
    Route::post('/search', 'SearchController@search');
    Route::post('/search/user', 'SearchController@getUser');
    Route::post('/likes', 'SearchController@likes');
    Route::post('/view/histories', 'SearchController@viewHistories');
    Route::post('/likes/open', 'SearchController@openLikePage');

    // Timeline
    Route::post('/timelines', 'TimelineController@allTimelines');
    Route::post('/timeline/add', 'TimelineController@addTimeline');
    Route::post('/timeline/delete', 'TimelineController@deleteTimeline');

    // Chatroom
    Route::post('/chatrooms', 'ChatController@chatrooms');
    Route::post('/chat/messages', 'ChatController@messages');
    Route::post('/chatroom/firebaseKey', 'ChatController@updateChatroomFirebaseKey');
    Route::post('/chat/message/send', 'ChatController@send');
    Route::post('/chat/message/upload_photo', 'ChatController@uploadMessagePhoto');
    Route::post('/chat/message/send/push', 'ChatController@sendPushNotification');
    Route::post('/chat/message/read', 'ChatController@read');
    Route::post('/chat/message/update_recruitment', 'ChatController@updateRecruitment');
    Route::post('/chat/message/point_payment', 'ChatController@pointPayment');
    Route::post('/chat/message/send_notice', 'ChatController@sendNoticeEmail');
    Route::post('/chatroom/top', 'ChatController@makeTop');
    Route::post('/chatroom/hide', 'ChatController@hideChatroom');
    Route::post('/prototype/create', 'ChatController@createPrototype');
    Route::post('/prototype/delete', 'ChatController@deletePrototype');

    // Pay
    Route::post('/pay', 'UserController@pay');
    Route::post('/telecom/check', 'UserController@telecomCheck');
    Route::post('/telecom/reset', 'UserController@telecomReset');
});

Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'admin'], function () {
    // Admin
    Route::post('/users', 'UserController@adminUsers');
    Route::post('/users/static', 'UserController@staticUsers');
    Route::post('/users/static/report', 'UserController@createReportByAfil');
    Route::post('/user/detail', 'UserController@adminUserDetail');
    Route::post('/user/approve', 'UserController@approveUser');
    Route::post('/user/approve/one', 'UserController@approveOneImage');
    Route::post('/user/report', 'UserController@reportFromAdmin');
    Route::post('/user/payments/point', 'PointController@getUserPointPurchases');
    Route::post('/user/memberships', 'MembershipController@getUserMemberships');
    Route::post('/user/update', 'UserController@adminUpdate');
    Route::post('/user/add/point', 'UserController@adminAddPoint');
    Route::post('/user/check/birthday', 'UserController@checkBirthday');
    Route::post('/user/reports', 'UserController@reports');
    Route::post('/telecom/trends', 'UserController@telecomTrends');

    Route::post('/reports', 'ReportController@allReports');
    Route::post('/report/ranks', 'ReportController@allReportRanks');
    Route::post('/blocks', 'BlockController@allBlocks');
    Route::post('/block/remove', 'BlockController@removeBlock');
    Route::post('/timelines', 'TimelineController@allAdminTimelines');
    Route::post('/timeline/delete', 'TimelineController@adminDeleteTimeline');
    Route::post('/notifications', 'NotificationController@allAdminNotifications');
    Route::post('/notification/new', 'NotificationController@newNotification');
    Route::post('/notification/delete', 'NotificationController@deleteNotification');
    Route::post('/notification/send/push', 'NotificationController@sendPushNotification');

    // Chatrooms
    Route::post('/chatrooms', 'ChatController@adminChatrooms');
    Route::post('/chatroom/messages', 'ChatController@adminMessages');
    Route::post('/chatroom/all/messages', 'ChatController@adminAllMessages');
});