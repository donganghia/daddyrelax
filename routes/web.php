<?php
// use Illuminate\Support\Facades\URL;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// if (env('APP_ENV') === 'production') {
    // URL::forceSchema('https');
// }

// Route::get('/auth/social/{provider}', 'SocialAuthController@providerRedirect');
// Route::get('/auth/{provider}/callback', 'SocialAuthController@providerRedirectCallback');
Route::get('/', function() {
    return File::get(public_path() . '/lp.html');
})->name('index');

Route::get('/a/{afiID}', 'UserController@setCookieAfil');

// Route::get('/card/response', function() {
//     // return view('telecom.card_response');
//     return File::get(public_path() . '/telecom/card_response.html');
// });
// Route::get('/card/payment', function() {
//     // return view('telecom.card_payment');
//     return File::get(public_path() . '/telecom/card_payment.html');
// });

Route::get('/offline', function() {
    return view('modules.laravelpwa.offline');
});

Route::get('/{vue?}', function () {
    return view('home');
})->where('vue', '[\/\w\.-]*')->name('home');