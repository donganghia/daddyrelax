<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtbMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtb_memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->string('type')->default('');
            $table->integer('price')->default(0);
            $table->integer("original_price")->default(0);
            $table->integer('period')->default(0);
            $table->integer('off_percent')->default(0);
            $table->integer('points')->default(0);
            $table->integer('search_days')->default(0);
            $table->integer('sort_no')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtb_memberships');
    }
}
