<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtb_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->integer('price')->default(0);
            $table->integer("original_price")->default(0);
            $table->integer('points')->default(0);
            $table->integer('sort_no')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtb_packages');
    }
}
