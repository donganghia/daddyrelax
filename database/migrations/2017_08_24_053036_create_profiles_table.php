<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('nickname')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('age')->default(0);
            $table->tinyInteger('age_approved')->default(0);
            $table->string('avatar')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('avatar_thumb')->nullable();
            $table->string('facebook_profile')->nullable();
            $table->string('twitter_profile')->nullable();
            $table->string('google_plus_profile')->nullable();
            $table->string('address')->nullable();
            $table->integer('height')->default(0);
            $table->integer('job_id')->nullable();
            $table->integer('marriage_id')->nullable();
            $table->string('dream')->nullable();
            $table->integer('salary_id')->nullable();
            $table->integer('body_id')->nullable();
            $table->integer('drink_id')->nullable();
            $table->integer('smoking_id')->nullable();
            $table->integer('membership_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->text('content')->nullabe();
            $table->string('work')->nullable();
            $table->integer('points')->default(0);
            $table->integer('notification_settings')->default(0);
            $table->integer('likes')->default(0);
            $table->integer('liked')->default(0);
            $table->tinyInteger('is_private')->default(0);
            $table->tinyInteger('is_vip')->default(0);
            $table->tinyInteger('show_online')->default(1);
            $table->integer('alert_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
