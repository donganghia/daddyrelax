<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointUsageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_usage_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('point_usage_id')->unsigned();
            $table->integer('target_id')->unsigned();
            $table->integer('points')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->datetime('recruitment_start_date')->nullable();
            $table->datetime('recruitment_end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_usage_details');
    }
}
