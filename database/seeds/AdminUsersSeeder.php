<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdminUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email', 'operation@daddyrelax.jp')->first();
        if (is_null($user)) {
            $user = \App\User::create([
                'email' => 'operation@daddyrelax.jp',
                'password' => bcrypt('giulian002'),
                'status' => 'activated',
                'is_admin' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            \App\Model\Profile::create([
                'user_id' => $user->id,
                'first_name' => '管理者2',
                'nickname'  => '管理者2',
                'last_name' => '',
                'content' => '',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $user = \App\User::where('email', 'support@daddyrelax.jp')->first();
        if (is_null($user)) {
            $user = \App\User::create([
                'email' => 'support@daddyrelax.jp',
                'password' => bcrypt('giulian003'),
                'status' => 'activated',
                'is_admin' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            \App\Model\Profile::create([
                'user_id' => $user->id,
                'first_name' => '管理者3',
                'nickname'  => '管理者3',
                'last_name' => '',
                'content' => '',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
