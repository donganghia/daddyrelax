<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Body;

class BodiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Body::create([
            'name' => '細め'
        ]);
        Body::create([
            'name' => 'やや細め'
        ]);
        Body::create([
            'name' => '普通'
        ]);
        Body::create([
            'name' => 'やや太め'
        ]);
        Body::create([
            'name' => '太め'
        ]);
    }
}
