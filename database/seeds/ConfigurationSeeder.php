<?php

use Illuminate\Database\Seeder;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Configuration::create([
            'name' => 'like_point_usage', 
            'value' => 20
        ]);
        \App\Configuration::create([
            'name' => 'like_back_point_usage', 
            'value' => 20
        ]);
        \App\Configuration::create([
            'name' => 'timeline_point_usage', 
            'value' => 20
        ]);
        \App\Configuration::create([
            'name' => 'point_reset_days',
            'value' => 180
        ]);
        \App\Configuration::create([
            'name' => 'timelines_per_day',
            'value' => 1
        ]);
        \App\Configuration::create([
            'name' => 'last_days',
            'value' => 90
        ]);
        \App\Configuration::create([
            'name' => 'male_like_limit',
            'value' => -1
        ]);
        \App\Configuration::create([
            'name' => 'female_like_limit',
            'value' => -1
        ]);
    }
}
