<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $user = \App\User::create([
        //     'email' => 'admin@daddyrelax.com',
        //     'password' => bcrypt('123123'),
        //     'status' => 'activated',
        //     'is_admin' => 1,
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);

        // \App\Model\Profile::create([
        //     'user_id' => $user->id,
        //     'first_name' => '管理者',
        //     'nickname'  => '管理者',
        //     'last_name' => '',
        //     'content' => '',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);

        $user = \App\User::create([
            'email' => 'megumi_nakanishi@giulian.jp',
            'password' => bcrypt('giulian4649'),
            'status' => 'activated',
            'is_admin' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \App\Model\Profile::create([
            'user_id' => $user->id,
            'first_name' => '管理者',
            'nickname'  => '管理者',
            'last_name' => '',
            'content' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $this->call([
            CitiesSeeder::class,
            BodiesSeeder::class,
            JobsSeeder::class,
            MarriagesSeeder::class,
            DrinksSeeder::class,
            SmokingsSeeder::class,
            SalariesSeeder::class,
            DaddiesSeeder::class,
            MembershipSeeder::class,
            PackageSeeder::class,
            QuestionsSeeder::class,
            AdminUsersSeeder::class,
            AddQuestionCategorySeeder::class
            // PurposesSeeder::class
        ]);

        \App\Model\Master\VipImage::create([
            'url' => '/assets/vip_img01.svg'
        ]);

        \App\Model\Master\Purpose::create([
            'name' => '今から会いたい'
        ]);
        \App\Model\Master\Purpose::create([
            'name' => 'この日空いてます'
        ]);
        \App\Model\Master\Purpose::create([
            'name' => '募集してます'
        ]);
    }
}
