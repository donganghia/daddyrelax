<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Package;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::create([
            'name' => 'package1',
            'price' => 50000,
            'original_price' => 50,
            'points' => 7500,
            'sort_no' => 0
        ]);
        Package::create([
            'name' => 'package2',
            'price' => 30000,
            'original_price' => 40,
            'points' => 4200,
            'sort_no' => 1
        ]);
        Package::create([
            'name' => 'package3',
            'price' => 20000,
            'original_price' => 30,
            'points' => 2600,
            'sort_no' => 2
        ]);
        Package::create([
            'name' => 'package4',
            'price' => 10000,
            'original_price' => 20,
            'points' => 1200,
            'sort_no' => 3
        ]);
        Package::create([
            'name' => 'package5',
            'price' => 5000,
            'original_price' => 10,
            'points' => 550,
            'sort_no' => 4
        ]);
        Package::create([
            'name' => 'package6',
            'price' => 3000,
            'original_price' => 0,
            'points' => 300,
            'sort_no' => 4
        ]);
        Package::create([
            'name' => 'package7',
            'price' => 1000,
            'original_price' => 0,
            'points' => 100,
            'sort_no' => 4
        ]);
    }
}
