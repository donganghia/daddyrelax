<?php

use Illuminate\Database\Seeder;
use App\Model\Master\City;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('mtb_cities')->truncate();

        City::create([
            'name' => '北海道'
        ]);
        City::create([
            'name' => '青森県'
        ]);
        City::create([
            'name' => '岩手県'
        ]);
        City::create([
            'name' => '宮城県'
        ]);
        City::create([
            'name' => '秋田県'
        ]);
        City::create([
            'name' => '山形県'
        ]);
        City::create([
            'name' => '福島県'
        ]);
        City::create([
            'name' => '茨城県'
        ]);
        City::create([
            'name' => '栃木県'
        ]);
        City::create([
            'name' => '群馬県'
        ]);
        City::create([
            'name' => '埼玉県'
        ]);
        City::create([
            'name' => '千葉県'
        ]);
        City::create([
            'name' => '東京市町村'
        ]);
        City::create([
            'name' => '東京23区'
        ]);
        City::create([
            'name' => '神奈川県'
        ]);
        City::create([
            'name' => '新潟県'
        ]);
        City::create([
            'name' => '富山県'
        ]);
        City::create([
            'name' => '石川県'
        ]);
        City::create([
            'name' => '福井県'
        ]);
        City::create([
            'name' => '山梨県'
        ]);
        City::create([
            'name' => '長野県'
        ]);
        City::create([
            'name' => '岐阜県'
        ]);
        City::create([
            'name' => '静岡県'
        ]);
        City::create([
            'name' => '愛知県'
        ]);
        City::create([
            'name' => '三重県'
        ]);
        City::create([
            'name' => '滋賀県'
        ]);
        City::create([
            'name' => '京都府'
        ]);
        City::create([
            'name' => '大阪府'
        ]);
        City::create([
            'name' => '兵庫県'
        ]);
        City::create([
            'name' => '奈良県'
        ]);
        City::create([
            'name' => '和歌山県'
        ]);
        City::create([
            'name' => '鳥取県'
        ]);
        City::create([
            'name' => '島根県'
        ]);
        City::create([
            'name' => '岡山県'
        ]);
        City::create([
            'name' => '広島県'
        ]);
        City::create([
            'name' => '山口県'
        ]);
        City::create([
            'name' => '徳島県'
        ]);
        City::create([
            'name' => '香川県'
        ]);
        City::create([
            'name' => '愛媛県'
        ]);
        City::create([
            'name' => '高知県'
        ]);
        City::create([
            'name' => '福岡県'
        ]);
        City::create([
            'name' => '佐賀県'
        ]);
        City::create([
            'name' => '長崎県'
        ]);
        City::create([
            'name' => '熊本県'
        ]);
        City::create([
            'name' => '大分県'
        ]);
        City::create([
            'name' => '宮崎県'
        ]);
        City::create([
            'name' => '鹿児島県'
        ]);
        City::create([
            'name' => '沖縄県'
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
