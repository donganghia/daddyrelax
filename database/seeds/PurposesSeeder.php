<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Purpose;

class PurposesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Purpose::create([
            'name' => '今から会いたい'
        ]);
        Purpose::create([
            'name' => 'この日空いてます'
        ]);
        Purpose::create([
            'name' => '募集してます'
        ]);
    }
}
