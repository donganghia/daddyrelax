<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Drink;

class DrinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Drink::create([
            'name' => '飲まない'
        ]);
        Drink::create([
            'name' => '時々飲む'
        ]);
        Drink::create([
            'name' => '飲む'
        ]);
    }
}
