<?php

use Illuminate\Database\Seeder;
use App\Model\Master\QuestionCategory;

class AddQuestionCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QuestionCategory::create([
            'name' => '運営からの連絡について',
            'sort_no' => 1
        ]);

        QuestionCategory::where('name', 'その他')->update([
            'sort_no' => 2
        ]);
    }
}
