<?php

use Illuminate\Database\Seeder;
use App\Model\Master\QuestionCategory;
use App\Model\Question;
use App\Model\Answer;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = QuestionCategory::create([
            'name' => '会員登録について'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '年齢を間違って登録しました',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '本人証明ご提出時、正しい年齢が記載された本人証明書をご提出ください。運営側で変更致します。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => 'プロフィール写真の承認確認はいつ終わりますか？',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '写真のアップロードから通常２営業日以内に完了します。<br/>確認は1件ずつスタッフが目視にて行っているため、時間帯や営業日によりご返答までの時間が異なります。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => 'プロフィール写真が否認されました',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '審査での否認の対象は以下項目となります。<br/><br/><strong>〇個人を特定しうる情報を含んでいるもの</strong><br/>本名、URL・メールアドレスなどの連絡先、勤務先の社名・ロゴ・役職、車のナンバープレート、その他個人の特定が可能だと判断しうるもの<br/><br/><strong>〇権利を損害するもの</strong><br/>著名人（芸能人、モデルなど）の写真のような本人以外の人物<br/>他人が著作権を有するもの<br/>18歳未満と判断される写真<br/>写真が見づらいもの<br/>不鮮明であるもの<br/>上下が逆さまであるもの<br/>画像加工ツールなどで著しく加工されているもの<br/><br/><strong>〇公序良俗に反するもの</strong><br/>ヌード写真<br/>わいせつ、またはそれを想起させうるもの<br/>宗教・信条を主張するもの<br/>その他閲覧者に不快感を与えると判断しうるもの'
        ]);

        $category = QuestionCategory::create([
            'name' => '年齢確認について'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '提出した本人証明書が否認されました',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '法令により、ご利用には本人証明書のご提出を義務づけております。承認には、下記に則った公的証明書のご提出が必要です。ご登録の年齢と年齢確認提出書に相違がある場合、運営にて公的証明書の年齢を適用致します。<br/><br/>＜以下の有効な公的証明書をご提出ください＞<br/>・運転免許証<br/>・健康保険証<br/>・パスポート<br/>※証明書のコピーは無効<br/><br/>＜必須な情報＞<br/>下記が必ず1枚の写真に写るようにしてください<br/>・氏名（カナ）<br/>・生年月日<br/>・証明書の発行元の名称<br/>・有効期限（保険証は表記がある場合）<br/>※マイナンバー通知カードは証明書としてご利用いただけません。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '本人証明書の確認はいつ終わりますか？',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '証明書のアップロードから通常２営業日以内に完了します。<br/>本人証明の確認は1件ずつスタッフが目視にて行うため、時間帯や営業日によりご返答までの時間が異なります。'
        ]);

        $category = QuestionCategory::create([
            'name' => '利用について'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '「ポイント」の使用について',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => 'ポイントは以下の用途でご使用いただけます。<br/>お相手に「いいね」を送る：20ポイント<br/>お相手からもらった「いいね」を返す：20ポイント<br/>募集の投稿：20ポイント<br/>※女性は無制限でご使用いただけます。ポイント購入の必要はございません。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '募集の投稿内容を修正したいです',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '投稿済みの内容変更はできません。マイページ＞タイムライン管理より、一度投稿を削除の上、再投稿してください。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '投稿した募集が表示されません',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '投稿内容は運営にて全て確認をさせていただいております。個人情報が含まれている等、不適切な内容と判断したものは、運営にて断りなく削除いたします。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '相手のプロフィールが白紙になりました',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => 'お相手が退会された場合、規約に基づきニックネーム、年齢、地域以外の情報は白紙になります。<br/>退会後のお相手でも、通報報告は可能です。メッセージ本文ページの最上部 ＞ 個人プロフィールページ ＞ 右上〇マークより、運営まで通報の詳細をご報告ください。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => 'ブロックした相手をブロック解除したいです',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => 'マイページ ＞ 設定 ＞ ブロック解除より解除いただけます'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '通報回数とは何ですか？',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '勧誘や、実物が別人だった等、お困りのユーザーがいましたら通報機能で運営にご報告ください。乱用防止のため、通報は１ヶ月あたり５回までです。毎月１日にリセットされます。５回使い切り、さらにどうしても困るユーザーがいた場合は、ヘルプのお問合せより運営までご連絡ください。'
        ]);

        $category = QuestionCategory::create([
            'name' => '有料会員、会員ステータスについて',
            'gender' => 1
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '利用料金について',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '有料会員・プラチナ会員の利用料金は積み上げ式となっております。プラチナ会員になるには、有料会員料金とは別にプラチナ会員の費用が発生致します。<br/>なお、プラチナ会員の利用料金は毎月払いのみです。<br/><br/>有料会員・プラチナ会員から無料会員に落ちていました<br/>利用料金の支払いがされているかご確認ください。マイページ＞設定＞支払情報よりご確認いただけます。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '有料会員・プラチナ会員から無料会員に落ちていました',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '利用料金の支払いがされているかご確認ください。マイページ＞設定＞支払情報よりご確認いただけます。'
        ]);

        $category = QuestionCategory::create([
            'name' => '支払いについて',
            'gender' => 1
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '支払いのクレジットカードを変更したいです',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => 'ポイント購入、及び有料サービスの決済時に、「カード情報を変更する」よりご変更ください'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '有料サービスの支払い状況を確認したいです',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => 'マイページ ＞ 設定 ＞ 支払情報より支払い状況を確認いただけます。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '有料サービスの支払いプランを変更したいです',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '一度、現在の支払いの自動更新をOFFにし、有料会員の利用期限が切れた後、改めてご希望のプランをご購入ください。マイページ＞設定＞有料サービス設定より変更いただけます。なお、利用期限が切れる際、一度無料会員になりますが、再度有料サービスをご購入されるとこれまでと同様にご利用いただけます。また、プラチナ会員、VIP会員の支払いプランは毎月払いのみです。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '有料サービスの自動支払いをオフにしたのに決済がかかりました',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '自動継続課金は、期限が切れる00:00の24時間以上前に自動課金をオフにしなければ、期限が切れる24時間以内に自動で請求が行われます。現在の支払い状況については、マイページ ＞ 設定 ＞ 支払情報をご確認ください。'
        ]);

        $category = QuestionCategory::create([
            'name' => '安心・安全への取り組み'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '実際に会ったら違う人だった',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '実際に会ったらプロフィールの写真と別人だった場合には、運営事務局へのご通報のご協力をお願いいたします。ご通報をいただいた内容に関しましては、弊社にて確認の上、迅速に対応いたします。<br/>お相手の顔を事前に確認したい場合には、メッセージのやりとりにて、プロフィールとは異なる写真を交換されることをお薦めいたします。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '18歳未満、高校生がいる',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '18歳未満、または高校生でご登録されているユーザーを見つけた際には、違反報告より運営事務局へのご通報のご協力をお願いいたします。通報内容について事務局にて確認の上、迅速に対応いたします。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => 'ビジネス勧誘について',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '下記のようなビジネス勧誘を受けた際は、運営事務局への通報のご協力をお願いいたします。通報内容について事務局にて確認の上、迅速に対応いたします。<br/>・アフィリエイト･投資･FX等の商材の販促･販売<br/>・化粧品･サプリメント･美容品の販促･販売<br/>・旅行商材や家電等の販促･販売'
        ]);

        $category = QuestionCategory::create([
            'name' => 'その他'
        ]);
        
        $question = Question::create([
            'category_id' => $category->id,
            'content' => '退会について',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => 'マイページ ＞ 設定＞ 退会 にてお手続きください。<br/><br/>＜ご注意事項＞<br/>・退会後は全ての会員向けサービスをご利用いただけなくなります。<br/>・メッセージの履歴・プロフィール情報等は全て削除されます。<br/>・各種有料会員プランへの登録期間内に退会する場合も月割り・日割りでの返金はありません。<br/>・各種有料会員をご利用の場合、退会前に必ず定期課金中の購入をキャンセルしてください。'
        ]);

        $question = Question::create([
            'category_id' => $category->id,
            'content' => '機種変更をしても同様に使えますか？',
            'publish' => 1
        ]);

        Answer::create([
            'question_id' => $question->id,
            'content' => '機種変更後も、これまでと同様にご利用いただけます。特別なお手続きは必要ありません。サイトを開き、通常通りログインしてください。'
        ]);
    }
}
