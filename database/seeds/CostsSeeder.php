<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Cost;

class CostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($p = 2000; $p <= 20000; $p += 500) {
            Cost::create([
                'name' => $p.'P',
                'points' => $p
            ]);
        }
    }
}
