<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Membership;

class MembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Membership::create([
            'name' => '無料会員',
            'type' => 'free',
            'price' => 0,
            'original_price' => 0,
            'period' => 0,
            'off_percent' => 0,
            'points' => 300,
            'search_days' => 0,
            'sort_no' => 0
        ]);
        Membership::create([
            'name' => '有料会員',
            'type' => 'paid',
            'price' => 2980,
            'original_price' => 2980,
            'period' => 1,
            'off_percent' => 0,
            'points' => 200,
            'search_days' => 1,
            'sort_no' => 0
        ]);
        Membership::create([
            'name' => '有料会員',
            'type' => 'paid',
            'price' => 2480,
            'original_price' => 2980,
            'period' => 3,
            'off_percent' => 17,
            'points' => 600,
            'search_days' => 1,
            'sort_no' => 1
        ]);
        Membership::create([
            'name' => '有料会員',
            'type' => 'paid',
            'price' => 1980,
            'original_price' => 2980,
            'period' => 6,
            'off_percent' => 28,
            'points' => 1200,
            'search_days' => 1,
            'sort_no' => 2
        ]);
        // Membership::create([
        //     'name' => '有料会員',
        //     'type' => 'paid',
        //     'price' => 5300,
        //     'original_price' => 9800,
        //     'period' => 12,
        //     'off_percent' => 46,
        //     'points' => 50,
        //     'search_days' => 3,
        //     'sort_no' => 3
        // ]);
        Membership::create([
            'name' => 'プラチナ会員',
            'type' => 'platium',
            'price' => 4980,
            'original_price' => 4980,
            'period' => 1,
            'off_percent' => 0,
            'points' => 400,
            'search_days' => 0,
            'sort_no' => 0
        ]);
        // Membership::create([
        //     'name' => 'VIP会員',
        //     'type' => 'vip',
        //     'price' => 19800,
        //     'original_price' => 19800,
        //     'period' => 0,
        //     'off_percent' => 0,
        //     'points' => 100,
        //     'search_days' => 0,
        //     'sort_no' => 0
        // ]);
    }
}
