<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Smoking;

class SmokingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Smoking::create([
            'name' => '吸わない'
        ]);
        Smoking::create([
            'name' => '時々吸う'
        ]);
        Smoking::create([
            'name' => '吸う'
        ]);
    }
}
