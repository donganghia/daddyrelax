<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Job;

class JobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Job::create([
            'name' => '経営者',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'オーナー',
            'gender' => 1
        ]);
        Job::create([
            'name' => '会社役員',
            'gender' => 1
        ]);
        Job::create([
            'name' => '医師',
            'gender' => 1
        ]);
        Job::create([
            'name' => '弁護士',
            'gender' => 1
        ]);
        Job::create([
            'name' => '公認会計士',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'コンサル',
            'gender' => 1
        ]);
        Job::create([
            'name' => '上場企業',
            'gender' => 1
        ]);
        Job::create([
            'name' => '大手商社',
            'gender' => 1
        ]);
        Job::create([
            'name' => '外資金融',
            'gender' => 1
        ]);
        Job::create([
            'name' => '大手外資',
            'gender' => 1
        ]);
        Job::create([
            'name' => '会社員',
            'gender' => 1
        ]);
        Job::create([
            'name' => '公務員',
            'gender' => 1
        ]); 
        Job::create([
            'name' => 'その他',
            'gender' => 1
        ]);
        
        Job::create([
            'name' => '学生',
            'gender' => 2
        ]);
        Job::create([
            'name' => '読者モデル',
            'gender' => 2
        ]);
        Job::create([
            'name' => '秘書 受付',
            'gender' => 2
        ]);
        Job::create([
            'name' => '客室乗務員',
            'gender' => 2
        ]);
        Job::create([
            'name' => '芸能 モデル',
            'gender' => 2
        ]);
        Job::create([
            'name' => '看護師',
            'gender' => 2
        ]);
        Job::create([
            'name' => '保育士',
            'gender' => 2
        ]);
        Job::create([
            'name' => '福祉 介護',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'アパレル',
            'gender' => 2
        ]);
        Job::create([
            'name' => '美容',
            'gender' => 2
        ]);
        Job::create([
            'name' => '接客業',
            'gender' => 2
        ]);
        Job::create([
            'name' => '会社員',
            'gender' => 2
        ]);
        Job::create([
            'name' => '公務員',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'クリエイター',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'フリーランス',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'その他',
            'gender' => 2
        ]);
    }
}
