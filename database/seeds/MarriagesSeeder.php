<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Marriage;

class MarriagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Marriage::create([
            'name' => '独身'
        ]);
        Marriage::create([
            'name' => '既婚'
        ]);
    }
}
