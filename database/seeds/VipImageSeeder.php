<?php

use Illuminate\Database\Seeder;

class VipImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Master\VipImage::create([
            'url' => '/assets/vip_img01.svg'
        ]);
    }
}
