<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Daddy;

class DaddiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Daddy::create([
            'name' => '肩もみ',
            'asset' => 'hand_up'
        ]);
        Daddy::create([
            'name' => '肩たたき',
            'asset' => 'hand_down'
        ]);
        Daddy::create([
            'name' => '服を見立てる',
            'asset' => 'shirt'
        ]);
    }
}
