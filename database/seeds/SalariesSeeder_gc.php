<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Salary;

class SalariesSeeder_gc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Salary::create([
            'name' => '600万以上'
        ]);
        Salary::create([
            'name' => '700万以上'
        ]);
        Salary::create([
            'name' => '800万以上'
        ]);
        Salary::create([
            'name' => '900万以上'
        ]);
        Salary::create([
            'name' => '1000万以上'
        ]);
        Salary::create([
            'name' => '1500万以上'
        ]);
        Salary::create([
            'name' => '2000万以上'
        ]);
        Salary::create([
            'name' => '3000万以上'
        ]);
        Salary::create([
            'name' => '4000万以上'
        ]);
        Salary::create([
            'name' => '5000万以上'
        ]);
        Salary::create([
            'name' => '6000万以上'
        ]);
        Salary::create([
            'name' => '7000万以上'
        ]);
        Salary::create([
            'name' => '8000万以上'
        ]);
        Salary::create([
            'name' => '9000万以上'
        ]);
        Salary::create([
            'name' => '1億以上'
        ]);
    }
}
