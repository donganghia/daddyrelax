<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Salary;

class SalariesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Salary::create([
            'name' => '400万以上'
        ]);
        Salary::create([
            'name' => '600万以上'
        ]);
        Salary::create([
            'name' => '800万以上'
        ]);
        Salary::create([
            'name' => '1000万以上'
        ]);
        Salary::create([
            'name' => '1500万以上'
        ]);
        Salary::create([
            'name' => '2000万以上'
        ]);
        Salary::create([
            'name' => '3000万以上'
        ]);
        Salary::create([
            'name' => '5000万以上'
        ]);
        Salary::create([
            'name' => '7000万以上'
        ]);
        Salary::create([
            'name' => '1億以上'
        ]);
        Salary::create([
            'name' => '2億以上'
        ]);
        Salary::create([
            'name' => '3億以上'
        ]);
        Salary::create([
            'name' => '4億以上'
        ]);
        Salary::create([
            'name' => '5億以上'
        ]);
        Salary::create([
            'name' => '7億以上'
        ]);
        Salary::create([
            'name' => '10億以上'
        ]);
    }
}
