<?php

use Illuminate\Database\Seeder;
use App\Model\Master\Job;

class JobsSeeder_gc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Job::create([
            'name' => '経営者',
            'gender' => 1
        ]);
        Job::create([
            'name' => '会社役員',
            'gender' => 1
        ]);
        Job::create([
            'name' => '投資家',
            'gender' => 1
        ]);
        Job::create([
            'name' => '外資系',
            'gender' => 1
        ]);
        Job::create([
            'name' => '医師',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'メーカー',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'IT通信',
            'gender' => 1
        ]);
        Job::create([
            'name' => '金融',
            'gender' => 1
        ]);
        Job::create([
            'name' => '商社',
            'gender' => 1
        ]);
        Job::create([
            'name' => '不動産',
            'gender' => 1
        ]);
        Job::create([
            'name' => '航空鉄道',
            'gender' => 1
        ]);
        Job::create([
            'name' => '旅行ホテル',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'エンタメ',
            'gender' => 1
        ]); 
        Job::create([
            'name' => '人材教育',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'インフラ系',
            'gender' => 1
        ]);
        Job::create([
            'name' => '店舗飲食',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'コンサル',
            'gender' => 1
        ]);
        Job::create([
            'name' => '芸能関係',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'アパレル',
            'gender' => 1
        ]); 
        Job::create([
            'name' => '広告出版',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'マスコミ',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'サービス業',
            'gender' => 1
        ]);
        Job::create([
            'name' => '医療福祉',
            'gender' => 1
        ]);
        Job::create([
            'name' => '弁護士',
            'gender' => 1
        ]); 
        Job::create([
            'name' => '会計士',
            'gender' => 1
        ]);
        Job::create([
            'name' => '公務員',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'エンジニア',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'クリエイター',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'フリーランス',
            'gender' => 1
        ]);
        Job::create([
            'name' => 'その他',
            'gender' => 1
        ]); 

        Job::create([
            'name' => '大学生',
            'gender' => 2
        ]);
        Job::create([
            'name' => '専門学生',
            'gender' => 2
        ]);
        Job::create([
            'name' => '芸能モデル',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'アイドル',
            'gender' => 2
        ]);
        Job::create([
            'name' => '秘書受付',
            'gender' => 2
        ]);
        Job::create([
            'name' => '客室乗務員',
            'gender' => 2
        ]);
        Job::create([
            'name' => '看護師',
            'gender' => 2
        ]);
        Job::create([
            'name' => '医療美容系',
            'gender' => 2
        ]);
        Job::create([
            'name' => '保育士',
            'gender' => 2
        ]);
        Job::create([
            'name' => '介護福祉',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'アパレル',
            'gender' => 2
        ]);
        Job::create([
            'name' => '美容師',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'エステティシャン',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'ヘアメイク',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'ネイリスト',
            'gender' => 2
        ]);
        Job::create([
            'name' => '化粧品販売',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'ブライダル',
            'gender' => 2
        ]);
        Job::create([
            'name' => '調理師',
            'gender' => 2
        ]);
        Job::create([
            'name' => '接客業',
            'gender' => 2
        ]);
        Job::create([
            'name' => '飲食店員',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'ショップ店員',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'サービス業',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'マスコミ',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'エンタメ',
            'gender' => 2
        ]);
        Job::create([
            'name' => '営業系',
            'gender' => 2
        ]);
        Job::create([
            'name' => '事務系',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'インストラクター',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'デザイナー',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'エンジニア',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'クリエイター',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'フリーランス',
            'gender' => 2
        ]);
        Job::create([
            'name' => 'その他',
            'gender' => 2
        ]);
    }
}
