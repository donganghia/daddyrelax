<?php
return [
    'admin_email' => 'support@daddyrelax.jp',
    'site_url' => env('APP_URL', 'https://daddyrelax.jp'),
    'STORAGE' => env('STORAGE'),
    'AWS_REGION' => env('AWS_REGION'),
    'AWS_BUCKET' => env('AWS_BUCKET'),
    'domain' => 'drosy.jp', //daddyrelax.jp',
    'clientip' => '87313',
    'month' => 30,
    'cookie_timeout'=> 48 * 60,
];
?>
